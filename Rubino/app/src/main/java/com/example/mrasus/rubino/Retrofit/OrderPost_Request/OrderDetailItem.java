package com.example.mrasus.rubino.Retrofit.OrderPost_Request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Mr.Asus on 5/22/2018.
 */

public class OrderDetailItem {
    @SerializedName("ProductId")
    @Expose
    private String productId;
    @SerializedName("Quantity")
    @Expose
    private Integer quantity;
    @SerializedName("Amount")
    @Expose
    private Integer amount;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }
}
