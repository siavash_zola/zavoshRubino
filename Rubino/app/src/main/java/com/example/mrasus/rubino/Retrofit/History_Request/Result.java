package com.example.mrasus.rubino.Retrofit.History_Request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Mr.Asus on 5/23/2018.
 */

public class Result {
    @SerializedName("orderHistories")
    @Expose
    private List<OrderHistory> orderHistories = null;
    @SerializedName("returnOrderHistories")
    @Expose
    private List<ReturnOrderHistory> returnOrderHistories = null;

    public List<OrderHistory> getOrderHistories() {
        return orderHistories;
    }

    public void setOrderHistories(List<OrderHistory> orderHistories) {
        this.orderHistories = orderHistories;
    }

    public List<ReturnOrderHistory> getReturnOrderHistories() {
        return returnOrderHistories;
    }

    public void setReturnOrderHistories(List<ReturnOrderHistory> returnOrderHistories) {
        this.returnOrderHistories = returnOrderHistories;
    }
}
