package com.example.mrasus.rubino.Constants;

import java.io.Serializable;

/**
 * Created by Mr.Asus on 5/16/2018.
 */

public class MessageRubino {
    private String textMessage;
    private String subject;
    private String date;
    private String attachUrl;

    public String getAttachUrl() {
        return attachUrl;
    }

    public void setAttachUrl(String attachUrl) {
        this.attachUrl = attachUrl;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }



    public String getTextMessage() {
        return textMessage;
    }

    public void setTextMessage(String textMessage) {
        this.textMessage = textMessage;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }


}
