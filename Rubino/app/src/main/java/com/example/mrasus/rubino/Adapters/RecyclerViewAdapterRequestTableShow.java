package com.example.mrasus.rubino.Adapters;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.IdRes;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.example.mrasus.rubino.Constants.Attach;
import com.example.mrasus.rubino.Constants.Product;
import com.example.mrasus.rubino.R;
import com.example.mrasus.rubino.Retrofit.ApiUtils;
import com.example.mrasus.rubino.activities.BaseActivity;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

/**
 * Created by Mr.Asus on 5/6/2018.
 */

public class RecyclerViewAdapterRequestTableShow extends RecyclerView.Adapter {
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_FOOTER = 1;
    private static final int TYPE_ITEM = 2;
    private static final int TYPE_ATTACH = 4;
    private Activity activity;
    private List<Product> productList;
    private String paymentType;
    private String transferType;
    private String address;
    private String priceORCargoCode;
    private String date;
    private String description;
    private String orderCode;
    private List<Attach> attachList;

    public RecyclerViewAdapterRequestTableShow(Activity activity , List<Product> productList,List<Attach> attachList,String paymentType,String transferType,String address,String priceORCargoCode,String date,String description,String orderCode) {
        this.activity = activity;
        this.productList = productList;
        this.paymentType = paymentType;
        this.transferType = transferType;
        this.address = address;
        this.priceORCargoCode = priceORCargoCode;
        this.date = date;
        this.description = description;
        this.orderCode = orderCode;
        this.attachList = attachList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_HEADER){
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.table_header_show, parent, false);
            return new HeaderViewHolder(itemView);
        }else if(viewType == TYPE_ITEM) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.table_item_show, parent, false);
            return new ItemViewHolder(itemView);
        }else if(viewType == TYPE_FOOTER) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.table_footer_show, parent, false);
            return new FooterViewHolder(itemView);
        }else if (viewType == TYPE_ATTACH){
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.attach_room, parent, false);
            return new AttachViewHolder(itemView);
        }else {
            return null;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final int positionNew = position-1;
        final int positionAttach = position-productList.size()-2;
        if (holder instanceof HeaderViewHolder){
            HeaderViewHolder headerViewHolder = (HeaderViewHolder) holder;
            headerViewHolder.textView_date.setText(date);
            headerViewHolder.textView_factorId.setText(orderCode);
        }else if (holder instanceof ItemViewHolder){
            ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
            final Product product = productList.get(positionNew);
            String price = product.getPrice();
            int number = product.getNumber();
            itemViewHolder.textView_title.setText(product.getTitle());
            itemViewHolder.textView_price.setText(convert(price));
            itemViewHolder.textView_num.setText(number+"");
            int sum = number * Integer.parseInt(price);
            itemViewHolder.textView_sum.setText(convert(sum+""));



        }else if (holder instanceof FooterViewHolder){
            final FooterViewHolder footerViewHolder = (FooterViewHolder) holder;
            int sum = 0;
            for (int i = 0; i < productList.size(); i++) {
                Product product = productList.get(i);
                sum = sum + product.getNumber() * Integer.parseInt(product.getPrice());
            }
            footerViewHolder.textView_price.setText(convert(sum+""));
            //---------------------------------------------------------
            footerViewHolder.radio_selected.setText(transferType);
            footerViewHolder.editText_transferSelected.setText(priceORCargoCode);
            footerViewHolder.editText_address.setText(address);
            footerViewHolder.radio_paySelected.setChecked(true);
            footerViewHolder.radio_paySelected.setText(paymentType);
            footerViewHolder.editText_Description.setText(description);
            //----------------------------------------------------------
            if (attachList.size()>0){
                footerViewHolder.textView_isExidtAttach.setText("");
            }else{
                footerViewHolder.textView_isExidtAttach.setText(R.string.noAttach);
            }


        }else if (holder instanceof AttachViewHolder){
            AttachViewHolder attachViewHolder = (AttachViewHolder) holder;
            final Attach attach = attachList.get(positionAttach);
            attachViewHolder.textView_attachName.setText(attach.getAttachName());

            attachViewHolder.linearLayout_attach.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String attachUrl = attach.getAttachUrl();
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse(attachUrl));
                    activity.startActivity(intent);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return productList.size()+2+attachList.size();
    }

    @Override
    public int getItemViewType(int position) {
        //if (position == 0){
        //    return TYPE_HEADER;
        //}else if (position == productList.size()+1) {
        //    return TYPE_FOOTER;
        //}else {
        //    return TYPE_ITEM;
        //}
        if (position == 0){
            return TYPE_HEADER;
        }else if(position>0 && position<productList.size()+1){
            return TYPE_ITEM;
        }else if (position == productList.size()+1){
            return TYPE_FOOTER;
        }else {
            return TYPE_ATTACH;
        }
    }

    private class HeaderViewHolder extends RecyclerView.ViewHolder{
        TextView textView_sum;
        TextView textView_num;
        TextView textView_price;
        TextView textView_title;
        TextView textView_factorId;
        TextView textView_date;
        public HeaderViewHolder(View itemView) {
            super(itemView);
            textView_sum = itemView.findViewById(R.id.textView_sum);
            textView_num = itemView.findViewById(R.id.textView_num);
            textView_price = itemView.findViewById(R.id.textView_price);
            textView_title = itemView.findViewById(R.id.textView_title);
            textView_factorId = itemView.findViewById(R.id.textView_factorId);
            textView_date = itemView.findViewById(R.id.textView_date);

            textView_sum.setTypeface(BaseActivity.appFont);
            textView_num.setTypeface(BaseActivity.appFont);
            textView_price.setTypeface(BaseActivity.appFont);
            textView_title.setTypeface(BaseActivity.appFont);
            textView_factorId.setTypeface(BaseActivity.appFont);
            textView_date.setTypeface(BaseActivity.appFont);
        }
    }

    private class ItemViewHolder extends RecyclerView.ViewHolder{
        TextView textView_sum;
        TextView textView_num;
        TextView textView_price;
        TextView textView_title;
        public ItemViewHolder(View itemView) {
            super(itemView);
            textView_sum = itemView.findViewById(R.id.textView_sum);
            textView_num = itemView.findViewById(R.id.textView_num);
            textView_price = itemView.findViewById(R.id.textView_price);
            textView_title = itemView.findViewById(R.id.textView_title);

            textView_sum.setTypeface(BaseActivity.appFont);
            textView_num.setTypeface(BaseActivity.appFont);
            textView_price.setTypeface(BaseActivity.appFont);
            textView_title.setTypeface(BaseActivity.appFont);
        }
    }

    private class FooterViewHolder extends RecyclerView.ViewHolder{
        TextView textView_price;
        TextView textView_priceText;
        TextView textView_transport_text;
        TextView textView_address_text;
        TextView textView_pay;
        TextView textView_attach_title;
        TextView textView_isExidtAttach;
        RadioButton radio_selected;
        RadioButton radio_paySelected;
        EditText editText_transferSelected;
        EditText editText_address;
        RadioGroup radioGroup;
        RadioGroup radioGroup_pay;
        EditText editText_Description;
        public FooterViewHolder(View itemView) {
            super(itemView);
            textView_price = itemView.findViewById(R.id.textView_price);
            textView_priceText = itemView.findViewById(R.id.textView_priceText);
            textView_transport_text = itemView.findViewById(R.id.textView_transport_text);
            textView_pay = itemView.findViewById(R.id.textView_pay);
            textView_attach_title = itemView.findViewById(R.id.textView_attach_title);
            textView_isExidtAttach = itemView.findViewById(R.id.textView_isExidtAttach);
            editText_Description = itemView.findViewById(R.id.editText_Description);
            radio_selected = itemView.findViewById(R.id.radio_selected);
            radio_paySelected = itemView.findViewById(R.id.radio_paySelected);
            editText_transferSelected = itemView.findViewById(R.id.editText_transferSelected);
            editText_address = itemView.findViewById(R.id.editText_address);
            textView_address_text = itemView.findViewById(R.id.textView_address_text);
            radioGroup = itemView.findViewById(R.id.radioGroup_transport);
            radioGroup_pay = itemView.findViewById(R.id.radioGroup_pay);

            textView_priceText.setTypeface(BaseActivity.appFont);
            textView_price.setTypeface(BaseActivity.appFont);
            textView_transport_text.setTypeface(BaseActivity.appFont);
            radio_selected.setTypeface(BaseActivity.appFont);
            editText_transferSelected.setTypeface(BaseActivity.appFont);
            editText_address.setTypeface(BaseActivity.appFont);
            textView_address_text.setTypeface(BaseActivity.appFont);
            textView_pay.setTypeface(BaseActivity.appFont);
            radio_paySelected.setTypeface(BaseActivity.appFont);
            textView_attach_title.setTypeface(BaseActivity.appFont);
            textView_isExidtAttach.setTypeface(BaseActivity.appFont);
            editText_Description.setTypeface(BaseActivity.appFont);

        }
    }

    private class AttachViewHolder extends RecyclerView.ViewHolder {
        TextView textView_attachName;
        LinearLayout linearLayout_attach;
        public AttachViewHolder(View itemView) {
            super(itemView);
            textView_attachName = itemView.findViewById(R.id.textView_attachName);
            linearLayout_attach = itemView.findViewById(R.id.linearLayout_attach);
            textView_attachName.setTypeface(BaseActivity.appFont);
        }
    }

    public String convert(String price){
        NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
        String numberAsString = numberFormat.format(Integer.parseInt(price));
        return numberAsString;
    }
}
