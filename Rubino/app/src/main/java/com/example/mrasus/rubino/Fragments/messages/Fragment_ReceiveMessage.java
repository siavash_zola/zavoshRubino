package com.example.mrasus.rubino.Fragments.messages;


import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.mrasus.rubino.Adapters.RecyclerViewAdapterSendMessage;
import com.example.mrasus.rubino.Constants.MessageRubino;
import com.example.mrasus.rubino.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment_ReceiveMessage extends Fragment {

    private RecyclerViewAdapterSendMessage recyclerViewAdapterReceiveMessage;
    private List<MessageRubino> messageRubinoList;
    private LinearLayoutManager layoutManager;
    private RecyclerView recyclerView_receiveMessage;
    private TextView textView_no_message;

    public Fragment_ReceiveMessage() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_fragment__receive_message, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        reference(view);
    }

    private void reference(View view) {
        messageRubinoList = new ArrayList<>();
        fromText(getArguments().getString("list"));
        recyclerView_receiveMessage = view.findViewById(R.id.recyclerView_receiveMessage);
        textView_no_message = view.findViewById(R.id.textView_no_message);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerViewAdapterReceiveMessage = new RecyclerViewAdapterSendMessage(messageRubinoList,getActivity());
        recyclerView_receiveMessage.setLayoutManager(layoutManager);
        recyclerView_receiveMessage.setAdapter(recyclerViewAdapterReceiveMessage);
        if (messageRubinoList.size() == 0){
            textView_no_message.setText(R.string.noMessage);
        }
    }

    public void fromText (String s){
        try {
            JSONArray jsonArray = new JSONArray(s);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                MessageRubino messageRubino = new MessageRubino();

                messageRubino.setTextMessage(jsonObject.getString("textMessage"));
                messageRubino.setSubject(jsonObject.getString("subject"));
                messageRubino.setDate(jsonObject.getString("date"));

                messageRubinoList.add(messageRubino);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
