package com.example.mrasus.rubino.Adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.mrasus.rubino.R;
import com.example.mrasus.rubino.Retrofit.GetProfile_Request.City;
import com.example.mrasus.rubino.Retrofit.GetProfile_Request.Province;
import com.example.mrasus.rubino.activities.BaseActivity;

import java.util.List;

/**
 * Created by Mr.Asus on 6/12/2018.
 */

public class AdapterSpinnerCity extends ArrayAdapter<City> {
    private List<City> list;
    private Context context;
    private Typeface appFont;
    private TextView textView_row_string;
    private LayoutInflater layoutInflater;

    public AdapterSpinnerCity(Context context, List<City> list) {
        super(context, 0, list);
        this.list = list;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rootView = layoutInflater.inflate(R.layout.spinner_view, null);
        textView_row_string = (TextView) rootView.findViewById(R.id.textView_spinner);
        textView_row_string.setTypeface(appFont);
        City city = list.get(position);
        textView_row_string.setText(city.getTitle());
        return rootView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View rootView = layoutInflater.inflate(R.layout.spinner_view, null);
        textView_row_string = (TextView) rootView.findViewById(R.id.textView_spinner);
        textView_row_string.setTypeface(BaseActivity.appFont);
        String s = list.get(position).getTitle();
        textView_row_string.setText(s);
        return rootView;

    }
}
