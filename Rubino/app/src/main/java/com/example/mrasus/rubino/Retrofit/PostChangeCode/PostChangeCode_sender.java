package com.example.mrasus.rubino.Retrofit.PostChangeCode;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Mr.Asus on 6/12/2018.
 */

public class PostChangeCode_sender {
    @SerializedName("cellNumber")
    @Expose
    private String cellNumber;

    public String getCellNumber() {
        return cellNumber;
    }

    public void setCellNumber(String cellNumber) {
        this.cellNumber = cellNumber;
    }
}
