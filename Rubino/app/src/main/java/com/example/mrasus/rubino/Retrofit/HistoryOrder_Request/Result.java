package com.example.mrasus.rubino.Retrofit.HistoryOrder_Request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Mr.Asus on 5/26/2018.
 */

public class Result {
    @SerializedName("orderDetailItem")
    @Expose
    private List<OrderDetailItem> orderDetailItem = null;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("submitDate")
    @Expose
    private String submitDate;
    @SerializedName("totalAmount")
    @Expose
    private Integer totalAmount;
    @SerializedName("shippmentType")
    @Expose
    private String shippmentType;
    @SerializedName("shippmentTitle")
    @Expose
    private String shippmentTitle;
    @SerializedName("paymentType")
    @Expose
    private String paymentType;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("orderAttachments")
    @Expose
    private List<OrderAttachment> orderAttachments = null;

    public List<OrderDetailItem> getOrderDetailItem() {
        return orderDetailItem;
    }

    public void setOrderDetailItem(List<OrderDetailItem> orderDetailItem) {
        this.orderDetailItem = orderDetailItem;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getSubmitDate() {
        return submitDate;
    }

    public void setSubmitDate(String submitDate) {
        this.submitDate = submitDate;
    }

    public Integer getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Integer totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getShippmentType() {
        return shippmentType;
    }

    public void setShippmentType(String shippmentType) {
        this.shippmentType = shippmentType;
    }

    public String getShippmentTitle() {
        return shippmentTitle;
    }

    public void setShippmentTitle(String shippmentTitle) {
        this.shippmentTitle = shippmentTitle;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<OrderAttachment> getOrderAttachments() {
        return orderAttachments;
    }

    public void setOrderAttachments(List<OrderAttachment> orderAttachments) {
        this.orderAttachments = orderAttachments;
    }



}
