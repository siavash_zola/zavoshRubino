package com.example.mrasus.rubino.Database;

/**
 * Created by Mr.Asus on 5/6/2018.
 */

public class SqlConstant {
    public static final String DATABASE_NAME = "RUBINO_DATABASE";
    public static final String TABLE_NAME = "RETURN_TABLE";
    public static final String TABLE_NAME_ORDER = "ORDER_TABLE";
    public static final String TABLE_NAME_RETURN = "RETURN_TABLE";
    public static final int DATABASE_VERSION = 1;
    public static final String COLUMN_0 = "_id";
    public static final String COLUMN_1  = "productID";
    public static final String COLUMN_2  = "title";
    public static final String COLUMN_3  = "price";
    public static final String COLUMN_4  = "number";
    public static final String CREATE_RETURN_TABLE = "CREATE TABLE " + TABLE_NAME +"( _id INTEGER PRIMARY KEY AUTOINCREMENT,"
            +COLUMN_1 + " TEXT,"
            +COLUMN_2 +" TEXT,"
            +COLUMN_3 +" TEXT,"
            +COLUMN_4 +" INTEGER);";
    public static final String CREATE_ORDER_TABLE = "CREATE TABLE " + TABLE_NAME_ORDER +"( _id INTEGER PRIMARY KEY AUTOINCREMENT,"
            +COLUMN_1 + " TEXT,"
            +COLUMN_2 +" TEXT,"
            +COLUMN_3 +" TEXT,"
            +COLUMN_4 +" INTEGER);";
}
