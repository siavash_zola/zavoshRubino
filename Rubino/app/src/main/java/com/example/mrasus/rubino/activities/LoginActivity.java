package com.example.mrasus.rubino.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mrasus.rubino.Constants.Constants;
import com.example.mrasus.rubino.Constants.InternetChecker;
import com.example.mrasus.rubino.CustomView.MyEditText;
import com.example.mrasus.rubino.Database.MyApp;
import com.example.mrasus.rubino.MyToast.RubinoToast;
import com.example.mrasus.rubino.R;
import com.example.mrasus.rubino.Retrofit.APIService;
import com.example.mrasus.rubino.Retrofit.ApiUtils;
import com.example.mrasus.rubino.Retrofit.Forgot_phone_request.Forgot_phone_request;
import com.example.mrasus.rubino.Retrofit.Forgot_phone_request.Forgot_phone_sender;
import com.example.mrasus.rubino.Retrofit.Login_Request.LoginRequest;


import me.zhanghai.android.materialprogressbar.MaterialProgressBar;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {
    private MyEditText editText_username,editText_password;
    private TextView textView_login,textView_forgot;
    private APIService apiService;
    private MaterialProgressBar materialProgressBar;
    private AlertDialog showFogotDialog;
    private AlertDialog showDialogSms;
    private ProgressBar progressBar_login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        checkIsLogin();
        reference();
        listener();
        internetChecker();
    }

    @Override
    protected void onPause() {
        super.onPause();
        //materialProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //finish();

    }

    private void internetChecker() {
        boolean online = InternetChecker.isOnline(LoginActivity.this);
        if (online){
            Log.i("online","connected");
        }else{
            Log.i("online"," not connected");
            RubinoToast.showToast(LoginActivity.this,getString(R.string.checkNetwork),Toast.LENGTH_SHORT,RubinoToast.warning);
        }
    }

    private void checkIsLogin() {
        String token = MyApp.getMemory().getString(Constants.tokenId, "");
        if (token.length()>0){
            startActivity(new Intent(LoginActivity.this,Splash.class));
            finish();
        }

    }

    private void listener() {
        textView_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String username = editText_username.text();
                String password = editText_password.text();
                if (username.length() == 0 | password.length() == 0){
                    RubinoToast.showToast(LoginActivity.this,getString(R.string.completeForm), Toast.LENGTH_SHORT,RubinoToast.warning);
                }else {
                    if (InternetChecker.isOnline(LoginActivity.this)) {
                        progressBar_login.setVisibility(View.VISIBLE);
                        textView_login.setVisibility(View.GONE);
                        login(username, password);
                    }else {
                        RubinoToast.showToast(LoginActivity.this,getString(R.string.checkNetwork),Toast.LENGTH_SHORT,RubinoToast.warning);
                    }
                }
            }
        });

        editText_username.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    Constants.hideKeyboard(LoginActivity.this,v);
                }
            }
        });
        editText_password.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    Constants.hideKeyboard(LoginActivity.this,v);
                }
            }
        });

        textView_forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog_forgot();
            }
        });


    }

    private void dialog_forgot() {
        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
        LayoutInflater inflater = (LayoutInflater)getSystemService(LAYOUT_INFLATER_SERVICE);
        View rootView = inflater.inflate(R.layout.dialog_forgo, null);
        TextView textView_title = rootView.findViewById(R.id.textView_title);
        TextView textView_recovery = rootView.findViewById(R.id.textView_recovery);
        TextView textView_cancel = rootView.findViewById(R.id.textView_cancel);
        final MyEditText editText_code = rootView.findViewById(R.id.editText_mobile);

        textView_cancel.setTypeface(MyApp.appFont);
        textView_recovery.setTypeface(MyApp.appFont);
        textView_title.setTypeface(MyApp.appFont);

        builder.setView(rootView);
        builder.create();
        showFogotDialog = builder.show();

        textView_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showFogotDialog.dismiss();
            }
        });
        textView_recovery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String mobile = editText_code.text();
                if (checkValid(mobile)) {
                    forgotPass(mobile);
                }
                showFogotDialog.dismiss();
            }
        });
    }
    public void showDialogGetSms(){
        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View rootView = inflater.inflate(R.layout.dialog_check_sms, null);
        TextView textView_title = rootView.findViewById(R.id.textView_title);
        TextView textView_sendCode = rootView.findViewById(R.id.textView_sendCode);
        TextView textView_cancel = rootView.findViewById(R.id.textView_cancel);
        final MyEditText editText_code = rootView.findViewById(R.id.editText_code);

        textView_cancel.setTypeface(MyApp.appFont);
        textView_sendCode.setTypeface(MyApp.appFont);
        textView_title.setTypeface(MyApp.appFont);
        editText_code.setTypeface(MyApp.appFont);

        builder.setView(rootView);
        builder.create();
        showDialogSms = builder.show();

        textView_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialogSms.dismiss();
            }
        });
        textView_sendCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String code = editText_code.text();
            }
        });
    }

    //-----retrofit-----
    private void login(final String username, final String password) {
        materialProgressBar.setVisibility(View.VISIBLE);
        Call<LoginRequest> login = apiService.login(username, password);
        login.enqueue(new Callback<LoginRequest>() {
            @Override
            public void onResponse(Call<LoginRequest> call, Response<LoginRequest> response) {
                progressBar_login.setVisibility(View.GONE);
                textView_login.setVisibility(View.VISIBLE);
                if (response.body() != null){
                    if (response.body().getStatus().getIsSuccess()) {
                        materialProgressBar.setVisibility(View.INVISIBLE);
                        MyApp.getMemory().edit().putString(Constants.tokenId, response.body().getResult().getTokenId()).commit();
                        MyApp.getMemory().edit().putString(Constants.username, username).commit();
                        MyApp.getMemory().edit().putString(Constants.password, password).commit();
                        startActivity(new Intent(LoginActivity.this, Splash.class));
                        finish();
                    }else {
                        materialProgressBar.setVisibility(View.INVISIBLE);
                        RubinoToast.showToast(LoginActivity.this,response.body().getStatus().getMessage(),Toast.LENGTH_SHORT,RubinoToast.warning);
                    }
                }else{
                    materialProgressBar.setVisibility(View.INVISIBLE);
                    RubinoToast.showToast(LoginActivity.this,getString(R.string.error),Toast.LENGTH_SHORT,RubinoToast.warning);

                }
            }

            @Override
            public void onFailure(Call<LoginRequest> call, Throwable t) {
                progressBar_login.setVisibility(View.GONE);
                textView_login.setVisibility(View.VISIBLE);
                materialProgressBar.setVisibility(View.INVISIBLE);
                RubinoToast.showToast(LoginActivity.this,getString(R.string.error),Toast.LENGTH_SHORT,RubinoToast.warning);

            }
        });
        //request midim be server age ok bood zakhire mikonimeshoon va mirim splash injaram finish mikonim


    }
    private void forgotPass(String mobile){
        materialProgressBar.setVisibility(View.VISIBLE);
        Forgot_phone_sender forgot_phone_sender = new Forgot_phone_sender();
        forgot_phone_sender.setCellNumber(mobile);
        forgot_phone_sender.setDeviceModel("model");
        Log.i("kk",getDeviceName());
        forgot_phone_sender.setOsType("android");
        forgot_phone_sender.setOsVersion(Build.VERSION.SDK_INT+"");
        forgot_phone_sender.setDeviceId(Settings.Secure.getString(LoginActivity.this.getContentResolver(), Settings.Secure.ANDROID_ID));
        Call<Forgot_phone_request> forgot_phone_requestCall = apiService.Forgot_sendPhone(forgot_phone_sender);
        forgot_phone_requestCall.enqueue(new Callback<Forgot_phone_request>() {
            @Override
            public void onResponse(Call<Forgot_phone_request> call, Response<Forgot_phone_request> response) {
                materialProgressBar.setVisibility(View.INVISIBLE);
                if (response.body() != null){
                    if (response.body().getStatus().getIsSuccess()) {
                        //RubinoToast.showToast(LoginActivity.this,response.body().getStatus().getMessage(),Toast.LENGTH_SHORT,RubinoToast.warning);
                        showDialogGetSms();
                    }else {
                        RubinoToast.showToast(LoginActivity.this,response.body().getStatus().getMessage(),Toast.LENGTH_SHORT,RubinoToast.warning);
                    }
                }else{
                    materialProgressBar.setVisibility(View.INVISIBLE);
                    RubinoToast.showToast(LoginActivity.this,getString(R.string.error),Toast.LENGTH_SHORT,RubinoToast.warning);
                }
            }

            @Override
            public void onFailure(Call<Forgot_phone_request> call, Throwable t) {
                materialProgressBar.setVisibility(View.INVISIBLE);
                RubinoToast.showToast(LoginActivity.this,getString(R.string.error),Toast.LENGTH_SHORT,RubinoToast.warning);
            }
        });
    }
    //-----retrofit-----

    private void reference() {
        materialProgressBar = (MaterialProgressBar) findViewById(R.id.materialProgressBar);
        apiService = ApiUtils.getAPIService();
        editText_username = (MyEditText) findViewById(R.id.editText_username);
        editText_password = (MyEditText) findViewById(R.id.editText_password);
        textView_forgot = (TextView) findViewById(R.id.textView_forgot);
        textView_login = (TextView) findViewById(R.id.textView_login);

        textView_forgot.setTypeface(MyApp.appFont);
        textView_login.setTypeface(MyApp.appFont);
        progressBar_login = (ProgressBar) findViewById(R.id.progressBar_login);
    }
    public String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.toLowerCase().startsWith(manufacturer.toLowerCase())) {
            return capitalize(model);
        } else {
            return capitalize(manufacturer) + " " + model;
        }
    }

    private boolean checkValid(String phone) {
        boolean b = false;

        if (phone.matches("(\\+989|09)(12|19|35|36|37|38|39|32)\\d{7}")){
            b = true;
        }else{
            RubinoToast.showToast(LoginActivity.this,getString(R.string.notValidPhone), Toast.LENGTH_SHORT,RubinoToast.warning);
        }

        return b;
    }

    private String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }

    }



}
