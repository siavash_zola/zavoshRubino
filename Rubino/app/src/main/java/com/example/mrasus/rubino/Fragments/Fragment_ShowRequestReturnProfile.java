package com.example.mrasus.rubino.Fragments;


import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.mrasus.rubino.Adapters.RecyclerViewAdapterHistoryReturn;
import com.example.mrasus.rubino.Adapters.RecyclerViewAdapterReturnNew;
import com.example.mrasus.rubino.Adapters.RecyclerViewAdapterReturnNewShow;
import com.example.mrasus.rubino.Constants.Attach;
import com.example.mrasus.rubino.Constants.Constants;
import com.example.mrasus.rubino.Constants.InternetChecker;
import com.example.mrasus.rubino.Constants.Product;
import com.example.mrasus.rubino.Constants.ReturnProduct;
import com.example.mrasus.rubino.Database.MyApp;
import com.example.mrasus.rubino.MyToast.RubinoToast;
import com.example.mrasus.rubino.R;
import com.example.mrasus.rubino.Retrofit.APIService;
import com.example.mrasus.rubino.Retrofit.ApiUtils;
import com.example.mrasus.rubino.Retrofit.HistoryOrder_Request.OrderAttachment;
import com.example.mrasus.rubino.Retrofit.HistoryReturn_Request.HistoryReturnRequest;
import com.example.mrasus.rubino.Retrofit.HistoryReturn_Request.HistoryReturnSender;
import com.example.mrasus.rubino.Retrofit.HistoryReturn_Request.ReturnOrderDetailItem;
import com.example.mrasus.rubino.Retrofit.Login_Request.LoginRequest;
import com.example.mrasus.rubino.activities.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import me.zhanghai.android.materialprogressbar.MaterialProgressBar;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment_ShowRequestReturnProfile extends Fragment {

    private String returnCode;
    private RecyclerView recyclerView_ReturnShow;
    private LinearLayoutManager layoutManager;
    private List<ReturnProduct> list;
    private List<Attach> attachList;
    private RecyclerViewAdapterReturnNewShow recyclerViewAdapterHistoryReturnShow;
    private MaterialProgressBar materialProgressBar;
    private boolean isLife = true;


    public Fragment_ShowRequestReturnProfile() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_fragment__show_request_return_profile, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        reference(view);
        internetChecker();
    }

    @Override
    public void onPause() {
        super.onPause();
        isLife = false;
        materialProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void onResume() {
        super.onResume();
        isLife = true;
    }

    //---------retrofit--------------
    private void requestToServer() {
        materialProgressBar.setVisibility(View.VISIBLE);
        APIService apiService = ApiUtils.getAPIService();
        HistoryReturnSender historyReturnSender = new HistoryReturnSender();
        historyReturnSender.setReturnOrderCode(returnCode);
        Call<HistoryReturnRequest> historyReturnRequestCall = apiService.returnHistory(MyApp.getMemory().getString(Constants.tokenId, ""), historyReturnSender);
        historyReturnRequestCall.enqueue(new Callback<HistoryReturnRequest>() {
            @Override
            public void onResponse(Call<HistoryReturnRequest> call, Response<HistoryReturnRequest> response) {
                materialProgressBar.setVisibility(View.GONE);
                if (isLife) {
                    if (response.body() != null) {
                        if (response.body().getStatus().getIsSuccess()) {
                            HistoryReturnRequest body = response.body();
                            List<ReturnOrderDetailItem> returnOrderDetailItems = body.getResult().getReturnOrderDetailItems();
                            list = getProductList(returnOrderDetailItems);
                            recyclerViewAdapterHistoryReturnShow = new RecyclerViewAdapterReturnNewShow(getActivity(), list, attachList, body.getResult().getShippmetType(), body.getResult().getShippmentTitle(), body.getResult().getSubmitDate());
                            recyclerView_ReturnShow.setAdapter(recyclerViewAdapterHistoryReturnShow);
                            BaseActivity.unFreeze();
                        } else {
                            RubinoToast.showToast(getActivity(), response.body().getStatus().getMessage(), Toast.LENGTH_SHORT, RubinoToast.warning);
                            BaseActivity.unFreeze();
                        }

                    } else {
                        //RubinoToast.showToast(getActivity(),getString(R.string.error), Toast.LENGTH_SHORT,RubinoToast.warning);
                        login();
                    }
                }
            }

            @Override
            public void onFailure(Call<HistoryReturnRequest> call, Throwable t) {
                materialProgressBar.setVisibility(View.GONE);
                RubinoToast.showToast(getActivity(),getString(R.string.error), Toast.LENGTH_SHORT,RubinoToast.warning);
            }
        });

    }
    private void login(){
        materialProgressBar.setVisibility(View.VISIBLE);
        APIService apiService = ApiUtils.getAPIService();
        Call<LoginRequest> login = apiService.login(MyApp.getMemory().getString(Constants.username, ""), MyApp.getMemory().getString(Constants.password, ""));
        login.enqueue(new Callback<LoginRequest>() {
            @Override
            public void onResponse(Call<LoginRequest> call, Response<LoginRequest> response) {
                if (response.body()!=null){
                    if (response.body().getStatus().getIsSuccess()){
                        if (isLife) {
                            MyApp.getMemory().edit().putString(Constants.tokenId, response.body().getResult().getTokenId()).commit();
                            requestToServer();
                        }
                    }else{
                        RubinoToast.showToast(getActivity(),response.body().getStatus().getMessage(), Toast.LENGTH_SHORT,RubinoToast.warning);
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginRequest> call, Throwable t) {
                RubinoToast.showToast(getActivity(),getString(R.string.error), Toast.LENGTH_SHORT,RubinoToast.warning);
            }
        });
    }
    //---------retrofit--------------
    private void reference(View view) {
        returnCode = getArguments().getString("OrderCode");
        list = new ArrayList<>();
        attachList = new ArrayList<>();
        recyclerView_ReturnShow = view.findViewById(R.id.recyclerView_ReturnShow);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView_ReturnShow.setLayoutManager(layoutManager);
        materialProgressBar = getActivity().findViewById(R.id.materialProgressBar);

    }

    private List<Attach> getAttachList(List<OrderAttachment> orderAttachments) {
        List<Attach> list = new ArrayList<>();
        for (int i = 0; i < orderAttachments.size(); i++) {
            Attach attach = new Attach();
            OrderAttachment orderAttachment = orderAttachments.get(i);
            attach.setAttachName(orderAttachment.getTitle());
            attach.setAttachUrl(orderAttachment.getLinkAddress());
            list.add(attach);
        }
        return list;
    }

    private List<ReturnProduct> getProductList(List<ReturnOrderDetailItem> returnOrderDetailItem) {
        List<ReturnProduct> list = new ArrayList<>();
        for (int i = 0; i < returnOrderDetailItem.size(); i++) {
            ReturnProduct returnProduct = new ReturnProduct();
            ReturnOrderDetailItem returnOrderDetailItem1 = returnOrderDetailItem.get(i);
            returnProduct.setSerial(returnOrderDetailItem1.getProductSerialNumber());
            returnProduct.setReason(returnOrderDetailItem1.getReturnReason());
            list.add(returnProduct);
        }
        return list;
    }

    private void internetChecker() {
        boolean online = InternetChecker.isOnline(getActivity());
        if (online){
            if (!BaseActivity.isFreezed) {
                BaseActivity.freeze(getActivity());
            }
            requestToServer();
        }else{
            RubinoToast.showToast(getActivity(),getString(R.string.checkNetwork), Toast.LENGTH_SHORT,RubinoToast.warning);
        }
    }

}
