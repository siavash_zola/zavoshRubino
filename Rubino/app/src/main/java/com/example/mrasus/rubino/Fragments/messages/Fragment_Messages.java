package com.example.mrasus.rubino.Fragments.messages;


import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mrasus.rubino.Constants.Constants;
import com.example.mrasus.rubino.Constants.InternetChecker;
import com.example.mrasus.rubino.Constants.MessageRubino;
import com.example.mrasus.rubino.Database.MyApp;
import com.example.mrasus.rubino.MyToast.RubinoToast;
import com.example.mrasus.rubino.R;
import com.example.mrasus.rubino.Retrofit.APIService;
import com.example.mrasus.rubino.Retrofit.ApiUtils;
import com.example.mrasus.rubino.Retrofit.GetAllMessage_Request.GetAllMessageRequest;
import com.example.mrasus.rubino.Retrofit.GetAllMessage_Request.Result;
import com.example.mrasus.rubino.Retrofit.Login_Request.LoginRequest;
import com.example.mrasus.rubino.activities.BaseActivity;
import com.gigamole.navigationtabstrip.NavigationTabStrip;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import me.zhanghai.android.materialprogressbar.MaterialProgressBar;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment_Messages extends Fragment {

    private NavigationTabStrip nts_messages;
    private FrameLayout frame_messages;
    private FragmentManager fragmentManager;
    private Fragment_SendMessage fragment_sendMessage;
    private Fragment_ReceiveMessage fragment_receiveMessage;
    private MaterialProgressBar materialProgressBar;
    private SwipeRefreshLayout refreshLayout;
    private List<MessageRubino> messageSendRubinoList;
    private List<MessageRubino> messageReceiveRubinoList;
    private String sendOrReceive = "send";
    private Call<GetAllMessageRequest> callAllMessage;
    private boolean isLife =true;

    public Fragment_Messages() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_fragment__messages, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        toolbar();
        nts(view);
        reference(view);
        internetChecker();
        listener();
    }

    @Override
    public void onPause() {
        super.onPause();
        isLife = false;
        materialProgressBar.setVisibility(View.GONE);
        callAllMessage.cancel();
    }

    @Override
    public void onResume() {
        super.onResume();
        isLife = true;
    }
    private void toolbar() {
        TextView textView = getActivity().findViewById(R.id.textView_toolbar);
        textView.setText(getString(R.string.messages));
    }
    private void reference(View view){
        frame_messages = view.findViewById(R.id.frame_messages);

        materialProgressBar = getActivity().findViewById(R.id.materialProgressBar);
        refreshLayout = view.findViewById(R.id.refresh);
        refreshLayout.setColorSchemeResources(R.color.colorPrimary);
        messageSendRubinoList = new ArrayList<>();
        messageReceiveRubinoList = new ArrayList<>();
    }
    private void nts(final View view) {
        nts_messages = view.findViewById(R.id.nts_messages);
        nts_messages.setTitles(getString(R.string.allMessageReceived),getString(R.string.allMessagePosted));
        nts_messages.setTabIndex(1);
        nts_messages.setTypeface(BaseActivity.appFont);

        nts_messages.setOnTabStripSelectedIndexListener(new NavigationTabStrip.OnTabStripSelectedIndexListener() {
            @Override
            public void onStartTabSelected(String title, int index) {

            }

            @Override
            public void onEndTabSelected(String title, int index) {
                switch (index){
                    case 0:
                        sendOrReceive = "receive";
                        fragment_receiveMessage = new Fragment_ReceiveMessage();
                        fragment_receiveMessage.setArguments(toText(messageReceiveRubinoList));
                        FragmentTransaction fragmentTransaction0 = BaseActivity.fragmentManager.beginTransaction();
                        fragmentTransaction0.replace(R.id.frame_messages,fragment_receiveMessage).commit();
                        break;
                    case 1:
                        sendOrReceive = "send";
                        fragment_sendMessage = new Fragment_SendMessage();
                        fragment_sendMessage.setArguments(toText(messageSendRubinoList));
                        FragmentTransaction fragmentTransaction1 = BaseActivity.fragmentManager.beginTransaction();
                        fragmentTransaction1.replace(R.id.frame_messages,fragment_sendMessage).commit();
                        break;
                }
            }
        });
    }
    //---------retrofit--------
    private void getAllMessage(){
        materialProgressBar.setVisibility(View.VISIBLE);
        APIService apiService = ApiUtils.getAPIService();
        callAllMessage = apiService.getAllMessage(MyApp.getMemory().getString(Constants.tokenId, ""));
        callAllMessage.enqueue(new Callback<GetAllMessageRequest>() {
            @Override
            public void onResponse(Call<GetAllMessageRequest> call, Response<GetAllMessageRequest> response) {
                materialProgressBar.setVisibility(View.GONE);
                refreshLayout.setRefreshing(false);
                if (isLife) {
                    if (response.body() != null) {
                        if (response.body().getStatus().getIsSuccess()) {
                            messageReceiveRubinoList.clear();
                            messageSendRubinoList.clear();
                            List<Result> result = response.body().getResult();
                            for (int i = 0; i < result.size(); i++) {
                                Result result1 = result.get(i);
                                Integer typeCode = result1.getTypeCode();
                                MessageRubino messageRubino = new MessageRubino();
                                messageRubino.setDate(result1.getSubmitDate());
                                messageRubino.setSubject(result1.getSubject());
                                messageRubino.setTextMessage(result1.getBody());
                                messageRubino.setAttachUrl(result1.getAttachmentUrl());
                                if (typeCode == 1) {
                                    messageSendRubinoList.add(messageRubino);
                                } else {
                                    messageReceiveRubinoList.add(messageRubino);
                                }

                            }

                            BaseActivity.unFreeze();
                            if (sendOrReceive.equals("send")) {
                                fragment_sendMessage = new Fragment_SendMessage();
                                fragment_sendMessage.setArguments(toText(messageSendRubinoList));
                                FragmentTransaction fragmentTransaction = BaseActivity.fragmentManager.beginTransaction();
                                fragmentTransaction.replace(R.id.frame_messages, fragment_sendMessage).commit();
                            } else {

                                fragment_receiveMessage = new Fragment_ReceiveMessage();
                                fragment_receiveMessage.setArguments(toText(messageReceiveRubinoList));
                                FragmentTransaction fragmentTransaction0 = BaseActivity.fragmentManager.beginTransaction();
                                fragmentTransaction0.replace(R.id.frame_messages, fragment_receiveMessage).commit();
                            }
                        } else {
                            RubinoToast.showToast(getActivity(), response.body().getStatus().getMessage(), Toast.LENGTH_SHORT, RubinoToast.warning);
                            BaseActivity.unFreeze();
                        }

                    } else {
                        //RubinoToast.showToast(getActivity(),getString(R.string.error), Toast.LENGTH_SHORT,RubinoToast.warning);
                        login();
                    }
                }

            }

            @Override
            public void onFailure(Call<GetAllMessageRequest> call, Throwable t) {
                if (call.isCanceled()){
                    materialProgressBar.setVisibility(View.GONE);
                }else {
                    materialProgressBar.setVisibility(View.GONE);
                    refreshLayout.setRefreshing(false);
                }

            }
        });
    }
    private void login(){
        materialProgressBar.setVisibility(View.VISIBLE);
        APIService apiService = ApiUtils.getAPIService();
        Call<LoginRequest> login = apiService.login(MyApp.getMemory().getString(Constants.username, ""), MyApp.getMemory().getString(Constants.password, ""));
        login.enqueue(new Callback<LoginRequest>() {
            @Override
            public void onResponse(Call<LoginRequest> call, Response<LoginRequest> response) {
                if (response.body()!=null){
                    if (response.body().getStatus().getIsSuccess()){
                        if (isLife) {
                            MyApp.getMemory().edit().putString(Constants.tokenId, response.body().getResult().getTokenId()).commit();
                            getAllMessage();
                        }
                    }else{
                        RubinoToast.showToast(getActivity(),response.body().getStatus().getMessage(), Toast.LENGTH_SHORT,RubinoToast.warning);
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginRequest> call, Throwable t) {
                RubinoToast.showToast(getActivity(),getString(R.string.error), Toast.LENGTH_SHORT,RubinoToast.warning);
            }
        });
    }
    //---------retrofit--------
    private void listener(){
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                internetChecker();
            }
        });
    }
    private void internetChecker() {
        boolean online = InternetChecker.isOnline(getActivity());
        if (online){
            if (!BaseActivity.isFreezed) {
                BaseActivity.freeze(getActivity());
            }
            getAllMessage();
        }else{
            refreshLayout.setRefreshing(false);
            RubinoToast.showToast(getActivity(),getString(R.string.checkNetwork), Toast.LENGTH_SHORT,RubinoToast.warning);
        }
    }

    private Bundle toText(List<MessageRubino> list) {
        Bundle bundle= new Bundle();
        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < list.size(); i++) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("textMessage",list.get(i).getTextMessage());
                jsonObject.put("subject",list.get(i).getSubject());
                jsonObject.put("date",list.get(i).getDate());
                if (list.get(i).getAttachUrl() != null) {
                    jsonObject.put("attachUrl", list.get(i).getAttachUrl());
                }else {
                    jsonObject.put("attachUrl", "");
                }

                jsonArray.put(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        bundle.putString("list",jsonArray.toString());
        return bundle;

    }

}
