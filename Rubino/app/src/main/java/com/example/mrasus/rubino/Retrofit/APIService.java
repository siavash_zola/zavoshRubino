package com.example.mrasus.rubino.Retrofit;

import com.example.mrasus.rubino.Constants.Constants;
import com.example.mrasus.rubino.Database.MyApp;
import com.example.mrasus.rubino.Retrofit.Brand_Request.BrandRequest;
import com.example.mrasus.rubino.Retrofit.Forgot_phone_request.Forgot_phone_request;
import com.example.mrasus.rubino.Retrofit.Forgot_phone_request.Forgot_phone_sender;
import com.example.mrasus.rubino.Retrofit.GetAllMessage_Request.GetAllMessageRequest;
import com.example.mrasus.rubino.Retrofit.GetProfile_Request.GetProfileRequest;
import com.example.mrasus.rubino.Retrofit.HistoryOrder_Request.HistoryOrderRequest;
import com.example.mrasus.rubino.Retrofit.HistoryOrder_Request.HistoryOrderSender;
import com.example.mrasus.rubino.Retrofit.HistoryReturn_Request.HistoryReturnRequest;
import com.example.mrasus.rubino.Retrofit.HistoryReturn_Request.HistoryReturnSender;
import com.example.mrasus.rubino.Retrofit.History_Request.HistoryRequest;
import com.example.mrasus.rubino.Retrofit.Login_Request.LoginRequest;
import com.example.mrasus.rubino.Retrofit.OrderPost_Request.OrderPostRequest;
import com.example.mrasus.rubino.Retrofit.OrderPost_Request.OrderPostSender;
import com.example.mrasus.rubino.Retrofit.PostChangeCode.PostChangeCode_request;
import com.example.mrasus.rubino.Retrofit.PostChangeCode.PostChangeCode_sender;
import com.example.mrasus.rubino.Retrofit.PostChange_Request.PostChangeSender;
import com.example.mrasus.rubino.Retrofit.ProductList_Request.ProductList_request;
import com.example.mrasus.rubino.Retrofit.Product_Category.ProductCategory;
import com.example.mrasus.rubino.Retrofit.Product_Category.Sender;
import com.example.mrasus.rubino.Retrofit.ReturnOrder_Reqestu.ReturnOrderSender;
import com.example.mrasus.rubino.Retrofit.SendMessage_Request.SendMessageRequest;
import com.example.mrasus.rubino.Retrofit.SendMessage_Request.SendMessageSender;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by Mr.Asus on 5/20/2018.
 */

public interface APIService {

    @POST("/account/login")
    @FormUrlEncoded
    Call<LoginRequest> login(@Field("cellNumber") String cellNumber,
                             @Field("password") String password);

    @GET("/Brand/get")
    Call<BrandRequest> getBrand();


    @Headers("Content-Type: application/json")
    @POST("/ProductCategory/list")
    Call<ProductCategory> categoryGrp(@Header("Authorization") String header,
                                      @Body Sender sender);

    @Headers("Content-Type: application/json")
    @POST("/product/list")
    Call<ProductList_request> categoryList(@Header("Authorization") String header,
                                           @Body com.example.mrasus.rubino.Retrofit.ProductList_Request.Sender sender);

    @Headers("Content-Type: application/json")
    @POST("/Order/Post")
    Call<OrderPostRequest> orderPost(@Header("Authorization") String header,
                                     @Body OrderPostSender orderPostSender);

    @Headers("Content-Type: application/json")
    @POST("/ReturnOrder/Post")
    Call<OrderPostRequest> returnOrder(@Header("Authorization") String header,
                                     @Body ReturnOrderSender returnOrderSender);

    @Headers("Content-Type: application/json")
    @GET("/Order/History")
    Call<HistoryRequest> getHistory(@Header("Authorization") String header);

    @Headers("Content-Type: application/json")
    @POST("/Order/Detail")
    Call<HistoryOrderRequest> orderHistory(@Header("Authorization") String header,
                                           @Body HistoryOrderSender historyOrderSender);

    @Headers("Content-Type: application/json")
    @POST("/ReturnOrder/detail")
    Call<HistoryReturnRequest> returnHistory(@Header("Authorization") String header,
                                             @Body HistoryReturnSender historyReturnSender);

    @Headers("Content-Type: application/json")
    @POST("/Message/Post")
    Call<SendMessageRequest> sendMessage(@Header("Authorization") String header,
                                         @Body SendMessageSender sendMessageSender);

    @Headers("Content-Type: application/json")
    @GET("/Message/History")
    Call<GetAllMessageRequest> getAllMessage(@Header("Authorization") String header);

    @Headers("Content-Type: application/json")
    @GET("/userprofile/getprofile")
    Call<GetProfileRequest> getProfile(@Header("Authorization") String header);

    @Headers("Content-Type: application/json")
    @POST("/user/profile/postchangeCode")
    Call<PostChangeCode_request> PostChangeCode(@Header("Authorization") String header,
                                                @Body PostChangeCode_sender postChangeCode_sender);

    @Headers("Content-Type: application/json")
    @POST("/user/profile/PostChange")
    Call<PostChangeCode_request> PostChangeByCode(@Header("Authorization") String header,
                                                  @Body PostChangeSender postChangeSender);

    @Headers("Content-Type: application/json")
    @POST("/account/requestforgetpassword")
    Call<Forgot_phone_request> Forgot_sendPhone(@Body Forgot_phone_sender forgot_phone_sender);


    @Multipart
    @POST("/testapi/testfile")
    Call<ResponseBody> upload(
            @Part("description") RequestBody description,
            @Part MultipartBody.Part file
    );


}
