package com.example.mrasus.rubino.Retrofit.HistoryReturn_Request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Mr.Asus on 5/27/2018.
 */

public class ReturnOrderDetailItem {
    @SerializedName("productSerialNumber")
    @Expose
    private String productSerialNumber;
    @SerializedName("returnReason")
    @Expose
    private String returnReason;

    public String getProductSerialNumber() {
        return productSerialNumber;
    }

    public void setProductSerialNumber(String productSerialNumber) {
        this.productSerialNumber = productSerialNumber;
    }

    public String getReturnReason() {
        return returnReason;
    }

    public void setReturnReason(String returnReason) {
        this.returnReason = returnReason;
    }
}
