package com.example.mrasus.rubino.Fragments;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.example.mrasus.rubino.Adapters.RecyclerViewAdapterProducts;
import com.example.mrasus.rubino.Constants.Constants;
import com.example.mrasus.rubino.Constants.InternetChecker;
import com.example.mrasus.rubino.Constants.Product;
import com.example.mrasus.rubino.Database.MyApp;
import com.example.mrasus.rubino.MyToast.RubinoToast;
import com.example.mrasus.rubino.R;
import com.example.mrasus.rubino.Retrofit.APIService;
import com.example.mrasus.rubino.Retrofit.ApiUtils;
import com.example.mrasus.rubino.Retrofit.Login_Request.LoginRequest;
import com.example.mrasus.rubino.Retrofit.ProductList_Request.ProductList_request;
import com.example.mrasus.rubino.Retrofit.ProductList_Request.Result;
import com.example.mrasus.rubino.Retrofit.ProductList_Request.Sender;
import com.example.mrasus.rubino.activities.BaseActivity;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import me.zhanghai.android.materialprogressbar.MaterialProgressBar;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment_ShowProduct extends Fragment {


    private RecyclerView recyclerView_products;
    private ArrayList<Product> productList;
    private TextView textViewAddOrder,textViewOtherProduct;
    private RecyclerViewAdapterProducts recyclerViewAdapterProducts;
    private LinearLayoutManager layoutManager;
    private String groupName;
    private String id;
    private MaterialProgressBar materialProgressBar;
    private SwipeRefreshLayout refreshLayout;
    private boolean isLife= true;

    public Fragment_ShowProduct () {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_fragment__show_product, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        reference(view);
        toolbar();
        internetChecker();
        listener();

    }

    @Override
    public void onPause() {
        super.onPause();
        isLife = false;
        materialProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void onResume() {
        super.onResume();
        isLife = true;
    }

    private void toolbar() {
        TextView toolbar = getActivity().findViewById(R.id.textView_toolbar);
        toolbar.setText(groupName);
    }

    private void reference(View view) {
        groupName = getArguments().getString("ProductName");
        id = getArguments().getString("ProductId");
        materialProgressBar = getActivity().findViewById(R.id.materialProgressBar);
        recyclerView_products = (RecyclerView) view.findViewById(R.id.recyclerView_productsAll);
        productList = new ArrayList<>();
        textViewAddOrder = (TextView) view.findViewById(R.id.textViewAddOrder);
        textViewOtherProduct = (TextView) view.findViewById(R.id.textViewOtherProduct);
        textViewAddOrder.setTypeface(BaseActivity.appFont);
        textViewOtherProduct.setTypeface(BaseActivity.appFont);

        recyclerViewAdapterProducts = new RecyclerViewAdapterProducts(getActivity(),productList);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView_products.setLayoutManager(layoutManager);
        recyclerView_products.setAdapter(recyclerViewAdapterProducts);

        refreshLayout = view.findViewById(R.id.refresh);
        refreshLayout.setColorSchemeResources(R.color.colorPrimary);


        textViewAddOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int sumNumber = MyApp.getCenterDAO_Order().getSumNumber();
                if (sumNumber>0) {
                    Fragment fragment_requestProfile = new Fragment_requestProfile();
                    android.support.v4.app.FragmentTransaction fragmentTransaction = BaseActivity.fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.frame, fragment_requestProfile).addToBackStack("tag").commit();
                }else{
                    //Toast.makeText(activity,R.string.noProductSelected,Toast.LENGTH_SHORT).show();
                    RubinoToast.showToast(getActivity(),getString(R.string.noProductSelected),Toast.LENGTH_SHORT,RubinoToast.warning);
                }
            }
        });
        textViewOtherProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
    }
    //---------retrofit--------
    public void getProductList(){
        materialProgressBar.setVisibility(View.VISIBLE);
        APIService apiService = ApiUtils.getAPIService();
        Sender sender = new Sender();
        sender.setProductCategoryId(id);
        Call<ProductList_request> productList_requestCall = apiService.categoryList(MyApp.getMemory().getString(Constants.tokenId, ""), sender);
        productList_requestCall.enqueue(new Callback<ProductList_request>() {
            @Override
            public void onResponse(Call<ProductList_request> call, Response<ProductList_request> response) {
                materialProgressBar.setVisibility(View.INVISIBLE);
                refreshLayout.setRefreshing(false);
                if (isLife) {
                    if (response.body() != null) {
                        if (response.body().getStatus() != null) {
                            if (response.body().getStatus().getIsSuccess()) {
                                List<Result> result = response.body().getResult();
                                inputList(result);
                                BaseActivity.unFreeze();
                            } else {
                                RubinoToast.showToast(getActivity(), response.body().getStatus().getMessage(), Toast.LENGTH_SHORT, RubinoToast.warning);
                                BaseActivity.unFreeze();
                            }
                        } else {
                            RubinoToast.showToast(getActivity(), getString(R.string.error), Toast.LENGTH_SHORT, RubinoToast.warning);
                            BaseActivity.unFreeze();
                        }

                    } else {
                        //RubinoToast.showToast(getActivity(),getString(R.string.error), Toast.LENGTH_SHORT,RubinoToast.warning);
                        login();
                    }
                }
            }

            @Override
            public void onFailure(Call<ProductList_request> call, Throwable t) {
                refreshLayout.setRefreshing(false);
                materialProgressBar.setVisibility(View.INVISIBLE);
                RubinoToast.showToast(getActivity(),getString(R.string.error), Toast.LENGTH_SHORT,RubinoToast.warning);
            }
        });
    }
    private void login(){
        materialProgressBar.setVisibility(View.VISIBLE);
        APIService apiService = ApiUtils.getAPIService();
        Call<LoginRequest> login = apiService.login(MyApp.getMemory().getString(Constants.username, ""), MyApp.getMemory().getString(Constants.password, ""));
        login.enqueue(new Callback<LoginRequest>() {
            @Override
            public void onResponse(Call<LoginRequest> call, Response<LoginRequest> response) {
                if (response.body()!=null){
                    if (response.body().getStatus().getIsSuccess()){
                        if (isLife) {
                            MyApp.getMemory().edit().putString(Constants.tokenId, response.body().getResult().getTokenId()).commit();
                            getProductList();
                        }
                    }else{
                        RubinoToast.showToast(getActivity(),response.body().getStatus().getMessage(), Toast.LENGTH_SHORT,RubinoToast.warning);
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginRequest> call, Throwable t) {
                RubinoToast.showToast(getActivity(),getString(R.string.error), Toast.LENGTH_SHORT,RubinoToast.warning);
            }
        });
    }
    //---------retrofit--------

    private void inputList(List<Result> result) {
        for (int i = 0; i < result.size(); i++) {
            Result resultProduct = result.get(i);
            Product product= new Product();
            product.setId(resultProduct.getId());
            product.setImageUrl(resultProduct.getImageUrl());
            product.setSize(resultProduct.getSize());
            product.setTitle(resultProduct.getTitle());
            product.setPrice(resultProduct.getAmount().toString());
            productList.add(product);
            recyclerViewAdapterProducts.notifyDataSetChanged();
        }
    }

    private void internetChecker() {
        boolean online = InternetChecker.isOnline(getActivity());
        if (online){
            if (!BaseActivity.isFreezed) {
                BaseActivity.freeze(getActivity());
            }
            getProductList();
        }else{
            RubinoToast.showToast(getActivity(),getString(R.string.checkNetwork), Toast.LENGTH_SHORT,RubinoToast.warning);
        }
    }

    private void listener(){
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                productList.clear();
                if (!BaseActivity.isFreezed) {
                    BaseActivity.freeze(getActivity());
                }
                getProductList();
            }
        });
    }
}
