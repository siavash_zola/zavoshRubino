package com.example.mrasus.rubino.Fragments;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mrasus.rubino.Adapters.AdapterSpinner;
import com.example.mrasus.rubino.Adapters.AdapterSpinnerCity;
import com.example.mrasus.rubino.Constants.Constants;
import com.example.mrasus.rubino.Constants.InternetChecker;
import com.example.mrasus.rubino.CustomView.MyEditText;
import com.example.mrasus.rubino.Database.MyApp;
import com.example.mrasus.rubino.MyToast.RubinoToast;
import com.example.mrasus.rubino.R;
import com.example.mrasus.rubino.Retrofit.APIService;
import com.example.mrasus.rubino.Retrofit.ApiUtils;
import com.example.mrasus.rubino.Retrofit.GetProfile_Request.City;
import com.example.mrasus.rubino.Retrofit.GetProfile_Request.GetProfileRequest;
import com.example.mrasus.rubino.Retrofit.GetProfile_Request.Province;
import com.example.mrasus.rubino.Retrofit.GetProfile_Request.Result;
import com.example.mrasus.rubino.Retrofit.Login_Request.LoginRequest;
import com.example.mrasus.rubino.Retrofit.PostChangeCode.PostChangeCode_request;
import com.example.mrasus.rubino.Retrofit.PostChangeCode.PostChangeCode_sender;
import com.example.mrasus.rubino.Retrofit.PostChange_Request.PostChangeSender;
import com.example.mrasus.rubino.activities.BaseActivity;
import com.example.mrasus.rubino.activities.LoginActivity;

import java.util.ArrayList;
import java.util.List;

import me.zhanghai.android.materialprogressbar.MaterialProgressBar;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.mrasus.rubino.R.string.city;
import static com.example.mrasus.rubino.R.string.phone_mobile;

/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment_Profile extends Fragment {

    private TextView textView_logOut,textView_headerTitle,textView_marketName,textView_email,textView_managerName,textView_managerFamily,textView_phone_mobile,textView_state,textView_city,textView_update;
    private MyEditText editText_marketName,editText_email,editText_managerName,editText_managerFamily,editText_phone_mobile,editText_state,editText_city;
    private LinearLayout linearLayout_state,linearLayout_city,linearLayout_logOut;
    private ArrayList<String> listState;
    private ArrayList<String> listCity;
    private AlertDialog show;
    private AlertDialog showDialogExit;
    private MaterialProgressBar materialProgressBar;
    private AlertDialog showDialogSms;
    private List<Province> provinces;
    private List<City> cities;
    private AdapterSpinner adapterSpinner;
    private Province lastProvince;
    private City lastCity;
    private APIService apiService;
    private ProgressBar progressBar;
    private List<City> allCities;
    private AdapterSpinnerCity adapterSpinnerCity;
    private SwipeRefreshLayout refreshLayout;
    private String code;
    private boolean isLife = true;

    public Fragment_Profile() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_fragment__profile, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        toolbar();
        reference(view);
        listener();
        internetChecker();
        keyboard();
    }

    @Override
    public void onResume() {
        super.onResume();
        materialProgressBar.setVisibility(View.GONE);
        isLife = true;
    }

    @Override
    public void onPause() {
        super.onPause();
        isLife = false;
        materialProgressBar.setVisibility(View.GONE);
    }

    private void listener() {
        linearLayout_state.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showDialog(listState,"state",adapterSpinner);
            }
        });

        linearLayout_city.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showDialog(listCity,"city",adapterSpinnerCity);
            }
        });

        textView_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkValid()) {
                    textView_update.setVisibility(View.INVISIBLE);
                    progressBar.setVisibility(View.VISIBLE);
                    getSmsCode();
                }
            }
        });
        linearLayout_logOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createDialogLogout();
            }
        });
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                internetChecker();
            }
        });
    }

    private boolean checkValid() {
        boolean b = false;
        String name = editText_marketName.text();
        String family = editText_managerFamily.text();
        String marketName = editText_marketName.text();
        String email = editText_email.text();
        String phone = editText_phone_mobile.text();
        String state = editText_state.text();
        String city = editText_city.text();

        if (name.length()>0 && family.length()>0 && marketName.length()>0
                && email.length()>0 && phone.length()>0 &&state.length()>0 && city.length()>0){
            if (isValidEmail(email)){
                if (phone.matches("(\\+989|09)(12|19|35|36|37|38|39|32)\\d{7}")){
                    b = true;
                }else{
                    RubinoToast.showToast(getActivity(),getString(R.string.notValidPhone), Toast.LENGTH_SHORT,RubinoToast.warning);
                }
            }else {
                RubinoToast.showToast(getActivity(),getString(R.string.notValidEmail), Toast.LENGTH_SHORT,RubinoToast.warning);
            }
        }else {
            RubinoToast.showToast(getActivity(),getString(R.string.completeForm), Toast.LENGTH_SHORT,RubinoToast.warning);
        }

        return b;
    }

    private void createDialogLogout() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rootView = inflater.inflate(R.layout.dialog_logout, null);
        builder.setView(rootView);

        TextView textView_header = rootView.findViewById(R.id.textView_header);
        TextView textView_message = rootView.findViewById(R.id.textView_message);
        TextView textView_exit = rootView.findViewById(R.id.textView_exit);
        TextView textView_cancel = rootView.findViewById(R.id.textView_cancel);

        textView_header.setTypeface(BaseActivity.appFont);
        textView_message.setTypeface(BaseActivity.appFont);
        textView_exit.setTypeface(BaseActivity.appFont);
        textView_cancel.setTypeface(BaseActivity.appFont);

        builder.create();
        showDialogExit = builder.show();

        textView_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialogExit.dismiss();
            }
        });
        textView_exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                //Intent intent = new Intent(getActivity(),LoginActivity.class);
                MyApp.getMemory().edit().putString(Constants.tokenId,"").commit();
                MyApp.getMemory().edit().putString(Constants.password,"").commit();
                MyApp.getMemory().edit().putString(Constants.username,"").commit();
                getActivity().onBackPressed();
                //getActivity().startActivity(intent);
                //getActivity().finish();
            }
        });

    }

    private void showDialog(final ArrayList<String> list, final String stateOrCity, final ArrayAdapter arrayAdapter) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rootView = inflater.inflate(R.layout.dialog_state_city, null);
        ListView listView = rootView.findViewById(R.id.listView_state_city);



        listView.setAdapter(arrayAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (stateOrCity.equals("state")){
                    lastProvince = provinces.get(i);
                    editText_state.setText(lastProvince.getTitle());
                    changeCitiesList(lastProvince.getId());
                    editText_city.setText("");
                    show.dismiss();
                }else{
                    lastCity = cities.get(i);
                    editText_city.setText(lastCity.getTitle());
                    show.dismiss();
                }
            }
        });
        builder.setView(rootView);
        builder.create();
        show = builder.show();
    }

    private void reference(View view) {
        apiService = ApiUtils.getAPIService();
        cities = new ArrayList<>();
        provinces = new ArrayList<>();
        progressBar = view.findViewById(R.id.progressBar);
        materialProgressBar = getActivity().findViewById(R.id.materialProgressBar);
        textView_marketName = view.findViewById(R.id.textView_marketName);
        textView_email = view.findViewById(R.id.textView_email);
        textView_managerName = view.findViewById(R.id.textView_managerName);
        textView_managerFamily = view.findViewById(R.id.textView_managerFamily);
        textView_phone_mobile = view.findViewById(R.id.textView_phone_mobile);
        textView_state = view.findViewById(R.id.textView_state);
        textView_city = view.findViewById(R.id.textView_city);
        textView_update = view.findViewById(R.id.textView_update);
        textView_headerTitle = view.findViewById(R.id.textView_headerTitle);
        textView_logOut = view.findViewById(R.id.textView_logOut);
        refreshLayout = view.findViewById(R.id.refresh);
        refreshLayout.setColorSchemeResources(R.color.colorPrimary);
        editText_marketName = view.findViewById(R.id.editText_marketName);
        editText_email = view.findViewById(R.id.editText_email);
        editText_managerName = view.findViewById(R.id.editText_managerName);
        editText_managerFamily = view.findViewById(R.id.editText_managerFamily);
        editText_phone_mobile = view.findViewById(R.id.editText_phone_mobile);
        editText_state = view.findViewById(R.id.editText_state);
        editText_city = view.findViewById(R.id.editText_city);

        adapterSpinner = new AdapterSpinner(getActivity(),provinces);
        adapterSpinnerCity = new AdapterSpinnerCity(getActivity(),cities);

        textView_marketName.setTypeface(BaseActivity.appFont);
        textView_managerName.setTypeface(BaseActivity.appFont);
        textView_managerFamily.setTypeface(BaseActivity.appFont);
        textView_phone_mobile.setTypeface(BaseActivity.appFont);
        textView_state.setTypeface(BaseActivity.appFont);
        textView_city.setTypeface(BaseActivity.appFont);
        textView_update.setTypeface(BaseActivity.appFont);
        textView_headerTitle.setTypeface(BaseActivity.appFont);
        textView_logOut.setTypeface(BaseActivity.appFont);

        linearLayout_city = view.findViewById(R.id.linearLayout_city);
        linearLayout_state = view.findViewById(R.id.linearLayout_state);
        linearLayout_logOut = view.findViewById(R.id.linearLayout_logOut);

        listState = new ArrayList<>();
        listCity = new ArrayList<>();

        listState.add("tehran");
        listState.add("esfehan");
        listState.add("alborz");

        listCity.add("tehran");
        listCity.add("rasht");
        listCity.add("bandar abas");

    }

    private void toolbar() {
        TextView textView = getActivity().findViewById(R.id.textView_toolbar);
        textView.setText(getString(R.string.profile));
    }

    public void showDialogGetSms(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(getActivity().LAYOUT_INFLATER_SERVICE);
        View rootView = inflater.inflate(R.layout.dialog_check_sms, null);
        TextView textView_title = rootView.findViewById(R.id.textView_title);
        TextView textView_sendCode = rootView.findViewById(R.id.textView_sendCode);
        TextView textView_cancel = rootView.findViewById(R.id.textView_cancel);
        final MyEditText editText_code = rootView.findViewById(R.id.editText_code);

        textView_cancel.setTypeface(BaseActivity.appFont);
        textView_sendCode.setTypeface(BaseActivity.appFont);
        textView_title.setTypeface(BaseActivity.appFont);

        builder.setView(rootView);
        builder.create();
        showDialogSms = builder.show();

        textView_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialogSms.dismiss();
            }
        });
        textView_sendCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                code = editText_code.text();
                sendCodeAndData(code);
            }
        });
    }

    //------------retro-----
    public void getProfile() {
        //materialProgressBar.setVisibility(View.VISIBLE);
        Call<GetProfileRequest> profile = apiService.getProfile(MyApp.getMemory().getString(Constants.tokenId, ""));
        profile.enqueue(new Callback<GetProfileRequest>() {
            @Override
            public void onResponse(Call<GetProfileRequest> call, Response<GetProfileRequest> response) {
                //materialProgressBar.setVisibility(View.GONE);
                refreshLayout.setRefreshing(false);
                if (isLife) {
                    if (response.body() != null) {
                        if (response.body().getStatus() != null) {
                            if (response.body().getStatus().getIsSuccess()) {
                                Result result = response.body().getResult();
                                setData(result);
                                BaseActivity.unFreeze();
                            } else {
                                RubinoToast.showToast(getActivity(), response.body().getStatus().getMessage(), Toast.LENGTH_SHORT, RubinoToast.warning);
                                BaseActivity.unFreeze();
                            }
                        } else {
                            RubinoToast.showToast(getActivity(), getString(R.string.error), Toast.LENGTH_SHORT, RubinoToast.warning);
                            BaseActivity.unFreeze();
                        }
                    } else {
                        login(1);
                    }
                }
            }

            @Override
            public void onFailure(Call<GetProfileRequest> call, Throwable t) {
                RubinoToast.showToast(getActivity(),getString(R.string.error), Toast.LENGTH_SHORT,RubinoToast.warning);
                refreshLayout.setRefreshing(false);
                BaseActivity.unFreeze();
            }
        });
    }
    public void getSmsCode(){
        PostChangeCode_sender postChangeCode_sender = new PostChangeCode_sender();
        postChangeCode_sender.setCellNumber(MyApp.getMemory().getString(Constants.username,""));
        Call<PostChangeCode_request> postChangeCode_requestCall = apiService.PostChangeCode(MyApp.getMemory().getString(Constants.tokenId, ""), postChangeCode_sender);
        postChangeCode_requestCall.enqueue(new Callback<PostChangeCode_request>() {
            @Override
            public void onResponse(Call<PostChangeCode_request> call, Response<PostChangeCode_request> response) {
                textView_update.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.INVISIBLE);
                if (isLife) {
                    if (response.body() != null) {
                        if (response.body().getStatus() != null) {
                            if (response.body().getStatus().getIsSuccess()) {
                                if (response.body().getResult().equals("ok")) {
                                    showDialogGetSms();
                                }
                            } else {
                                RubinoToast.showToast(getActivity(), response.body().getStatus().getMessage(), Toast.LENGTH_SHORT, RubinoToast.warning);
                            }
                        } else {
                            RubinoToast.showToast(getActivity(), getString(R.string.error), Toast.LENGTH_SHORT, RubinoToast.warning);
                        }
                    } else {
                        //RubinoToast.showToast(getActivity(),getString(R.string.error), Toast.LENGTH_SHORT,RubinoToast.warning);
                        login(2);
                    }
                }
            }

            @Override
            public void onFailure(Call<PostChangeCode_request> call, Throwable t) {
                textView_update.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.INVISIBLE);
                RubinoToast.showToast(getActivity(),getString(R.string.error), Toast.LENGTH_SHORT,RubinoToast.warning);
            }
        });
    }
    private void sendCodeAndData(String code) {
        PostChangeSender postChangeSender = new PostChangeSender();
        postChangeSender.setCellNumber(MyApp.getMemory().getString(Constants.username,""));
        postChangeSender.setChangeCode(code);
        postChangeSender.setStoreTitle(editText_marketName.getText().toString());
        postChangeSender.setFirstName(editText_managerName.getText().toString());
        postChangeSender.setLastName(editText_managerFamily.getText().toString());
        postChangeSender.setCityId(lastCity.getId());
        postChangeSender.setEmail(editText_email.getText().toString());

        Call<PostChangeCode_request> postChangeCode_requestCall = apiService.PostChangeByCode(MyApp.getMemory().getString(Constants.tokenId, ""), postChangeSender);
        postChangeCode_requestCall.enqueue(new Callback<PostChangeCode_request>() {
            @Override
            public void onResponse(Call<PostChangeCode_request> call, Response<PostChangeCode_request> response) {
                if (isLife) {
                    if (response.body() != null) {
                        if (response.body().getStatus() != null) {
                            if (response.body().getStatus().getIsSuccess()) {
                                showDialogSms.dismiss();
                                getProfile();
                                RubinoToast.showToast(getActivity(), response.body().getStatus().getMessage(), Toast.LENGTH_SHORT, RubinoToast.warning);
                            } else {
                                RubinoToast.showToast(getActivity(), response.body().getStatus().getMessage(), Toast.LENGTH_SHORT, RubinoToast.warning);
                            }
                        } else {
                            RubinoToast.showToast(getActivity(), getString(R.string.error), Toast.LENGTH_SHORT, RubinoToast.warning);
                        }
                    } else {
                        //RubinoToast.showToast(getActivity(),getString(R.string.error), Toast.LENGTH_SHORT,RubinoToast.warning);
                        login(3);
                    }
                }
            }

            @Override
            public void onFailure(Call<PostChangeCode_request> call, Throwable t) {
                RubinoToast.showToast(getActivity(),getString(R.string.error), Toast.LENGTH_SHORT,RubinoToast.warning);
            }
        });

    }
    private void login(final int retroId){
        materialProgressBar.setVisibility(View.VISIBLE);
        APIService apiService = ApiUtils.getAPIService();
        Call<LoginRequest> login = apiService.login(MyApp.getMemory().getString(Constants.username, ""), MyApp.getMemory().getString(Constants.password, ""));
        login.enqueue(new Callback<LoginRequest>() {
            @Override
            public void onResponse(Call<LoginRequest> call, Response<LoginRequest> response) {
                if (response.body()!=null){
                    if (response.body().getStatus().getIsSuccess()){
                        if (isLife) {
                            MyApp.getMemory().edit().putString(Constants.tokenId, response.body().getResult().getTokenId()).commit();
                            switch (retroId) {
                                case 1:
                                    getProfile();
                                    break;
                                case 2:
                                    getSmsCode();
                                    break;
                                case 3:
                                    sendCodeAndData(code);
                                    break;
                            }
                        }
                    }else{
                        RubinoToast.showToast(getActivity(),response.body().getStatus().getMessage()+"...", Toast.LENGTH_SHORT,RubinoToast.warning);
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginRequest> call, Throwable t) {
                RubinoToast.showToast(getActivity(),getString(R.string.error), Toast.LENGTH_SHORT,RubinoToast.warning);
            }
        });
    }
    //------------retro-----

    private void setData(Result result) {
        if (result.getFirstName()!= null) {
            editText_managerName.setText(result.getFirstName());
        }
        if (result.getLastName() != null) {
            editText_managerFamily.setText(result.getLastName());
        }
        if (result.getStoreTitle() != null) {
            editText_marketName.setText(result.getStoreTitle());
        }
        if (result.getEmail() != null) {
            editText_email.setText(result.getEmail());
        }
        if (result.getCellNumber() != null) {
            editText_phone_mobile.setText(result.getCellNumber());
        }
        List<Province> provinceList = result.getProvinces();

        provinces.clear();
        for (int i = 0; i < provinceList.size(); i++) {
            provinces.add(provinceList.get(i));
        }


        allCities = result.getCities();

        String provinceId = result.getProvinceId();
        String cityId = result.getCityId();


        for (int i = 0; i < this.provinces.size(); i++) {
            lastProvince = this.provinces.get(i);
            if (lastProvince.getId().equals(provinceId)){
                editText_state.setText(lastProvince.getTitle());
                break;
            }
        }

        changeCitiesList(lastProvince.getId());

        for (int i = 0; i < this.cities.size(); i++) {
            lastCity = this.cities.get(i);
            if (lastCity.getId().equals(cityId)){
                editText_city.setText(lastCity.getTitle());
                break;
            }
        }


        //adapterSpinner.notifyDataSetChanged();
    }

    public void changeCitiesList(String pId){
        cities.clear();
        for (int i = 0; i < allCities.size(); i++) {
            City city = allCities.get(i);
            String provinceId = city.getProvinceId();
            if (pId.equals(provinceId)){
                cities.add(city);
            }
        }
        adapterSpinnerCity.notifyDataSetChanged();
    };

    public final static boolean isValidEmail(CharSequence target) {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    private void internetChecker() {
        boolean online = InternetChecker.isOnline(getActivity());
        if (online){
            if (!BaseActivity.isFreezed) {
                BaseActivity.freeze(getActivity());
            }
            getProfile();
        }else{
            RubinoToast.showToast(getActivity(),getString(R.string.checkNetwork), Toast.LENGTH_SHORT,RubinoToast.warning);
            refreshLayout.setRefreshing(false);
        }
    }

    private void keyboard (){
        editText_managerName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    Constants.hideKeyboard(getActivity(),v);
                }
            }
        });
        editText_managerFamily.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    Constants.hideKeyboard(getActivity(),v);
                }
            }
        });
        editText_marketName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    Constants.hideKeyboard(getActivity(),v);
                }
            }
        });
        editText_email.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    Constants.hideKeyboard(getActivity(),v);
                }
            }
        });
        editText_phone_mobile.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    Constants.hideKeyboard(getActivity(),v);
                }
            }
        });
        editText_state.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    Constants.hideKeyboard(getActivity(),v);
                }
            }
        });
        editText_city.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    Constants.hideKeyboard(getActivity(),v);
                }
            }
        });
    }
}
