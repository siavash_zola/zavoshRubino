package com.example.mrasus.rubino.Fragments;


import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mrasus.rubino.Adapters.RecyclerViewAdapterRequestTableShow;
import com.example.mrasus.rubino.Constants.Attach;
import com.example.mrasus.rubino.Constants.Constants;
import com.example.mrasus.rubino.Constants.InternetChecker;
import com.example.mrasus.rubino.Constants.Product;
import com.example.mrasus.rubino.Database.MyApp;
import com.example.mrasus.rubino.MyToast.RubinoToast;
import com.example.mrasus.rubino.R;
import com.example.mrasus.rubino.Retrofit.APIService;
import com.example.mrasus.rubino.Retrofit.ApiUtils;
import com.example.mrasus.rubino.Retrofit.HistoryOrder_Request.HistoryOrderRequest;
import com.example.mrasus.rubino.Retrofit.HistoryOrder_Request.HistoryOrderSender;
import com.example.mrasus.rubino.Retrofit.HistoryOrder_Request.OrderAttachment;
import com.example.mrasus.rubino.Retrofit.HistoryOrder_Request.OrderDetailItem;
import com.example.mrasus.rubino.Retrofit.HistoryOrder_Request.Result;
import com.example.mrasus.rubino.Retrofit.Login_Request.LoginRequest;
import com.example.mrasus.rubino.activities.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import me.zhanghai.android.materialprogressbar.MaterialProgressBar;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment_ShowRequestProfile extends Fragment {

    private RecyclerView recyclerView_showRequestProfile;
    private LinearLayoutManager layoutManager;
    private RecyclerViewAdapterRequestTableShow recyclerViewAdapterRequestTableShow;
    private String orderCode;
    private List<Product> productList;
    private List<Attach> attachList;
    private MaterialProgressBar materialProgressBar;
    private boolean isLife = true;

    public Fragment_ShowRequestProfile() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fragment__show_request_profile, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        toolbar();
        reference(view);
    }

    @Override
    public void onPause() {
        super.onPause();
        isLife = false;
        materialProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void onResume() {
        super.onResume();
        isLife = true;
    }

    private void toolbar() {
        TextView textView = getActivity().findViewById(R.id.textView_toolbar);
        textView.setText(getString(R.string.factor));
    }
    private void reference(View view) {
        orderCode = getArguments().getString("orderCode");
        productList = new ArrayList<>();
        attachList = new ArrayList<>();
        materialProgressBar = getActivity().findViewById(R.id.materialProgressBar);
        recyclerView_showRequestProfile = view.findViewById(R.id.recyclerView_showRequestProfile);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView_showRequestProfile.setLayoutManager(layoutManager);
        //recyclerViewAdapterRequestTableShow = new RecyclerViewAdapterRequestTableShow(activity,productList,attachList,"","","","","",orderCode);
        //recyclerViewAdapterRequestTableShow = new RecyclerViewAdapterRequestTableShow(activity,productList,attachList,paymentType,transferType,address,priceORCargoCode,date,orderCode);
        internetChecker();


    }
    //---------retrofit--------------
    private void requestToServer() {
        materialProgressBar.setVisibility(View.VISIBLE);
        APIService apiService = ApiUtils.getAPIService();
        HistoryOrderSender historyOrderSender = new HistoryOrderSender();
        historyOrderSender.setOrderCode(orderCode);
        Call<HistoryOrderRequest> historyOrderRequestCall = apiService.orderHistory(MyApp.getMemory().getString(Constants.tokenId, ""), historyOrderSender);
        historyOrderRequestCall.enqueue(new Callback<HistoryOrderRequest>() {
            @Override
            public void onResponse(Call<HistoryOrderRequest> call, Response<HistoryOrderRequest> response) {
                materialProgressBar.setVisibility(View.GONE);
                if (isLife) {
                    if (response.body() != null) {
                        if (response.body().getStatus().getIsSuccess()) {
                            BaseActivity.unFreeze();
                            HistoryOrderRequest historyOrderRequest = response.body();
                            Result result = historyOrderRequest.getResult();
                            productList = getProductList(result.getOrderDetailItem());
                            attachList = getAttachList(result.getOrderAttachments());
                            recyclerViewAdapterRequestTableShow = new RecyclerViewAdapterRequestTableShow(getActivity(), productList, attachList, result.getPaymentType(), result.getShippmentType(), result.getAddress(), result.getShippmentTitle(), result.getSubmitDate(), result.getDescription(), orderCode);
                            recyclerView_showRequestProfile.setAdapter(recyclerViewAdapterRequestTableShow);
                        } else {
                            RubinoToast.showToast(getActivity(), response.body().getStatus().getMessage(), Toast.LENGTH_SHORT, RubinoToast.warning);
                            BaseActivity.unFreeze();
                        }
                    } else {
                        //RubinoToast.showToast(getActivity(),getString(R.string.error), Toast.LENGTH_SHORT,RubinoToast.warning);
                        login();
                    }
                }
            }

            @Override
            public void onFailure(Call<HistoryOrderRequest> call, Throwable t) {
                materialProgressBar.setVisibility(View.GONE);
                RubinoToast.showToast(getActivity(),getString(R.string.error), Toast.LENGTH_SHORT,RubinoToast.warning);
            }
        });
    }
    private void login(){
        materialProgressBar.setVisibility(View.VISIBLE);
        APIService apiService = ApiUtils.getAPIService();
        Call<LoginRequest> login = apiService.login(MyApp.getMemory().getString(Constants.username, ""), MyApp.getMemory().getString(Constants.password, ""));
        login.enqueue(new Callback<LoginRequest>() {
            @Override
            public void onResponse(Call<LoginRequest> call, Response<LoginRequest> response) {
                if (response.body()!=null){
                    if (response.body().getStatus().getIsSuccess()){
                        if (isLife) {
                            MyApp.getMemory().edit().putString(Constants.tokenId, response.body().getResult().getTokenId()).commit();
                            requestToServer();
                        }
                    }else{
                        RubinoToast.showToast(getActivity(),response.body().getStatus().getMessage(), Toast.LENGTH_SHORT,RubinoToast.warning);
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginRequest> call, Throwable t) {
                RubinoToast.showToast(getActivity(),getString(R.string.error), Toast.LENGTH_SHORT,RubinoToast.warning);
            }
        });
    }
    //---------retrofit--------------
    private List<Attach> getAttachList(List<OrderAttachment> orderAttachments) {
        List<Attach> list = new ArrayList<>();
        for (int i = 0; i < orderAttachments.size(); i++) {
            Attach attach = new Attach();
            OrderAttachment orderAttachment = orderAttachments.get(i);
            attach.setAttachName(orderAttachment.getTitle());
            attach.setAttachUrl(orderAttachment.getLinkAddress());
            list.add(attach);
        }
        return list;
    }

    private List<Product> getProductList(List<OrderDetailItem> orderDetailItem) {
        List<Product> list = new ArrayList<>();
        for (int i = 0; i < orderDetailItem.size(); i++) {
            Product product = new Product();
            OrderDetailItem orderDetailItem1 = orderDetailItem.get(i);
            product.setNumber(orderDetailItem1.getQuantity());
            product.setTitle(orderDetailItem1.getProductTitle());
            product.setPrice(orderDetailItem1.getPrice()+"");
            list.add(product);
        }
        return list;
    }
    private void internetChecker() {
        boolean online = InternetChecker.isOnline(getActivity());
        if (online){
            if (!BaseActivity.isFreezed) {
                BaseActivity.freeze(getActivity());
            }
            requestToServer();
        }else{
            RubinoToast.showToast(getActivity(),getString(R.string.checkNetwork), Toast.LENGTH_SHORT,RubinoToast.warning);
        }
    }
}
