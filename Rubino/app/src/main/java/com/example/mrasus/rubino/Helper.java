package com.example.mrasus.rubino;

import android.net.Uri;

/**
 * Created by Mr.Asus on 8/15/2018.
 */

public interface Helper {
    interface ReloadList{
        void reload();
    }
    interface Browser{
        void setUri (Uri uri);
        void showChooserNow();
    }
}
