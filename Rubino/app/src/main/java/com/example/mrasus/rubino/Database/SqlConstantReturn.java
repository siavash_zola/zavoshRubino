package com.example.mrasus.rubino.Database;

/**
 * Created by Mr.Asus on 5/14/2018.
 */

public class SqlConstantReturn {
    public static final String TABLE_NAME = "RETURN_TABLE_NEW";
    public static final String COLUMN_0 = "_id";
    public static final String COLUMN_1  = "serial";
    public static final String COLUMN_2  = "reason";
    public static final String CREATE_RETURN_TABLE = "CREATE TABLE " + TABLE_NAME +"( _id INTEGER PRIMARY KEY AUTOINCREMENT,"
            +COLUMN_1 + " TEXT,"
            +COLUMN_2 +" TEXT);";
}
