package com.example.mrasus.rubino.Retrofit.OrderPost_Request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Mr.Asus on 5/22/2018.
 */

public class OrderPostSender {
    @SerializedName("orderDetailItems")
    @Expose
    private List<OrderDetailItem> orderDetailItems = null;
    @SerializedName("totalAmount")
    @Expose
    private Integer totalAmount;
    @SerializedName("shippmentType")
    @Expose
    private String shippmentType;
    @SerializedName("shippmentTitle")
    @Expose
    private String shippmentTitle;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("paymentType")
    @Expose
    private String paymentType;
    @SerializedName("desc")
    @Expose
    private String desc;
    @SerializedName("phone")
    @Expose
    private String phone;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public List<OrderDetailItem> getOrderDetailItems() {
        return orderDetailItems;
    }

    public void setOrderDetailItems(List<OrderDetailItem> orderDetailItems) {
        this.orderDetailItems = orderDetailItems;
    }

    public Integer getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Integer totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getShippmentType() {
        return shippmentType;
    }

    public void setShippmentType(String shippmentType) {
        this.shippmentType = shippmentType;
    }

    public String getShippmentTitle() {
        return shippmentTitle;
    }

    public void setShippmentTitle(String shippmentTitle) {
        this.shippmentTitle = shippmentTitle;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
