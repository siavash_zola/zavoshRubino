package com.example.mrasus.rubino.Retrofit.HistoryReturn_Request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Mr.Asus on 5/27/2018.
 */

public class HistoryReturnSender {
    @SerializedName("returnOrderCode")
    @Expose
    private String returnOrderCode;

    public String getReturnOrderCode() {
        return returnOrderCode;
    }

    public void setReturnOrderCode(String returnOrderCode) {
        this.returnOrderCode = returnOrderCode;
    }
}
