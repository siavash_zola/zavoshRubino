package com.example.mrasus.rubino.Adapters;

import android.Manifest;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mrasus.rubino.Constants.Constants;
import com.example.mrasus.rubino.Constants.InternetChecker;
import com.example.mrasus.rubino.Constants.MessageRubino;
import com.example.mrasus.rubino.CustomView.MyEditText;
import com.example.mrasus.rubino.Database.MyApp;
import com.example.mrasus.rubino.Helper;
import com.example.mrasus.rubino.MyToast.RubinoToast;
import com.example.mrasus.rubino.R;
import com.example.mrasus.rubino.Retrofit.APIService;
import com.example.mrasus.rubino.Retrofit.ApiUtils;
import com.example.mrasus.rubino.Retrofit.Login_Request.LoginRequest;
import com.example.mrasus.rubino.Retrofit.SendMessage_Request.SendMessageRequest;
import com.example.mrasus.rubino.Retrofit.SendMessage_Request.SendMessageSender;
import com.example.mrasus.rubino.activities.BaseActivity;
import com.example.mrasus.rubino.fileManagerHelper.afilechooser.utils.FileUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Mr.Asus on 8/15/2018.
 */

public class MessagePagerAdapter extends PagerAdapter implements Helper.Browser{
    private LinearLayoutManager layoutManager;
    private List<List<MessageRubino>> lists;
    private Activity activity;
    public Helper.ReloadList reloadList;

    private FloatingActionButton fab_addMessage;
    private RecyclerView recyclerView_sendMessage,recyclerView_receiveMessage;
    private TextView textView_no_message,textView_no_message0;
    private RecyclerViewAdapterSendMessage recyclerViewAdapterSendMessage;
    private AlertDialog showAddMeessageDialog;
    private TextView textView_filePath;
    private Uri uri = null;
    private RecyclerViewAdapterSendMessage recyclerViewAdapterReceiveMessage;

    public MessagePagerAdapter(List<List<MessageRubino>> lists, Activity activity) {
        this.lists = lists;
        this.activity = activity;
        BaseActivity.browser = this;

    }

    @Override
    public int getCount() {
        return lists.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View rootView = null;
        if (position == 0) {
            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rootView = inflater.inflate(R.layout.receive, null);
            container.addView(rootView);
            reference0(rootView,lists.get(0));
        }else {
            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rootView = inflater.inflate(R.layout.send, null);
            container.addView(rootView);
            reference1(rootView,lists.get(1));
        }

        return rootView;
    }
    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }
    private void reference1(View view,List<MessageRubino> list) {
        fab_addMessage = view.findViewById(R.id.fab_addMessage);
        recyclerView_sendMessage = view.findViewById(R.id.recyclerView_sendMessage);
        layoutManager = new LinearLayoutManager(activity);
        textView_no_message = view.findViewById(R.id.textView_no_message);
        recyclerViewAdapterSendMessage = new RecyclerViewAdapterSendMessage(list,activity);
        recyclerView_sendMessage.setLayoutManager(layoutManager);
        recyclerView_sendMessage.setAdapter(recyclerViewAdapterSendMessage);
        if (list.size() == 0){
            textView_no_message.setText(R.string.noMessage);
        }else {
            textView_no_message.setText("");
        }

        listener1();
    }
    private void reference0(View view,List<MessageRubino> list) {
        recyclerView_receiveMessage = view.findViewById(R.id.recyclerView_receiveMessage);
        textView_no_message0 = view.findViewById(R.id.textView_no_message);
        layoutManager = new LinearLayoutManager(activity);
        recyclerViewAdapterReceiveMessage = new RecyclerViewAdapterSendMessage(list,activity);
        recyclerView_receiveMessage.setLayoutManager(layoutManager);
        recyclerView_receiveMessage.setAdapter(recyclerViewAdapterReceiveMessage);
        if (list.size() == 0){
            textView_no_message0.setText(R.string.noMessage);
        }else {
            textView_no_message0.setText("");
        }


    }

    private void listener1() {
        fab_addMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createDialogAddMessage();
            }
        });
    }

    private void createDialogAddMessage() {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rootView = inflater.inflate(R.layout.dialog_add_message, null);
        builder.setView(rootView);
        builder.create();
        TextView textView_cancel = rootView.findViewById(R.id.textView_cancel);
        TextView textView_sendMessage = rootView.findViewById(R.id.textView_sendMessage);
        textView_filePath = rootView.findViewById(R.id.textView_filePath);
        ImageView imageView_attach = rootView.findViewById(R.id.imageView_attach);
        final MyEditText editText_subject = rootView.findViewById(R.id.editText_subject);
        final MyEditText editText_textMessage = rootView.findViewById(R.id.editText_textMessage);
        textView_cancel.setTypeface(BaseActivity.appFont);
        textView_filePath.setTypeface(BaseActivity.appFont);
        textView_sendMessage.setTypeface(BaseActivity.appFont);
        showAddMeessageDialog = builder.show();


        textView_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAddMeessageDialog.dismiss();
            }
        });

        textView_sendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String subject = editText_subject.text();
                String textMessage = editText_textMessage.text();
                if (textMessage.length()== 0 || subject.length() == 0){
                    RubinoToast.showToast(activity,activity.getString(R.string.completeForm), Toast.LENGTH_SHORT,RubinoToast.warning);
                }else{
                    showAddMeessageDialog.dismiss();
                    BaseActivity.freeze(activity);
                    internetChecker1(subject,textMessage);
                }

            }
        });
        editText_subject.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    Constants.hideKeyboard(activity,v);
                }
            }
        });
        editText_textMessage.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    Constants.hideKeyboard(activity,v);
                }
            }
        });
        imageView_attach.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showChooser();
            }
        });
    }
    private void showChooser() {
        // Use the GET_CONTENT intent from the utility class
        //Intent target = FileUtils.createGetContentIntent();
        Intent intent = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        // Create the chooser Intent
        //Intent intent = Intent.createChooser(target, "chooser_title");
        try {
            if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(activity,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},2);
                //ActivityCompat.requestPermissions(getActivity(),
                //        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                //        2);
            }else{
                activity.startActivityForResult(intent, 1);
            }

        } catch (ActivityNotFoundException e) {
            // The reason for the existence of aFileChooser
        }
    }



    //---------retrofit--------------
    private void sendMessageToServer(final String subject, final String textMessage) {
        APIService apiService = ApiUtils.getAPIService();
        SendMessageSender letter = new SendMessageSender();
        letter.setSubject(subject);
        letter.setBody(textMessage);
        if (uri != null) {
            File file = FileUtils.getFile(activity, uri);
            Bitmap bitmap = BitmapFactory.decodeFile(file.getPath());
            String encodedImage = Base64.encodeToString(getFileDataFromDrawable(bitmap), Base64.DEFAULT);
            letter.setImage(encodedImage);
        }else {
            letter.setImage("");
        }
        Call<SendMessageRequest> sendMessageRequestCall = apiService.sendMessage(MyApp.getMemory().getString(Constants.tokenId, ""), letter);
        sendMessageRequestCall.enqueue(new Callback<SendMessageRequest>() {
            @Override
            public void onResponse(Call<SendMessageRequest> call, Response<SendMessageRequest> response) {
                BaseActivity.unFreeze();
                    if (response.body() != null) {
                        if (response.body().getStatus().getIsSuccess()) {

                            if (response.body().getStatus().getIsSuccess()) {
                                RubinoToast.showToast(activity, response.body().getResult(), Toast.LENGTH_SHORT, RubinoToast.warning);
                                reloadList.reload();
                                textView_no_message.setText("");
                            }
                        } else {
                            RubinoToast.showToast(activity, response.body().getStatus().getMessage(), Toast.LENGTH_SHORT, RubinoToast.warning);
                        }

                    } else {
                        //RubinoToast.showToast(getActivity(),getString(R.string.error), Toast.LENGTH_SHORT,RubinoToast.warning);
                        login(subject, textMessage);
                    }

            }

            @Override
            public void onFailure(Call<SendMessageRequest> call, Throwable t) {
                BaseActivity.unFreeze();
                RubinoToast.showToast(activity,activity.getString(R.string.error), Toast.LENGTH_SHORT,RubinoToast.warning);
            }
        });


    }
    private void login(final String subject, final String textMessage){
        APIService apiService = ApiUtils.getAPIService();
        Call<LoginRequest> login = apiService.login(MyApp.getMemory().getString(Constants.username, ""), MyApp.getMemory().getString(Constants.password, ""));
        login.enqueue(new Callback<LoginRequest>() {
            @Override
            public void onResponse(Call<LoginRequest> call, Response<LoginRequest> response) {
                if (response.body()!=null){
                    if (response.body().getStatus().getIsSuccess()){
                            MyApp.getMemory().edit().putString(Constants.tokenId, response.body().getResult().getTokenId()).commit();
                            sendMessageToServer(subject, textMessage);
                    }else{
                        RubinoToast.showToast(activity,response.body().getStatus().getMessage(), Toast.LENGTH_SHORT,RubinoToast.warning);
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginRequest> call, Throwable t) {
                RubinoToast.showToast(activity,activity.getString(R.string.error), Toast.LENGTH_SHORT,RubinoToast.warning);
            }
        });
    }
    //---------retrofit--------------
    private void internetChecker1(String subject, String textMessage) {
        boolean online = InternetChecker.isOnline(activity);
        if (online){
            sendMessageToServer(subject,textMessage);
        }else{
            RubinoToast.showToast(activity,activity.getString(R.string.checkNetwork), Toast.LENGTH_SHORT,RubinoToast.warning);
        }
    }
    public byte[] getFileDataFromDrawable(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }
    @Override
    public void setUri(Uri uri) {
        Log.i("kk","setUri");
        this.uri = uri;
        String path = FileUtils.getPath(activity, uri);
        textView_filePath.setText(path);
    }

    @Override
    public void showChooserNow() {
        showChooser();
    }
}
