package com.example.mrasus.rubino.Adapters;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.mrasus.rubino.Constants.ProductGroup;
import com.example.mrasus.rubino.Fragments.Fragment_ShowProduct;
import com.example.mrasus.rubino.R;
import com.example.mrasus.rubino.Retrofit.ApiUtils;
import com.example.mrasus.rubino.activities.BaseActivity;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Mr.Asus on 4/29/2018.
 */

public class RecyclerViewAdapterHome extends RecyclerView.Adapter{
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_FOOTER = 1;
    private static final int TYPE_ITEM = 2;
    private List<ProductGroup> products_list;
    private Activity activity;
    public RecyclerViewAdapterHome(Activity activity, List<ProductGroup> data) {
        this.activity = activity;
        this.products_list = data;
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM){
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_room, parent, false);
            return new ItemViewHolder(itemView);
        }else {
            return null;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof ItemViewHolder){
            ItemViewHolder itemViewHolder = (ItemViewHolder)holder;
            final ProductGroup productGroup = products_list.get(position);

            itemViewHolder.textViewProductName.setText(productGroup.getProductName());
            Picasso.with(activity)
                    .load(productGroup.getImageUrl())
                    .placeholder(R.drawable.pre_view_picaso)
                    .error(R.drawable.pre_view_picaso)
                    .into(itemViewHolder.imageViewProduct);
            itemViewHolder.linearLayout_room.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Bundle bundle = new Bundle();
                    bundle.putString("ProductName",productGroup.getProductName());
                    bundle.putString("ProductId",productGroup.getProductId());
                    Fragment fragment_showProduct = new Fragment_ShowProduct();
                    fragment_showProduct.setArguments(bundle);
                    android.support.v4.app.FragmentTransaction fragmentTransaction = BaseActivity.fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.frame, fragment_showProduct ).addToBackStack( "tag" ).commit();
            }
            });

        }
    }

    @Override
    public int getItemCount() {
        return products_list.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == products_list.size()){
            return TYPE_FOOTER;
        }else {
            return TYPE_ITEM;
        }
    }

    private class ItemViewHolder extends RecyclerView.ViewHolder {
        ImageView imageViewProduct;
        TextView textViewProductName;
        LinearLayout linearLayout_room;

        public ItemViewHolder(View itemView) {
            super(itemView);
            imageViewProduct = itemView.findViewById(R.id.imageViewProduct);
            textViewProductName = itemView.findViewById(R.id.textViewProductName);
            linearLayout_room = itemView.findViewById(R.id.linearLayout_room);
            textViewProductName.setTypeface(BaseActivity.appFont_Bold);

        }
    }
}
