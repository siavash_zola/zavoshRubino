package com.example.mrasus.rubino.Adapters;

import android.app.Activity;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mrasus.rubino.Constants.MessageRubino;
import com.example.mrasus.rubino.MyToast.RubinoToast;
import com.example.mrasus.rubino.R;
import com.example.mrasus.rubino.activities.BaseActivity;
import com.squareup.picasso.Picasso;

import java.util.List;

import static com.example.mrasus.rubino.Database.MyApp.context;

/**
 * Created by Mr.Asus on 5/16/2018.
 */

public class RecyclerViewAdapterSendMessage extends RecyclerView.Adapter {
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_FOOTER = 1;
    private static final int TYPE_ITEM = 2;
    private List<MessageRubino> messageList;
    private Activity activity;
    private AlertDialog showMeessageDialog;

    public RecyclerViewAdapterSendMessage(List<MessageRubino> messageList, Activity activity) {
        this.messageList = messageList;
        this.activity = activity;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM){
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.message_send_room, parent, false);
            return new ItemViewHolder(itemView);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ItemViewHolder){
            ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
            final MessageRubino messageRubino = messageList.get(position);
            messageRubino.getSubject();
            itemViewHolder.textView_date.setText(messageRubino.getDate());
            itemViewHolder.textView_subject.setText(messageRubino.getSubject());


            itemViewHolder.linearLayout_message.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //RubinoToast.showToast(activity,"Comming Soon", Toast.LENGTH_SHORT,RubinoToast.information);
                    createDialogShowMessage(messageRubino);
                }
            });
        }
    }

    private void createDialogShowMessage(final MessageRubino messageRubino) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        final LayoutInflater inflater = (LayoutInflater) activity.getSystemService(activity.LAYOUT_INFLATER_SERVICE);
        View rootView = inflater.inflate(R.layout.dialog_show_message, null);

        TextView textView_textMessage = rootView.findViewById(R.id.textView_textMessage);
        TextView textView_subject = rootView.findViewById(R.id.textView_subject);
        TextView textView_date = rootView.findViewById(R.id.textView_date);
        LinearLayout linearLayout_cancel =rootView.findViewById(R.id.linearLayout_cancel);
        textView_textMessage.setTypeface(BaseActivity.appFont);
        textView_subject.setTypeface(BaseActivity.appFont);
        textView_date.setTypeface(BaseActivity.appFont);
        LinearLayout linearLayout_attach = rootView.findViewById(R.id.linearLayout_attach_Sms);
        TextView textAttach = rootView.findViewById(R.id.textAttach);
        textAttach.setTypeface(BaseActivity.appFont);


        textView_subject.setText(messageRubino.getSubject());
        textView_textMessage.setText(messageRubino.getTextMessage());
        textView_date.setText(messageRubino.getDate());


        if (messageRubino.getAttachUrl()!=null){
            Log.i("kk",messageRubino.getAttachUrl());
            linearLayout_attach.setVisibility(View.VISIBLE);
        }else{
            linearLayout_attach.setVisibility(View.GONE);
        }


        builder.setView(rootView);
        builder.create();

        linearLayout_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showMeessageDialog.dismiss();
            }
        });
        linearLayout_attach.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showImageAttach(messageRubino.getAttachUrl());
            }


        });

        showMeessageDialog = builder.show();


    }

    @Override
    public int getItemCount() {
        return messageList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return TYPE_ITEM;
    }

    private class ItemViewHolder extends RecyclerView.ViewHolder {
        TextView textView_date,textView_subject;
        LinearLayout linearLayout_message;
        public ItemViewHolder(View itemView) {
            super(itemView);
            textView_date = itemView.findViewById(R.id.textView_date);
            textView_subject = itemView.findViewById(R.id.textView_subject);
            linearLayout_message = itemView.findViewById(R.id.linearLayout_message);

            textView_date.setTypeface(BaseActivity.appFont);
            textView_subject.setTypeface(BaseActivity.appFont);
        }
    }
    private void showImageAttach(final String url) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        final AlertDialog dialog = builder.create();
        LayoutInflater inflater = activity.getLayoutInflater();
        View dialogLayout = inflater.inflate(R.layout.dialog_show_image, null);
        dialog.setView(dialogLayout);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        ImageView image = (ImageView) dialogLayout.findViewById(R.id.goProDialogImage);
        LinearLayout linear_close = (LinearLayout) dialogLayout.findViewById(R.id.linear_close);
        Picasso.with(activity)
                .load(url)
                .into(image);
        dialog.show();

        linear_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });


    }
}
