package com.example.mrasus.rubino.Fragments.messages;


import android.Manifest;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mrasus.rubino.Adapters.RecyclerViewAdapterSendMessage;
import com.example.mrasus.rubino.Constants.Constants;
import com.example.mrasus.rubino.Constants.InternetChecker;
import com.example.mrasus.rubino.Constants.MessageRubino;
import com.example.mrasus.rubino.CustomView.MyEditText;
import com.example.mrasus.rubino.Database.MyApp;
import com.example.mrasus.rubino.MyToast.RubinoToast;
import com.example.mrasus.rubino.R;
import com.example.mrasus.rubino.Retrofit.APIService;
import com.example.mrasus.rubino.Retrofit.ApiUtils;
import com.example.mrasus.rubino.Retrofit.GetAllMessage_Request.GetAllMessageRequest;
import com.example.mrasus.rubino.Retrofit.GetAllMessage_Request.Result;
import com.example.mrasus.rubino.Retrofit.Login_Request.LoginRequest;
import com.example.mrasus.rubino.Retrofit.SendMessage_Request.SendMessageRequest;
import com.example.mrasus.rubino.Retrofit.SendMessage_Request.SendMessageSender;
import com.example.mrasus.rubino.activities.BaseActivity;
import com.example.mrasus.rubino.activities.LoginActivity;
import com.example.mrasus.rubino.fileManagerHelper.afilechooser.utils.FileUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import me.zhanghai.android.materialprogressbar.MaterialProgressBar;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 * avalin kar ke mikone useresho mide server
 * dar javab tamame message hai ke ferestadaro migire
 * too list neshoon midim
 */
public class Fragment_SendMessage extends Fragment {
    private FloatingActionButton fab_addMessage;
    private AlertDialog showAddMeessageDialog;
    private RecyclerView recyclerView_sendMessage;
    private RecyclerViewAdapterSendMessage recyclerViewAdapterSendMessage;
    private List<MessageRubino> messageRubinoList;
    private LinearLayoutManager layoutManager;
    private MaterialProgressBar materialProgressBar;
    private TextView textView_no_message;
    private boolean isLife =true;
    private int ATTACH_FILE_REQUEST =10;
    private TextView textView_filePath;
    public File file;
    private Uri uri;



    public Fragment_SendMessage() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_fragment__send_message, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Toast.makeText(getActivity(),"Created",Toast.LENGTH_SHORT).show();
        reference(view);
        listeners();
    }

    @Override
    public void onPause() {
        super.onPause();
        Toast.makeText(getActivity(),"onPause",Toast.LENGTH_SHORT).show();
        isLife = false;
        materialProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void onResume() {
        Toast.makeText(getActivity(),"onResume",Toast.LENGTH_SHORT).show();
        super.onResume();
        isLife = true;
    }

    private void listeners() {
        fab_addMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createDialogAddMessage();
            }
        });
    }

    private void showChooser() {
        // Use the GET_CONTENT intent from the utility class
        //Intent target = FileUtils.createGetContentIntent();
        Intent intent = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        // Create the chooser Intent
        //Intent intent = Intent.createChooser(target, "chooser_title");
        try {
            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},ATTACH_FILE_REQUEST);
                //ActivityCompat.requestPermissions(getActivity(),
                //        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                //        2);
            }else{
                getActivity().startActivityForResult(intent, ATTACH_FILE_REQUEST);
            }

        } catch (ActivityNotFoundException e) {
            // The reason for the existence of aFileChooser
        }
    }

    private void createDialogAddMessage() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rootView = inflater.inflate(R.layout.dialog_add_message, null);
        builder.setView(rootView);
        builder.create();
        TextView textView_cancel = rootView.findViewById(R.id.textView_cancel);
        TextView textView_sendMessage = rootView.findViewById(R.id.textView_sendMessage);
        textView_filePath = rootView.findViewById(R.id.textView_filePath);
        ImageView imageView_attach = rootView.findViewById(R.id.imageView_attach);
        final MyEditText editText_subject = rootView.findViewById(R.id.editText_subject);
        final MyEditText editText_textMessage = rootView.findViewById(R.id.editText_textMessage);
        textView_cancel.setTypeface(BaseActivity.appFont);
        textView_filePath.setTypeface(BaseActivity.appFont);
        textView_sendMessage.setTypeface(BaseActivity.appFont);
        showAddMeessageDialog = builder.show();


        textView_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAddMeessageDialog.dismiss();
            }
        });

        textView_sendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String subject = editText_subject.text();
                String textMessage = editText_textMessage.text();
                if (textMessage.length()== 0 || subject.length() == 0){
                    RubinoToast.showToast(getActivity(),getString(R.string.completeForm), Toast.LENGTH_SHORT,RubinoToast.warning);
                }else{
                    showAddMeessageDialog.dismiss();
                    BaseActivity.freeze(getActivity());
                    internetChecker(subject,textMessage);
                }

            }
        });
        editText_subject.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    Constants.hideKeyboard(getActivity(),v);
                }
            }
        });
        editText_textMessage.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    Constants.hideKeyboard(getActivity(),v);
                }
            }
        });
        imageView_attach.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showChooser();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // If the file selection was successful
        if (requestCode == 65 && resultCode == RESULT_OK) {
            if (data != null) {
                // Get the URI of the selected file
                uri = data.getData();
                try {
                    // Get the file path from the URI
                    final String path = FileUtils.getPath(getActivity(), uri);
                    //Toast.makeText(getActivity(), "File Selected: " + path, Toast.LENGTH_LONG).show();
                    textView_filePath.setText(path);
                    //File file = new File(path);

                    //Picasso.with(MainActivity.this)
                    //        .load(file)
                    //        .placeholder(R.drawable.ic_file)
                    //        .error(R.drawable.ic_provider)
                    //        .into(imageView);


                } catch (Exception e) {
                }
            }else{
            }
        }else {
        }
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == ATTACH_FILE_REQUEST && grantResults.length > 0&& grantResults[0] == PackageManager.PERMISSION_GRANTED){
            Toast.makeText(getActivity(),"oomad",Toast.LENGTH_SHORT).show();
            showChooser();
        }

    }


    //---------retrofit--------------
    private void sendMessageToServer(final String subject, final String textMessage) {
        materialProgressBar.setVisibility(View.VISIBLE);
        APIService apiService = ApiUtils.getAPIService();
        SendMessageSender letter = new SendMessageSender();
        letter.setSubject(subject);
        letter.setBody(textMessage);
        if (uri != null) {
            File file = FileUtils.getFile(getActivity(), uri);
            Bitmap bitmap = BitmapFactory.decodeFile(file.getPath());
            String encodedImage = Base64.encodeToString(getFileDataFromDrawable(bitmap), Base64.DEFAULT);
            letter.setImage(encodedImage);
        }else {
            letter.setImage("");
        }
        Call<SendMessageRequest> sendMessageRequestCall = apiService.sendMessage(MyApp.getMemory().getString(Constants.tokenId, ""), letter);
        sendMessageRequestCall.enqueue(new Callback<SendMessageRequest>() {
            @Override
            public void onResponse(Call<SendMessageRequest> call, Response<SendMessageRequest> response) {
                materialProgressBar.setVisibility(View.INVISIBLE);
                BaseActivity.unFreeze();
                if (isLife) {
                    if (response.body() != null) {
                        if (response.body().getStatus().getIsSuccess()) {

                            if (response.body().getStatus().getIsSuccess()) {
                                RubinoToast.showToast(getActivity(), response.body().getResult(), Toast.LENGTH_SHORT, RubinoToast.warning);
                                reloadList();
                                textView_no_message.setText("");
                            }
                        } else {
                            RubinoToast.showToast(getActivity(), response.body().getStatus().getMessage(), Toast.LENGTH_SHORT, RubinoToast.warning);
                        }

                    } else {
                        //RubinoToast.showToast(getActivity(),getString(R.string.error), Toast.LENGTH_SHORT,RubinoToast.warning);
                        login(subject, textMessage);
                    }
                }
            }

            @Override
            public void onFailure(Call<SendMessageRequest> call, Throwable t) {
                materialProgressBar.setVisibility(View.INVISIBLE);
                BaseActivity.unFreeze();
                RubinoToast.showToast(getActivity(),getString(R.string.error), Toast.LENGTH_SHORT,RubinoToast.warning);
            }
        });


    }
    private void login(final String subject, final String textMessage){
        APIService apiService = ApiUtils.getAPIService();
        Call<LoginRequest> login = apiService.login(MyApp.getMemory().getString(Constants.username, ""), MyApp.getMemory().getString(Constants.password, ""));
        login.enqueue(new Callback<LoginRequest>() {
            @Override
            public void onResponse(Call<LoginRequest> call, Response<LoginRequest> response) {
                if (response.body()!=null){
                    if (response.body().getStatus().getIsSuccess()){
                        if (isLife) {
                            MyApp.getMemory().edit().putString(Constants.tokenId, response.body().getResult().getTokenId()).commit();
                            sendMessageToServer(subject, textMessage);
                        }
                    }else{
                        RubinoToast.showToast(getActivity(),response.body().getStatus().getMessage(), Toast.LENGTH_SHORT,RubinoToast.warning);
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginRequest> call, Throwable t) {
                RubinoToast.showToast(getActivity(),getString(R.string.error), Toast.LENGTH_SHORT,RubinoToast.warning);
            }
        });
    }
    //---------retrofit--------------

    private void reference(View view) {
        messageRubinoList = new ArrayList<>();
        fromText(getArguments().getString("list"));
        materialProgressBar = getActivity().findViewById(R.id.materialProgressBar);
        fab_addMessage = view.findViewById(R.id.fab_addMessage);
        recyclerView_sendMessage = view.findViewById(R.id.recyclerView_sendMessage);
        textView_no_message = view.findViewById(R.id.textView_no_message);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerViewAdapterSendMessage = new RecyclerViewAdapterSendMessage(messageRubinoList,getActivity());
        recyclerView_sendMessage.setLayoutManager(layoutManager);
        recyclerView_sendMessage.setAdapter(recyclerViewAdapterSendMessage);
        if (messageRubinoList.size() == 0){
            textView_no_message.setText(R.string.noMessage);
        }
    }

    private void internetChecker(String subject, String textMessage) {
        boolean online = InternetChecker.isOnline(getActivity());
        if (online){
            sendMessageToServer(subject,textMessage);
        }else{
            RubinoToast.showToast(getActivity(),getString(R.string.checkNetwork), Toast.LENGTH_SHORT,RubinoToast.warning);
        }
    }

    private void reloadList(){
        materialProgressBar.setVisibility(View.VISIBLE);
        APIService apiService = ApiUtils.getAPIService();
        Call<GetAllMessageRequest> allMessage = apiService.getAllMessage(MyApp.getMemory().getString(Constants.tokenId, ""));
        allMessage.enqueue(new Callback<GetAllMessageRequest>() {
            @Override
            public void onResponse(Call<GetAllMessageRequest> call, Response<GetAllMessageRequest> response) {
                materialProgressBar.setVisibility(View.INVISIBLE);
                if (response.body() != null){
                    if (response.body().getStatus().getIsSuccess()){
                        messageRubinoList.clear();
                        List<Result> result = response.body().getResult();
                        for (int i = 0; i < result.size(); i++) {
                            Result result1 = result.get(i);
                            Integer typeCode = result1.getTypeCode();
                            MessageRubino messageRubino = new MessageRubino();
                            messageRubino.setDate(result1.getSubmitDate());
                            messageRubino.setSubject(result1.getSubject());
                            messageRubino.setTextMessage(result1.getBody());
                            messageRubino.setAttachUrl(result1.getAttachmentUrl());
                            if (typeCode == 1){
                                messageRubinoList.add(messageRubino);
                            }
                            recyclerViewAdapterSendMessage.notifyDataSetChanged();
                        }
                    }else {
                        RubinoToast.showToast(getActivity(),response.body().getStatus().getMessage(), Toast.LENGTH_SHORT,RubinoToast.warning);
                    }

                }else {
                    RubinoToast.showToast(getActivity(),getString(R.string.error), Toast.LENGTH_SHORT,RubinoToast.warning);
                }

            }

            @Override
            public void onFailure(Call<GetAllMessageRequest> call, Throwable t) {
                materialProgressBar.setVisibility(View.INVISIBLE);

            }
        });
    }

    public void fromText (String s){
        try {
            JSONArray jsonArray = new JSONArray(s);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                MessageRubino messageRubino = new MessageRubino();

                messageRubino.setTextMessage(jsonObject.getString("textMessage"));
                messageRubino.setSubject(jsonObject.getString("subject"));
                messageRubino.setDate(jsonObject.getString("date"));
                messageRubino.setAttachUrl(jsonObject.getString("attachUrl"));
                messageRubinoList.add(messageRubino);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public byte[] getFileDataFromDrawable(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }
}
