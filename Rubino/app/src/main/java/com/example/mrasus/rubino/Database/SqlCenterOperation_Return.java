package com.example.mrasus.rubino.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.mrasus.rubino.Constants.ReturnProduct;
import com.example.mrasus.rubino.Retrofit.ReturnOrder_Reqestu.ReturnOrderDetailItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mr.Asus on 5/14/2018.
 */

public class SqlCenterOperation_Return implements CenterDAO_Return {
    private SQLiteDatabase db;
    private Context context;
    public SqlCenterOperation_Return(Context context) {
        MyDataBaseOpenHelper myDataBaseOpenHelper = new MyDataBaseOpenHelper(context);
        db = myDataBaseOpenHelper.getWritableDatabase();
        this.context = context;
    }
    @Override
    public void addProduct(ReturnProduct returnProduct) {
        String reason = returnProduct.getReason();
        String serial = returnProduct.getSerial();
        ContentValues values = new ContentValues();
        values.put(SqlConstantReturn.COLUMN_1,serial);
        values.put(SqlConstantReturn.COLUMN_2,reason);
        db.insert(SqlConstantReturn.TABLE_NAME, null, values);
        //Log.i("db","add");
    }

    @Override
    public void deleteProduct(ReturnProduct returnProduct) {
        db.delete(SqlConstantReturn.TABLE_NAME, "_id = ?", new String[]{""+returnProduct.getId()});
        //Log.i("db","delete");
    }

    @Override
    public List<ReturnProduct> getAllProductSelectedForReturn() {
        List<ReturnProduct> returnProductList = new ArrayList<>();
        Cursor query = db.query(SqlConstantReturn.TABLE_NAME, null, null, null, null, null, null);
        while (query.moveToNext()){
            ReturnProduct returnProduct = new ReturnProduct();
            returnProduct.setId(query.getInt(query.getColumnIndex(SqlConstantReturn.COLUMN_0)));
            returnProduct.setSerial(query.getString(query.getColumnIndex(SqlConstantReturn.COLUMN_1)));
            returnProduct.setReason(query.getString(query.getColumnIndex(SqlConstantReturn.COLUMN_2)));
            returnProductList.add(returnProduct);
        }
        return returnProductList;
    }

    @Override
    public int getNumber() {
        return 0;
    }

    @Override
    public void clearTable() {
        db.delete(SqlConstantReturn.TABLE_NAME,null,null);
    }

    @Override
    public List<ReturnOrderDetailItem> getOrdersReturnFromDataBase() {
        List<ReturnOrderDetailItem> list = new ArrayList<>();
        Cursor query = db.query(SqlConstantReturn.TABLE_NAME, null, null, null, null, null, null);
        while (query.moveToNext()){
            ReturnOrderDetailItem returnOrderDetailItem = new ReturnOrderDetailItem();
            returnOrderDetailItem.setProductSerialNumber(query.getString(query.getColumnIndex(SqlConstantReturn.COLUMN_1)));
            returnOrderDetailItem.setReturnReason(query.getString(query.getColumnIndex(SqlConstantReturn.COLUMN_2)));
            list.add(returnOrderDetailItem);
        }
        return list;
    }
}
