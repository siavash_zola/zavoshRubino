package com.example.mrasus.rubino.Retrofit;

/**
 * Created by Mr.Asus on 5/20/2018.
 */

public class ApiUtils {
    private ApiUtils() {}

    //public static final String BASE_URL = "http://rubino.shebuy.ir/";
    public static final String BASE_URL = "http://api.rubinoco.com/";

    public static APIService getAPIService() {

        return RetrofitClient.getClient(BASE_URL).create(APIService.class);
    }

}
