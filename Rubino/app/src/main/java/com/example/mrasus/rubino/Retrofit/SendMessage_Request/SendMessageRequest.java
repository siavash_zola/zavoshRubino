package com.example.mrasus.rubino.Retrofit.SendMessage_Request;

import com.example.mrasus.rubino.Retrofit.Login_Request.Status;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Mr.Asus on 5/28/2018.
 */

public class SendMessageRequest {
    @SerializedName("result")
    @Expose
    private String result;
    @SerializedName("status")
    @Expose
    private Status status;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

}
