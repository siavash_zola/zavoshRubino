package com.example.mrasus.rubino.Retrofit.HistoryOrder_Request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Mr.Asus on 5/26/2018.
 */

public class OrderAttachment {
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("linkAddress")
    @Expose
    private String linkAddress;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLinkAddress() {
        return linkAddress;
    }

    public void setLinkAddress(String linkAddress) {
        this.linkAddress = linkAddress;
    }
}
