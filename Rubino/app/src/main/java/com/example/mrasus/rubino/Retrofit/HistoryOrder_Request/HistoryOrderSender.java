package com.example.mrasus.rubino.Retrofit.HistoryOrder_Request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Mr.Asus on 5/26/2018.
 */

public class HistoryOrderSender {
    @SerializedName("orderCode")
    @Expose
    private String orderCode;

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }
}
