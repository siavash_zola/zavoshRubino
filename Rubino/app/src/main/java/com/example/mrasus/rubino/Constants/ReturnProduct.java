package com.example.mrasus.rubino.Constants;

/**
 * Created by Mr.Asus on 5/13/2018.
 */

public class ReturnProduct {
    private String serial;
    private String reason;
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
