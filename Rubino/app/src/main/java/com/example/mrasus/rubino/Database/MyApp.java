package com.example.mrasus.rubino.Database;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.widget.LinearLayout;

import com.example.mrasus.rubino.Constants.Constants;
import com.example.mrasus.rubino.Retrofit.APIService;
import com.example.mrasus.rubino.Retrofit.ApiUtils;
import com.example.mrasus.rubino.Retrofit.Login_Request.LoginRequest;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Mr.Asus on 5/6/2018.
 */

public class MyApp extends Application {
    private static CenterDAO sqlCenterOperation;
    private static CenterDAO sqlCenterOperation_order;
    private static CenterDAO_Return sqlCenterOperation_Return;
    private static SharedPreferences sharedPreferences;
    public static Context context;
    public static Typeface appFont;

    public static List<String> filterLanguage;

    private String key_performances = "key_performances";
    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
        sqlCenterOperation = new SqlCenterOperation(this);
        sqlCenterOperation_order = new SqlCenterOperationOrder(this);
        sqlCenterOperation_Return = new SqlCenterOperation_Return(this);
        sharedPreferences = getSharedPreferences(key_performances, MODE_PRIVATE);
        appFont = Typeface.createFromAsset(getAssets(),"fonts/IRANSansMobile(FaNum)_Light.ttf");
        inputData();
    }

    public static CenterDAO getCenterDAO(){
        return sqlCenterOperation;
    }
    public static CenterDAO getCenterDAO_Order(){
        return sqlCenterOperation_order;
    }
    public static CenterDAO_Return getCenterDAO_Return(){
        return sqlCenterOperation_Return;
    }
    public static SharedPreferences getMemory(){
        return sharedPreferences;
    }


    public static boolean login(){
        final boolean[] bool = {false};
        APIService apiService = ApiUtils.getAPIService();
        Call<LoginRequest> login = apiService.login(getMemory().getString(Constants.username, ""), getMemory().getString(Constants.password, ""));
        login.enqueue(new Callback<LoginRequest>() {
            @Override
            public void onResponse(Call<LoginRequest> call, Response<LoginRequest> response) {
                if (response.body()!=null){
                    LoginRequest body = response.body();
                    String tokenId = body.getResult().getTokenId();
                    getMemory().edit().putString(Constants.tokenId,tokenId);
                    bool[0] = true;
                }
            }

            @Override
            public void onFailure(Call<LoginRequest> call, Throwable t) {

            }
        });
        return bool[0];
    }

    public void inputData(){
        filterLanguage = new ArrayList<>();
        filterLanguage.add("a");
        filterLanguage.add("b");
        filterLanguage.add("c");
        filterLanguage.add("d");
        filterLanguage.add("e");
        filterLanguage.add("f");
        filterLanguage.add("g");
        filterLanguage.add("h");
        filterLanguage.add("i");
        filterLanguage.add("j");
        filterLanguage.add("k");
        filterLanguage.add("l");
        filterLanguage.add("m");
        filterLanguage.add("n");
        filterLanguage.add("o");
        filterLanguage.add("p");
        filterLanguage.add("q");
        filterLanguage.add("r");
        filterLanguage.add("s");
        filterLanguage.add("t");
        filterLanguage.add("u");
        filterLanguage.add("v");
        filterLanguage.add("x");
        filterLanguage.add("y");
        filterLanguage.add("z");
        filterLanguage.add("A");
        filterLanguage.add("B");
        filterLanguage.add("C");
        filterLanguage.add("D");
        filterLanguage.add("E");
        filterLanguage.add("F");
        filterLanguage.add("G");
        filterLanguage.add("H");
        filterLanguage.add("I");
        filterLanguage.add("J");
        filterLanguage.add("K");
        filterLanguage.add("L");
        filterLanguage.add("M");
        filterLanguage.add("N");
        filterLanguage.add("O");
        filterLanguage.add("P");
        filterLanguage.add("Q");
        filterLanguage.add("R");
        filterLanguage.add("S");
        filterLanguage.add("T");
        filterLanguage.add("U");
        filterLanguage.add("V");
        filterLanguage.add("W");
        filterLanguage.add("X");
        filterLanguage.add("Y");
        filterLanguage.add("Z");
    }
}
