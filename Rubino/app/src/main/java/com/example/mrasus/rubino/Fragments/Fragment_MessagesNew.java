package com.example.mrasus.rubino.Fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.mrasus.rubino.Adapters.MessagePagerAdapter;
import com.example.mrasus.rubino.Constants.Constants;
import com.example.mrasus.rubino.Constants.InternetChecker;
import com.example.mrasus.rubino.Constants.MessageRubino;
import com.example.mrasus.rubino.Database.MyApp;
import com.example.mrasus.rubino.Fragments.messages.Fragment_ReceiveMessage;
import com.example.mrasus.rubino.Fragments.messages.Fragment_SendMessage;
import com.example.mrasus.rubino.Helper;
import com.example.mrasus.rubino.MyToast.RubinoToast;
import com.example.mrasus.rubino.R;
import com.example.mrasus.rubino.Retrofit.APIService;
import com.example.mrasus.rubino.Retrofit.ApiUtils;
import com.example.mrasus.rubino.Retrofit.GetAllMessage_Request.GetAllMessageRequest;
import com.example.mrasus.rubino.Retrofit.GetAllMessage_Request.Result;
import com.example.mrasus.rubino.Retrofit.Login_Request.LoginRequest;
import com.example.mrasus.rubino.activities.BaseActivity;
import com.gigamole.navigationtabstrip.NavigationTabStrip;

import java.util.ArrayList;
import java.util.List;

import me.zhanghai.android.materialprogressbar.MaterialProgressBar;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment_MessagesNew extends Fragment implements Helper.ReloadList{


    private ViewPager viewPager_message;
    private NavigationTabStrip nts_messages;
    private MessagePagerAdapter adapter;
    private List<List<MessageRubino>> lists;
    private Call<GetAllMessageRequest> callAllMessage;
    private SwipeRefreshLayout refreshLayout;
    private MaterialProgressBar materialProgressBar;
    private List<MessageRubino> messageSendRubinoList;
    private List<MessageRubino> messageReceiveRubinoList;

    public Fragment_MessagesNew() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fragment__messages_new, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        reference(view);
    }

    private void reference(View view) {
        viewPager_message = view.findViewById(R.id.viewPager_message);
        nts(view);
        lists = new ArrayList<>();
        materialProgressBar = getActivity().findViewById(R.id.materialProgressBar);
        messageSendRubinoList = new ArrayList<>();
        messageReceiveRubinoList = new ArrayList<>();

        materialProgressBar = getActivity().findViewById(R.id.materialProgressBar);
        refreshLayout = view.findViewById(R.id.refresh);
        refreshLayout.setColorSchemeResources(R.color.colorPrimary);
        lists.add(messageReceiveRubinoList);
        lists.add(messageSendRubinoList);
        adapter = new MessagePagerAdapter(lists,getActivity());
        adapter.reloadList = this;
        viewPager_message.setAdapter(adapter);
        viewPager_message.setCurrentItem(1);
        internetChecker();
        listener();
    }

    private void nts(final View view) {
        nts_messages = view.findViewById(R.id.nts_messages);
        nts_messages.setTitles(getString(R.string.allMessageReceived),getString(R.string.allMessagePosted));
        nts_messages.setTabIndex(1);
        nts_messages.setTypeface(BaseActivity.appFont);

        nts_messages.setOnTabStripSelectedIndexListener(new NavigationTabStrip.OnTabStripSelectedIndexListener() {
            @Override
            public void onStartTabSelected(String title, int index) {

            }

            @Override
            public void onEndTabSelected(String title, int index) {
                switch (index){
                    case 0:
                        viewPager_message.setCurrentItem(0);
                        break;
                    case 1:
                        viewPager_message.setCurrentItem(1);
                        break;
                }
            }
        });
    }
    private void listener(){
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                internetChecker();
            }
        });
        viewPager_message.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (viewPager_message.getCurrentItem() == 0){
                    nts_messages.setTabIndex(0);
                }else {
                    nts_messages.setTabIndex(1);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }
    //---------retrofit--------
    private void getAllMessage(){
        APIService apiService = ApiUtils.getAPIService();
        callAllMessage = apiService.getAllMessage(MyApp.getMemory().getString(Constants.tokenId, ""));
        callAllMessage.enqueue(new Callback<GetAllMessageRequest>() {
            @Override
            public void onResponse(Call<GetAllMessageRequest> call, Response<GetAllMessageRequest> response) {
                materialProgressBar.setVisibility(View.GONE);
                refreshLayout.setRefreshing(false);
                if (true) {
                    if (response.body() != null) {
                        if (response.body().getStatus().getIsSuccess()) {
                            lists.clear();
                            messageSendRubinoList.clear();
                            messageReceiveRubinoList.clear();
                            adapter.notifyDataSetChanged();
                            messageReceiveRubinoList.clear();
                            messageSendRubinoList.clear();
                            List<Result> result = response.body().getResult();
                            for (int i = 0; i < result.size(); i++) {
                                Result result1 = result.get(i);
                                Integer typeCode = result1.getTypeCode();
                                MessageRubino messageRubino = new MessageRubino();
                                messageRubino.setDate(result1.getSubmitDate());
                                messageRubino.setSubject(result1.getSubject());
                                messageRubino.setTextMessage(result1.getBody());
                                messageRubino.setAttachUrl(result1.getAttachmentUrl());
                                if (typeCode == 1) {
                                    messageSendRubinoList.add(messageRubino);
                                } else {
                                    messageReceiveRubinoList.add(messageRubino);
                                }

                                lists.add(messageReceiveRubinoList);
                                lists.add(messageSendRubinoList);
                                adapter = new MessagePagerAdapter(lists,getActivity());
                                adapter.reloadList = Fragment_MessagesNew.this;
                                viewPager_message.setAdapter(adapter);
                                viewPager_message.setCurrentItem(1);


                            }

                            BaseActivity.unFreeze();
                        } else {
                            RubinoToast.showToast(getActivity(), response.body().getStatus().getMessage(), Toast.LENGTH_SHORT, RubinoToast.warning);
                            BaseActivity.unFreeze();
                        }

                    } else {
                        //RubinoToast.showToast(getActivity(),getString(R.string.error), Toast.LENGTH_SHORT,RubinoToast.warning);
                        login();
                    }
                }

            }

            @Override
            public void onFailure(Call<GetAllMessageRequest> call, Throwable t) {
                if (call.isCanceled()){
                    materialProgressBar.setVisibility(View.GONE);
                }else {
                    materialProgressBar.setVisibility(View.GONE);
                    refreshLayout.setRefreshing(false);
                }

            }
        });
    }
    private void login(){
        materialProgressBar.setVisibility(View.VISIBLE);
        APIService apiService = ApiUtils.getAPIService();
        Call<LoginRequest> login = apiService.login(MyApp.getMemory().getString(Constants.username, ""), MyApp.getMemory().getString(Constants.password, ""));
        login.enqueue(new Callback<LoginRequest>() {
            @Override
            public void onResponse(Call<LoginRequest> call, Response<LoginRequest> response) {
                if (response.body()!=null){
                    if (response.body().getStatus().getIsSuccess()){
                        if (true) {
                            MyApp.getMemory().edit().putString(Constants.tokenId, response.body().getResult().getTokenId()).commit();
                            getAllMessage();
                        }
                    }else{
                        RubinoToast.showToast(getActivity(),response.body().getStatus().getMessage(), Toast.LENGTH_SHORT,RubinoToast.warning);
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginRequest> call, Throwable t) {
                RubinoToast.showToast(getActivity(),getString(R.string.error), Toast.LENGTH_SHORT,RubinoToast.warning);
            }
        });
    }
    //---------retrofit--------
    private void internetChecker() {
        boolean online = InternetChecker.isOnline(getActivity());
        if (online){
            if (!BaseActivity.isFreezed) {
                materialProgressBar.setVisibility(View.VISIBLE);
                BaseActivity.freeze(getActivity());
            }
            getAllMessage();
        }else{
            refreshLayout.setRefreshing(false);
            RubinoToast.showToast(getActivity(),getString(R.string.checkNetwork), Toast.LENGTH_SHORT,RubinoToast.warning);
        }
    }

    @Override
    public void reload() {
        Log.i("kk","reload");
        internetChecker();
    }
}
