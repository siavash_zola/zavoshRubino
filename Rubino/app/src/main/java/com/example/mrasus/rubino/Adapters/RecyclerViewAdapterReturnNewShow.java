package com.example.mrasus.rubino.Adapters;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.example.mrasus.rubino.Constants.Attach;
import com.example.mrasus.rubino.Constants.ReturnProduct;
import com.example.mrasus.rubino.Database.MyApp;
import com.example.mrasus.rubino.R;
import com.example.mrasus.rubino.activities.BaseActivity;

import java.util.List;

/**
 * Created by Mr.Asus on 5/23/2018.
 */

public class RecyclerViewAdapterReturnNewShow extends RecyclerView.Adapter {
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_FOOTER = 1;
    private static final int TYPE_ITEM = 2;
    private static final int TYPE_ATTACH = 3;
    private Activity activity;
    private TextView textView_no_return;
    private List<ReturnProduct> returnProductList;
    private List<Attach> attachList;
    private String shipmentType;
    private String shipmentTitle;
    private String date;

    public RecyclerViewAdapterReturnNewShow(Activity activity, List<ReturnProduct> returnProductList, List<Attach> attachList,String shipmentType,String shipmentTitle,String date) {
        this.activity = activity;
        this.returnProductList = returnProductList;
        this.attachList = attachList;
        textView_no_return = activity.findViewById(R.id.textView_no_return);
        this.shipmentType = shipmentType;
        this.shipmentTitle = shipmentTitle;
        this.date = date;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM){
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.return_item_new_show, parent, false);
            return new ItemViewHolder(itemView);
        }else if (viewType == TYPE_FOOTER){
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.return_footer_new_show, parent, false);
            return new FooterViewHolder(itemView);
        }else if (viewType == TYPE_ATTACH){
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.attach_room, parent, false);
            return new AttachViewHolder(itemView);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final int positionAttach = position-returnProductList.size()-1;
        if (holder instanceof ItemViewHolder){
            ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
            final ReturnProduct returnProduct = returnProductList.get(position);
            itemViewHolder.textView_serial.setText(returnProduct.getSerial());
            itemViewHolder.textView_reasonText.setText(returnProduct.getReason());
        }
        else if (holder instanceof FooterViewHolder){
            FooterViewHolder footerViewHolder = (FooterViewHolder) holder;
            footerViewHolder.radio_selected.setText(shipmentType);
            footerViewHolder.editText_transferSelected.setText(shipmentTitle);
            footerViewHolder.textView_date.setText(date);
            if (attachList.size() == 0){
                //footerViewHolder.textView_isExistAttach.setText(R.string.noAttach);
            }
        } else if (holder instanceof AttachViewHolder){
            AttachViewHolder attachViewHolder = (AttachViewHolder) holder;
            final Attach attach = attachList.get(positionAttach);
            attachViewHolder.textView_attachName.setText(attach.getAttachName());

            attachViewHolder.linearLayout_attach.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String attachUrl = attach.getAttachUrl();
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse(attachUrl));
                    activity.startActivity(intent);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return returnProductList.size()+1+attachList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (position < returnProductList.size()){
            return TYPE_ITEM;
        }
        else if (position == returnProductList.size()){
            return TYPE_FOOTER;
        }else {
            return TYPE_ATTACH;
        }
    }

    private class ItemViewHolder extends RecyclerView.ViewHolder {
        TextView textView_serial_title,textView_reasonTitle,textView_reasonText,textView_serial;

        public ItemViewHolder(View itemView) {
            super(itemView);
            textView_serial_title = itemView.findViewById(R.id.textView_serial_title);
            textView_reasonTitle = itemView.findViewById(R.id.textView_reasonTitle);
            textView_reasonText = itemView.findViewById(R.id.textView_reasonText);
            textView_serial = itemView.findViewById(R.id.textView_serial);

            textView_serial_title.setTypeface(BaseActivity.appFont_Bold);
            textView_reasonTitle.setTypeface(BaseActivity.appFont);
            textView_reasonText.setTypeface(BaseActivity.appFont);
            textView_serial.setTypeface(BaseActivity.appFont_Bold);

        }
    }
    private class FooterViewHolder extends RecyclerView.ViewHolder {
        TextView textView_transport_text,textView_attach_title,textView_isExistAttach,textView_info,textView_date;
        EditText editText_transferSelected;
        RadioButton radio_selected;
        public FooterViewHolder(View itemView) {
            super(itemView);
            textView_transport_text = itemView.findViewById(R.id.textView_transport_text);
            editText_transferSelected = itemView.findViewById(R.id.editText_transferSelected);
            //textView_attach_title = itemView.findViewById(R.id.textView_attach_title);
            //textView_isExistAttach = itemView.findViewById(R.id.textView_isExistAttach);
            textView_info = itemView.findViewById(R.id.textView_info);
            radio_selected = itemView.findViewById(R.id.radio_selected);
            textView_date = itemView.findViewById(R.id.textView_date);

            textView_transport_text.setTypeface(BaseActivity.appFont);
            editText_transferSelected.setTypeface(BaseActivity.appFont);
            //textView_attach_title.setTypeface(BaseActivity.appFont);
            //textView_isExistAttach.setTypeface(BaseActivity.appFont);
            textView_info.setTypeface(BaseActivity.appFont);
            radio_selected.setTypeface(BaseActivity.appFont);
            textView_date.setTypeface(BaseActivity.appFont);
        }
    }
    private class AttachViewHolder extends RecyclerView.ViewHolder {
        TextView textView_attachName;
        LinearLayout linearLayout_attach;
        public AttachViewHolder(View itemView) {
            super(itemView);

            textView_attachName = itemView.findViewById(R.id.textView_attachName);
            linearLayout_attach = itemView.findViewById(R.id.linearLayout_attach);
            textView_attachName.setTypeface(BaseActivity.appFont);

        }
    }
}
