package com.example.mrasus.rubino.Retrofit.ProductList_Request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Mr.Asus on 5/21/2018.
 */

public class Sender {
    @SerializedName("productCategoryId")
    @Expose
    private String productCategoryId;

    public String getProductCategoryId() {
        return productCategoryId;
    }

    public void setProductCategoryId(String productCategoryId) {
        this.productCategoryId = productCategoryId;
    }
}
