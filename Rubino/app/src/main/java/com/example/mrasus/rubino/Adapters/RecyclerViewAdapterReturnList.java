package com.example.mrasus.rubino.Adapters;

import android.app.Activity;
import android.support.annotation.IdRes;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.example.mrasus.rubino.Constants.Product;
import com.example.mrasus.rubino.Database.MyApp;
import com.example.mrasus.rubino.R;
import com.example.mrasus.rubino.activities.BaseActivity;

import java.util.List;

/**
 * Created by Mr.Asus on 5/7/2018.
 */

public class RecyclerViewAdapterReturnList extends RecyclerView.Adapter {
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_FOOTER = 1;
    private static final int TYPE_ITEM = 2;
    private Activity activity;
    private List<Product> productList;

    public RecyclerViewAdapterReturnList(Activity activity, List<Product> productList) {
        this.activity = activity;
        this.productList = productList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == TYPE_ITEM) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.return_item, parent, false);
            return new ItemViewHolder(itemView);
        }else if (viewType == TYPE_FOOTER){
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.return_footer, parent, false);
            return new FooterViewHolder(itemView);
        }else{
            return null;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ItemViewHolder){
            ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
            Product product = productList.get(position);
            itemViewHolder.textView_title_return.setText(product.getTitle());
            itemViewHolder.textView_numberReturn.setText(MyApp.getCenterDAO().getNumberProduct(product)+"");
        }else if (holder instanceof FooterViewHolder){
            final FooterViewHolder footerViewHolder = (FooterViewHolder) holder;
            footerViewHolder.radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {
                    switch (i) {
                        case R.id.radio_Truck:
                            //Toast.makeText(activity,"radio_Truck",Toast.LENGTH_SHORT).show();
                            footerViewHolder.editText_Bearing.setBackgroundColor(activity.getResources().getColor(R.color.white));
                            footerViewHolder.editText_Truck.setBackgroundColor(activity.getResources().getColor(R.color.table_background));
                            footerViewHolder.editText_Truck.setEnabled(true);
                            footerViewHolder.editText_Bearing.setEnabled(false);

                            break;
                        case R.id.radio_Bearing:
                            //Toast.makeText(activity,"radio_Bearing",Toast.LENGTH_SHORT).show();
                            footerViewHolder.editText_Bearing.setBackgroundColor(activity.getResources().getColor(R.color.table_background));
                            footerViewHolder.editText_Truck.setBackgroundColor(activity.getResources().getColor(R.color.white));
                            footerViewHolder.editText_Truck.setEnabled(false);
                            footerViewHolder.editText_Bearing.setEnabled(true);
                            break;
                    }
                }
            });

            footerViewHolder.textViewFinalReturn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return productList.size()+1;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == productList.size()){
            return TYPE_FOOTER;
        }else return TYPE_ITEM;
    }

    private class ItemViewHolder extends RecyclerView.ViewHolder {
        TextView textView_numberReturn;
        TextView textView_title_return;
        TextView textView_number_text;
        EditText editText_reason;
        public ItemViewHolder(View itemView) {
            super(itemView);
            textView_numberReturn = itemView.findViewById(R.id.textView_numberReturn);
            textView_title_return = itemView.findViewById(R.id.textView_title_return);
            textView_number_text = itemView.findViewById(R.id.textView_number_text);
            editText_reason = itemView.findViewById(R.id.editText_reason);

            textView_numberReturn.setTypeface(BaseActivity.appFont);
            textView_title_return.setTypeface(BaseActivity.appFont);
            editText_reason.setTypeface(BaseActivity.appFont);
            textView_number_text.setTypeface(BaseActivity.appFont);
        }
    }

    private class FooterViewHolder extends RecyclerView.ViewHolder {
        TextView textView_transfer_text;
        TextView textViewFinalReturn;
        RadioGroup radioGroup;
        RadioButton radio_Bearing;
        RadioButton radio_Truck;
        EditText editText_Bearing;
        EditText editText_Truck;

        public FooterViewHolder(View itemView) {
            super(itemView);
            textView_transfer_text = itemView.findViewById(R.id.textView_transfer_text);
            textViewFinalReturn = itemView.findViewById(R.id.textViewFinalReturn);
            radioGroup = itemView.findViewById(R.id.radioGroup_transport);
            radio_Truck = itemView.findViewById(R.id.radio_Truck);
            radio_Bearing = itemView.findViewById(R.id.radio_Bearing);
            editText_Bearing = itemView.findViewById(R.id.editText_Bearing);
            editText_Truck = itemView.findViewById(R.id.editText_Truck);

            textView_transfer_text.setTypeface(BaseActivity.appFont);
            textViewFinalReturn.setTypeface(BaseActivity.appFont);
            editText_Bearing.setTypeface(BaseActivity.appFont);
            editText_Truck.setTypeface(BaseActivity.appFont);
            radio_Truck.setTypeface(BaseActivity.appFont);
            radio_Bearing.setTypeface(BaseActivity.appFont);
        }
    }
}
