package com.example.mrasus.rubino.Adapters;

import android.app.Activity;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mrasus.rubino.Constants.Constants;
import com.example.mrasus.rubino.Constants.InternetChecker;
import com.example.mrasus.rubino.Constants.Product;
import com.example.mrasus.rubino.CustomView.MyEditText;
import com.example.mrasus.rubino.Database.MyApp;
import com.example.mrasus.rubino.Fragments.Fragment_Order;
import com.example.mrasus.rubino.MyToast.RubinoToast;
import com.example.mrasus.rubino.R;
import com.example.mrasus.rubino.Retrofit.APIService;
import com.example.mrasus.rubino.Retrofit.ApiUtils;
import com.example.mrasus.rubino.Retrofit.Login_Request.LoginRequest;
import com.example.mrasus.rubino.Retrofit.OrderPost_Request.OrderDetailItem;
import com.example.mrasus.rubino.Retrofit.OrderPost_Request.OrderPostRequest;
import com.example.mrasus.rubino.Retrofit.OrderPost_Request.OrderPostSender;
import com.example.mrasus.rubino.activities.BaseActivity;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import me.zhanghai.android.materialprogressbar.MaterialProgressBar;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Mr.Asus on 5/3/2018.
 */

public class RecyclerViewAdapterRequestTable extends RecyclerView.Adapter {
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_FOOTER = 1;
    private static final int TYPE_ITEM = 2;
    private Activity activity;
    private List<Product> productList;
    private OrderPostSender orderPostSender;
    private MaterialProgressBar materialProgressBar;

    public RecyclerViewAdapterRequestTable(Activity activity , List<Product> productList) {
        this.activity = activity;
        this.productList = productList;
        orderPostSender = new OrderPostSender();
        orderPostSender.setShippmentType("");
        orderPostSender.setPaymentType("");

        materialProgressBar = activity.findViewById(R.id.materialProgressBar);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_HEADER){
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.table_header, parent, false);
            return new HeaderViewHolder(itemView);
        }else if(viewType == TYPE_ITEM) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.table_item, parent, false);
            return new ItemViewHolder(itemView);
        }else if(viewType == TYPE_FOOTER) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.table_footer, parent, false);
            return new FooterViewHolder(itemView);
        }else{
            return null;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final int positionNew = position-1;
        if (holder instanceof ItemViewHolder){
            final ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
            final Product product = productList.get(positionNew);
            String price = product.getPrice();
            final int number = product.getNumber();
            itemViewHolder.textView_title.setText(product.getTitle());
            itemViewHolder.textView_price.setText(convert(price));
            itemViewHolder.textView_num.setText(number+"");
            int sum = number * Integer.parseInt(price);
            itemViewHolder.textView_sum.setText(convert(sum+""));

            itemViewHolder.linearLayout_plus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int number1 = product.getNumber();
                    MyApp.getCenterDAO_Order().addProduct(product);
                    number1++;
                    itemViewHolder.textView_num.setText(MyApp.getCenterDAO_Order().getNumberProduct(product)+"");
                    int numberProduct = MyApp.getCenterDAO_Order().getNumberProduct(product);
                    itemViewHolder.textView_sum.setText(numberProduct*Integer.parseInt(product.getPrice())+"");
                    product.setNumber(number1);
                    notifyDataSetChanged();
                }
            });
            itemViewHolder.linearLayout_minus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int number1 = product.getNumber();
                    MyApp.getCenterDAO_Order().deleteProduct(product);
                    if (number1>0) {
                        number1--;
                    }
                    itemViewHolder.textView_num.setText(MyApp.getCenterDAO_Order().getNumberProduct(product)+"");
                    int numberProduct = MyApp.getCenterDAO_Order().getNumberProduct(product);
                    itemViewHolder.textView_sum.setText(numberProduct*Integer.parseInt(product.getPrice())+"");
                    product.setNumber(number1);
                    notifyDataSetChanged();
                }
            });
        }else if (holder instanceof FooterViewHolder){
            final FooterViewHolder footerViewHolder = (FooterViewHolder) holder;
            int sum = 0;
            for (int i = 0; i < productList.size(); i++) {
                Product product = productList.get(i);
                sum = sum + product.getNumber() * Integer.parseInt(product.getPrice());
            }
            footerViewHolder.textView_price.setText(convert(sum+""));
            orderPostSender.setTotalAmount(sum);
            //---------------------------------------------------------
            footerViewHolder.radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {
                    switch (i) {
                        case R.id.radio_Truck:
                            //Toast.makeText(activity,"radio_Truck",Toast.LENGTH_SHORT).show();
                            footerViewHolder.editText_Bearing.setBackgroundColor(activity.getResources().getColor(R.color.white));
                            footerViewHolder.editText_Bearing.setEnabled(false);
                            orderPostSender.setShippmentType("truck");

                            break;
                        case R.id.radio_Bearing:
                            //Toast.makeText(activity,"radio_Bearing",Toast.LENGTH_SHORT).show();
                            footerViewHolder.editText_Bearing.setBackgroundColor(activity.getResources().getColor(R.color.table_background));
                            footerViewHolder.editText_Bearing.setEnabled(true);
                            orderPostSender.setShippmentType("barbari");
                            break;
                    }
                }
            });

            footerViewHolder.radioGroup_pay.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {
                    switch (i) {
                        case R.id.radio_card:
                            orderPostSender.setPaymentType("credit");

                            break;
                        case R.id.radio_cash:
                            orderPostSender.setPaymentType("cash");
                            break;
                    }
                }
            });
            //----------------------------------------------------------

            //----------------------------------------------------------
            footerViewHolder.textViewFinal.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    internetChecker(footerViewHolder);
                }
            });

            footerListener(footerViewHolder);
        }
    }

    //---------retrofit--------------
    private void doSendRequest(final FooterViewHolder footerViewHolder) {
        List<OrderDetailItem> ordersList = MyApp.getCenterDAO_Order().getOrdersFromDataBase();
        orderPostSender.setOrderDetailItems(ordersList);
        orderPostSender.setAddress(footerViewHolder.editText_address.text());
        orderPostSender.setDesc(footerViewHolder.editText_Description.text());
        orderPostSender.setPhone(footerViewHolder.editText_receiverPhone.text());
        if (orderPostSender.getShippmentType().equals("barbari")) {
            orderPostSender.setShippmentTitle(footerViewHolder.editText_Bearing.text());
        }else{
            orderPostSender.setShippmentTitle("");
        }

        if (MyApp.getCenterDAO_Order().getSumNumber() != 0) {
            if (checkingInputOrder()){
                materialProgressBar.setVisibility(View.VISIBLE);
                APIService apiService = ApiUtils.getAPIService();
                Call<OrderPostRequest> orderPostRequestCall = apiService.orderPost(MyApp.getMemory().getString(Constants.tokenId, ""), orderPostSender);
                orderPostRequestCall.enqueue(new Callback<OrderPostRequest>() {
                    @Override
                    public void onResponse(Call<OrderPostRequest> call, Response<OrderPostRequest> response) {
                        if (response.body() !=null) {
                            if (response.body().getStatus() != null) {
                                if (response.body().getStatus().getIsSuccess()) {
                                    OrderPostRequest body = response.body();
                                    if (body.getResult().equals("ok")) {
                                        materialProgressBar.setVisibility(View.INVISIBLE);
                                        RubinoToast.showToast(activity, activity.getString(R.string.order_added), Toast.LENGTH_SHORT, RubinoToast.success);
                                        MyApp.getCenterDAO_Order().clearTable();
                                        for (int i = BaseActivity.fragmentManager.getBackStackEntryCount(); i > 0; i--) {
                                            BaseActivity.fragmentManager.popBackStack();
                                        }
                                        Fragment_Order fragment_order = new Fragment_Order();
                                        FragmentTransaction fragmentTransaction = BaseActivity.fragmentManager.beginTransaction();
                                        fragmentTransaction.replace(R.id.frame, fragment_order).commit();
                                    }
                                }else{
                                    RubinoToast.showToast(activity,response.body().getStatus().getMessage(), Toast.LENGTH_SHORT,RubinoToast.warning);
                                }
                            }else{
                                RubinoToast.showToast(activity,activity.getString(R.string.error), Toast.LENGTH_SHORT,RubinoToast.warning);
                            }
                        }else{
                            //RubinoToast.showToast(activity,activity.getString(R.string.error), Toast.LENGTH_SHORT,RubinoToast.warning);
                            login(footerViewHolder);
                        }
                    }

                    @Override
                    public void onFailure(Call<OrderPostRequest> call, Throwable t) {
                        materialProgressBar.setVisibility(View.INVISIBLE);
                        RubinoToast.showToast(activity,activity.getString(R.string.error),Toast.LENGTH_SHORT,RubinoToast.success);
                    }
                });
            }else {
                RubinoToast.showToast(activity,activity.getString(R.string.completeForm),Toast.LENGTH_SHORT,RubinoToast.warning);
            }
        }else{
            RubinoToast.showToast(activity,activity.getString(R.string.noProductSelected),Toast.LENGTH_SHORT,RubinoToast.warning);
        }
    }
    private void login(final FooterViewHolder footerViewHolder){
        APIService apiService = ApiUtils.getAPIService();
        Call<LoginRequest> login = apiService.login(MyApp.getMemory().getString(Constants.username, ""), MyApp.getMemory().getString(Constants.password, ""));
        login.enqueue(new Callback<LoginRequest>() {
            @Override
            public void onResponse(Call<LoginRequest> call, Response<LoginRequest> response) {
                if (response.body()!=null){
                    if (response.body().getStatus().getIsSuccess()){
                        MyApp.getMemory().edit().putString(Constants.tokenId,response.body().getResult().getTokenId()).commit();
                        doSendRequest(footerViewHolder);
                    }else{
                        RubinoToast.showToast(activity,response.body().getStatus().getMessage(), Toast.LENGTH_SHORT,RubinoToast.warning);
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginRequest> call, Throwable t) {
                RubinoToast.showToast(activity,activity.getString(R.string.error), Toast.LENGTH_SHORT,RubinoToast.warning);
            }
        });
    }
    //---------retrofit--------------
    private boolean checkingInputOrder() {
        boolean b = false;
        String shippmentType = orderPostSender.getShippmentType();
        String address = orderPostSender.getAddress();
        String shippmentTitle = orderPostSender.getShippmentTitle();
        String paymentType = orderPostSender.getPaymentType();
        String phone = orderPostSender.getPhone();
        if (address.length()>0 && shippmentType.length()>0 && paymentType.length()>0 && phone.length()>0){
            if (phone.matches("(\\+989|09)(12|19|35|36|37|38|39|32)\\d{7}")) {
                if (shippmentType.equals("barbari")) {
                    if (shippmentTitle.length() > 0) {
                        b = true;
                    }
                } else {
                    b = true;
                }
            }else{
                RubinoToast.showToast(activity,activity.getString(R.string.notValidPhone), Toast.LENGTH_SHORT,RubinoToast.warning);
            }
        }
        return b;
    }

    public void footerListener (FooterViewHolder footerViewHolder){
        footerViewHolder.editText_receiverPhone.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    Constants.hideKeyboard(activity,v);
                }
            }
        });
        footerViewHolder.editText_Bearing.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    Constants.hideKeyboard(activity,v);
                }
            }
        });
        footerViewHolder.editText_address.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    Constants.hideKeyboard(activity,v);
                }
            }
        });
        footerViewHolder.editText_Description.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    Constants.hideKeyboard(activity,v);
                }
            }
        });
    }
    @Override
    public int getItemCount() {
        return productList.size()+2;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0){
            return TYPE_HEADER;
        }else if (position == productList.size()+1) {
            return TYPE_FOOTER;
        }else {
            return TYPE_ITEM;
        }
    }

    private class HeaderViewHolder extends RecyclerView.ViewHolder{
        TextView textView_sum;
        TextView textView_num;
        TextView textView_price;
        TextView textView_title;
        public HeaderViewHolder(View itemView) {
            super(itemView);
            textView_sum = itemView.findViewById(R.id.textView_sum);
            textView_num = itemView.findViewById(R.id.textView_num);
            textView_price = itemView.findViewById(R.id.textView_price);
            textView_title = itemView.findViewById(R.id.textView_title);

            textView_sum.setTypeface(BaseActivity.appFont);
            textView_num.setTypeface(BaseActivity.appFont);
            textView_price.setTypeface(BaseActivity.appFont);
            textView_title.setTypeface(BaseActivity.appFont);
        }
    }

    private class ItemViewHolder extends RecyclerView.ViewHolder{
        TextView textView_sum;
        TextView textView_num;
        TextView textView_price;
        TextView textView_title;
        LinearLayout linearLayout_plus;
        LinearLayout linearLayout_minus;
        public ItemViewHolder(View itemView) {
            super(itemView);
            textView_sum = itemView.findViewById(R.id.textView_sum);
            textView_num = itemView.findViewById(R.id.textView_num);
            textView_price = itemView.findViewById(R.id.textView_price);
            textView_title = itemView.findViewById(R.id.textView_title);

            linearLayout_minus = itemView.findViewById(R.id.linearLayout_minus);
            linearLayout_plus = itemView.findViewById(R.id.linearLayout_plus);

            textView_sum.setTypeface(BaseActivity.appFont);
            textView_num.setTypeface(BaseActivity.appFont);
            textView_price.setTypeface(BaseActivity.appFont);
            textView_title.setTypeface(BaseActivity.appFont);
        }
    }

    private class FooterViewHolder extends RecyclerView.ViewHolder{
        TextView textView_price;
        TextView textView_phone_title;
        TextView textView_priceText;
        TextView textView_transport_text;
        TextView textView_address_text;
        TextView textView_pay;
        TextView textViewFinal;
        TextView textView_Description;
        RadioButton radio_Bearing;
        RadioButton radio_Truck;
        RadioButton radio_card;
        RadioButton radio_cash;
        MyEditText editText_Bearing;
        MyEditText editText_Description;
        MyEditText editText_address;
        MyEditText editText_receiverPhone;
        RadioGroup radioGroup;
        RadioGroup radioGroup_pay;
        public FooterViewHolder(View itemView) {
            super(itemView);
            textView_price = itemView.findViewById(R.id.textView_price);
            textView_priceText = itemView.findViewById(R.id.textView_priceText);
            textView_transport_text = itemView.findViewById(R.id.textView_transport_text);
            textView_pay = itemView.findViewById(R.id.textView_pay);
            textViewFinal = itemView.findViewById(R.id.textViewFinal);
            textView_Description = itemView.findViewById(R.id.textView_Description);
            textView_phone_title = itemView.findViewById(R.id.textView_phone_title);
            radio_Truck = itemView.findViewById(R.id.radio_Truck);
            radio_Bearing = itemView.findViewById(R.id.radio_Bearing);
            radio_card = itemView.findViewById(R.id.radio_card);
            radio_cash = itemView.findViewById(R.id.radio_cash);
            editText_Bearing = itemView.findViewById(R.id.editText_Bearing);
            editText_address = itemView.findViewById(R.id.editText_address);
            editText_receiverPhone = itemView.findViewById(R.id.editText_receiverPhone);
            editText_Description = itemView.findViewById(R.id.editText_Description);
            textView_address_text = itemView.findViewById(R.id.textView_address_text);
            radioGroup = itemView.findViewById(R.id.radioGroup_transport);
            radioGroup_pay = itemView.findViewById(R.id.radioGroup_pay);

            textView_priceText.setTypeface(BaseActivity.appFont);
            textView_price.setTypeface(BaseActivity.appFont);
            textView_transport_text.setTypeface(BaseActivity.appFont);
            radio_Bearing.setTypeface(BaseActivity.appFont);
            radio_Truck.setTypeface(BaseActivity.appFont);
            textView_address_text.setTypeface(BaseActivity.appFont);
            textView_pay.setTypeface(BaseActivity.appFont);
            radio_card.setTypeface(BaseActivity.appFont);
            radio_cash.setTypeface(BaseActivity.appFont);
            textViewFinal.setTypeface(BaseActivity.appFont);
            textView_Description.setTypeface(BaseActivity.appFont);
            textView_phone_title.setTypeface(BaseActivity.appFont);

            editText_address.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    //Log.i("kk",editText_address.text());
                    String text = editText_address.text();
                    if (Constants.checkLanguage(text)){
                        Log.i("kk","far");
                        editText_address.setError(null);

                    }else {
                        Log.i("kk","eng");
                        //editText_address.setText("");
                        StringBuilder sb = new StringBuilder(text);
                        String s = sb.deleteCharAt(text.length() - 1).toString();
                        editText_address.setText(s);
                        editText_address.setSelection(editText_address.text().length());
                        editText_address.setError(activity.getString(R.string.justPersian));
                    }
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });
            editText_Description.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    //Log.i("kk",editText_address.text());
                    String text = editText_Description.text();
                    if (Constants.checkLanguage(text)){
                        Log.i("kk","far");
                        editText_Description.setError(null);

                    }else {
                        Log.i("kk","eng");
                        //editText_address.setText("");
                        StringBuilder sb = new StringBuilder(text);
                        String s = sb.deleteCharAt(text.length() - 1).toString();
                        editText_Description.setText(s);
                        editText_Description.setSelection(editText_Description.text().length());
                        editText_Description.setError(activity.getString(R.string.justPersian));
                    }
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });
        }
    }
    public String convert(String price){
        NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
        String numberAsString = numberFormat.format(Integer.parseInt(price));
        return numberAsString;
    }
    private void internetChecker(FooterViewHolder footerViewHolder) {
        boolean online = InternetChecker.isOnline(activity);
        if (online){
            doSendRequest(footerViewHolder);
        }else{
            RubinoToast.showToast(activity,activity.getString(R.string.checkNetwork), Toast.LENGTH_SHORT,RubinoToast.warning);
        }
    }
}
