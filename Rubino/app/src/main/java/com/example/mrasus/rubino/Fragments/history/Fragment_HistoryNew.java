package com.example.mrasus.rubino.Fragments.history;


import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mrasus.rubino.Adapters.RecyclerViewAdapterHistory;
import com.example.mrasus.rubino.Adapters.RecyclerViewAdapterHistoryReturn;
import com.example.mrasus.rubino.Constants.Constants;
import com.example.mrasus.rubino.Constants.InternetChecker;
import com.example.mrasus.rubino.Constants.RegisteredOrders;
import com.example.mrasus.rubino.Database.MyApp;
import com.example.mrasus.rubino.MyToast.RubinoToast;
import com.example.mrasus.rubino.R;
import com.example.mrasus.rubino.Retrofit.APIService;
import com.example.mrasus.rubino.Retrofit.ApiUtils;
import com.example.mrasus.rubino.Retrofit.History_Request.HistoryRequest;
import com.example.mrasus.rubino.Retrofit.History_Request.OrderHistory;
import com.example.mrasus.rubino.Retrofit.History_Request.ReturnOrderHistory;
import com.example.mrasus.rubino.Retrofit.Login_Request.LoginRequest;
import com.example.mrasus.rubino.activities.BaseActivity;
import com.gigamole.navigationtabstrip.NavigationTabStrip;

import java.util.ArrayList;
import java.util.List;

import me.zhanghai.android.materialprogressbar.MaterialProgressBar;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment_HistoryNew extends Fragment {
    private NavigationTabStrip mBottomNavigationTabStrip;
    private List<RegisteredOrders> list_Order;
    private List<RegisteredOrders> list_Return;
    private FrameLayout frameLayout;
    private MaterialProgressBar materialProgressBar;

    private RecyclerView recyclerView_History;
    private LinearLayoutManager layoutManager;
    private RecyclerViewAdapterHistoryReturn recyclerViewAdapterHistoryReturn;
    private RecyclerViewAdapterHistory recyclerViewAdapterHistory;
    private SwipeRefreshLayout refreshLayout;
    private TextView textView_monitor;
    Call<HistoryRequest> callHistory;
    private boolean isLife = true;
    public Fragment_HistoryNew() {
        // Required empty public constructor
    }

    //public Fragment_HistoryNew(Activity activity) {
    //    this.activity = activity;
    //}


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fragment__history_new, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        toolbar();
        reference(view);
        nts(view);
        listener();
    }



    @Override
    public void onPause() {
        super.onPause();
        if (callHistory != null) {
            callHistory.cancel();
        }
        isLife = false;
        materialProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void onResume() {
        super.onResume();
        isLife = true;
    }

    private void listener(){
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //requestToServerFor2List();
                internetChecker();
            }
        });
    }
    private void toolbar() {
        TextView textView = getActivity().findViewById(R.id.textView_toolbar);
        textView.setText(getString(R.string.history));
    }
    private void reference(View view) {
        textView_monitor = view.findViewById(R.id.textView_no_history);
        list_Order = new ArrayList<>();
        list_Return = new ArrayList<>();
        materialProgressBar = getActivity().findViewById(R.id.materialProgressBar);
        recyclerView_History = view.findViewById(R.id.recyclerView_History);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView_History.setLayoutManager(layoutManager);
        refreshLayout = view.findViewById(R.id.refresh);
        refreshLayout.setColorSchemeResources(R.color.colorPrimary);
    }
    private void nts(View view) {
        mBottomNavigationTabStrip = (NavigationTabStrip) view.findViewById(R.id.nts_history);
        mBottomNavigationTabStrip.setTitles( getString(R.string.returnRegistered),getString(R.string.registeredOrder));
        if (BaseActivity.isFormReturn){
            Log.i("==","0 shod");
            mBottomNavigationTabStrip.setTabIndex(0);
            recyclerViewAdapterHistoryReturn = new RecyclerViewAdapterHistoryReturn(getActivity(),list_Return);
            recyclerView_History.setAdapter(recyclerViewAdapterHistoryReturn);
        }else{
            Log.i("==","1 shod");
            mBottomNavigationTabStrip.setTabIndex(1);
            recyclerViewAdapterHistory = new RecyclerViewAdapterHistory(getActivity(),list_Order);
            recyclerView_History.setAdapter(recyclerViewAdapterHistory);
        }
        internetChecker();
        mBottomNavigationTabStrip.setTypeface(BaseActivity.appFont);
        mBottomNavigationTabStrip.setOnTabStripSelectedIndexListener(new NavigationTabStrip.OnTabStripSelectedIndexListener() {
            @Override
            public void onStartTabSelected(String title, int index) {
            }

            @Override
            public void onEndTabSelected(String title, int index) {
                switch (index) {
                    case 0:
                        if (list_Return.size() == 0){
                            textView_monitor.setText(R.string.noHistory);
                        }else{
                            textView_monitor.setText("");
                        }
                        recyclerViewAdapterHistoryReturn = new RecyclerViewAdapterHistoryReturn(getActivity(),list_Return);
                        recyclerView_History.setAdapter(recyclerViewAdapterHistoryReturn);
                        BaseActivity.isFormReturn = true;
                        break;
                    case 1:
                        if (list_Order.size() == 0){
                            textView_monitor.setText(R.string.noHistory);
                        }else{
                            textView_monitor.setText("");
                        }
                        recyclerViewAdapterHistory = new RecyclerViewAdapterHistory(getActivity(),list_Order);
                        recyclerView_History.setAdapter(recyclerViewAdapterHistory);
                        BaseActivity.isFormReturn = false;
                        break;
                }
            }
        });

    }

    //------------retro-----
    private void requestToServerFor2List(){
        materialProgressBar.setVisibility(View.VISIBLE);
        APIService apiService = ApiUtils.getAPIService();
        callHistory = apiService.getHistory(MyApp.getMemory().getString(Constants.tokenId, ""));
        callHistory.enqueue(new Callback<HistoryRequest>() {
            @Override
            public void onResponse(Call<HistoryRequest> call, Response<HistoryRequest> response) {
                materialProgressBar.setVisibility(View.GONE);
                refreshLayout.setRefreshing(false);
                if (isLife) {
                    if (response.body() != null) {
                        if (response.body().getStatus().getIsSuccess()) {
                            BaseActivity.unFreeze();
                            HistoryRequest body = response.body();
                            List<OrderHistory> orderHistories = body.getResult().getOrderHistories();
                            List<ReturnOrderHistory> returnOrderHistories = body.getResult().getReturnOrderHistories();
                            list_Order = convertOrderList(orderHistories);
                            list_Return = convertReturnList(returnOrderHistories);


                            if (BaseActivity.isFormReturn) {
                                if (list_Return.size() == 0) {
                                    textView_monitor.setText(R.string.noHistory);
                                } else {
                                    textView_monitor.setText("");
                                }
                                recyclerViewAdapterHistoryReturn = new RecyclerViewAdapterHistoryReturn(getActivity(), list_Return);
                                recyclerView_History.setAdapter(recyclerViewAdapterHistoryReturn);
                            } else {
                                if (list_Order.size() == 0) {
                                    textView_monitor.setText(R.string.noHistory);
                                } else {
                                    textView_monitor.setText("");
                                }
                                recyclerViewAdapterHistory = new RecyclerViewAdapterHistory(getActivity(), list_Order);
                                recyclerView_History.setAdapter(recyclerViewAdapterHistory);
                            }
                        } else {
                            RubinoToast.showToast(getActivity(), response.body().getStatus().getMessage(), Toast.LENGTH_SHORT, RubinoToast.warning);
                            BaseActivity.unFreeze();
                        }
                    } else {
                        materialProgressBar.setVisibility(View.GONE);
                        //RubinoToast.showToast(getActivity(),getString(R.string.error), Toast.LENGTH_SHORT,RubinoToast.warning);
                        login();
                    }
                }
            }

            @Override
            public void onFailure(Call<HistoryRequest> call, Throwable t) {
                if (call.isCanceled()){
                    Log.i("kk","cancel");
                }else {
                    refreshLayout.setRefreshing(false);
                    RubinoToast.showToast(getActivity(), getString(R.string.error), Toast.LENGTH_SHORT, RubinoToast.warning);
                }
            }
        });

    }
    private void login(){
        materialProgressBar.setVisibility(View.VISIBLE);
        APIService apiService = ApiUtils.getAPIService();
        Call<LoginRequest> login = apiService.login(MyApp.getMemory().getString(Constants.username, ""), MyApp.getMemory().getString(Constants.password, ""));
        login.enqueue(new Callback<LoginRequest>() {
            @Override
            public void onResponse(Call<LoginRequest> call, Response<LoginRequest> response) {
                if (response.body()!=null){
                    if (response.body().getStatus().getIsSuccess()){
                        if (isLife){
                            MyApp.getMemory().edit().putString(Constants.tokenId, response.body().getResult().getTokenId()).commit();
                            requestToServerFor2List();
                        }
                    }else{
                        RubinoToast.showToast(getActivity(),response.body().getStatus().getMessage(), Toast.LENGTH_SHORT,RubinoToast.warning);
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginRequest> call, Throwable t) {
                RubinoToast.showToast(getActivity(),getString(R.string.error), Toast.LENGTH_SHORT,RubinoToast.warning);
            }
        });
    }
    //------------retro-----
    private List<RegisteredOrders> convertOrderList(List<OrderHistory> orderHistories) {
        List<RegisteredOrders> list = new ArrayList<>();
        for (int i = 0; i < orderHistories.size(); i++) {
            OrderHistory orderHistory = orderHistories.get(i);
            RegisteredOrders order = new RegisteredOrders();
            order.setNumber(orderHistory.getTotalQuantity());
            order.setTotal(orderHistory.getTotalAmount()+"");
            order.setCondition(orderHistory.getStatus());
            order.setDate(orderHistory.getSubmitDate());
            order.setOrderCode(orderHistory.getOrderCode());
            order.setTransferType(orderHistory.getShippmentType());
            order.setPaymentType(orderHistory.getPaymentType());
            //-----------

            order.setPriceORCargoCode("");
            //-----------
            list.add(order);
        }

        return list;
    }

    private List<RegisteredOrders> convertReturnList(List<ReturnOrderHistory> returnHistories) {
        List<RegisteredOrders> list = new ArrayList<>();
        for (int i = 0; i < returnHistories.size(); i++) {
            ReturnOrderHistory orderHistory = returnHistories.get(i);
            RegisteredOrders order = new RegisteredOrders();

            order.setNumber(orderHistory.getTotalQuantity());
            order.setCondition(orderHistory.getStatus());
            order.setDate(orderHistory.getSubmitDate());
            order.setOrderCode(orderHistory.getRetunOrderCode());
            order.setTransferType(orderHistory.getShippmentType());
            //-----------
            order.setPriceORCargoCode("");
            //-----------
            list.add(order);
        }
        return list;
    }
    private void internetChecker() {
        boolean online = InternetChecker.isOnline(getActivity());
        if (online){
            if (!BaseActivity.isFreezed) {
                BaseActivity.freeze(getActivity());
            }
            requestToServerFor2List();
        }else{
            RubinoToast.showToast(getActivity(),getString(R.string.checkNetwork), Toast.LENGTH_SHORT,RubinoToast.warning);
        }
    }

}
