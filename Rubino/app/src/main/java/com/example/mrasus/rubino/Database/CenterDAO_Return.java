package com.example.mrasus.rubino.Database;

import com.example.mrasus.rubino.Constants.ReturnProduct;
import com.example.mrasus.rubino.Retrofit.ReturnOrder_Reqestu.ReturnOrderDetailItem;

import java.util.List;

/**
 * Created by Mr.Asus on 5/14/2018.
 */

public interface CenterDAO_Return {
    void addProduct(ReturnProduct returnProduct);
    void deleteProduct (ReturnProduct returnProduct);
    List<ReturnProduct> getAllProductSelectedForReturn ();
    int getNumber();
    void clearTable();
    List<ReturnOrderDetailItem> getOrdersReturnFromDataBase();
}
