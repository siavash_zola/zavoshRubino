package com.example.mrasus.rubino.Retrofit.History_Request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Mr.Asus on 5/23/2018.
 */

public class ReturnOrderHistory {
    @SerializedName("submitDate")
    @Expose
    private String submitDate;
    @SerializedName("retunOrderCode")
    @Expose
    private String retunOrderCode;
    @SerializedName("totalQuantity")
    @Expose
    private Integer totalQuantity;
    @SerializedName("shippmentType")
    @Expose
    private String shippmentType;
    @SerializedName("status")
    @Expose
    private String status;

    public String getSubmitDate() {
        return submitDate;
    }

    public void setSubmitDate(String submitDate) {
        this.submitDate = submitDate;
    }

    public String getRetunOrderCode() {
        return retunOrderCode;
    }

    public void setRetunOrderCode(String retunOrderCode) {
        this.retunOrderCode = retunOrderCode;
    }

    public Integer getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(Integer totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public String getShippmentType() {
        return shippmentType;
    }

    public void setShippmentType(String shippmentType) {
        this.shippmentType = shippmentType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
