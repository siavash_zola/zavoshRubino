package com.example.mrasus.rubino.Fragments;


import android.Manifest;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mrasus.rubino.Adapters.RecyclerViewAdapterReturnNew;
import com.example.mrasus.rubino.Constants.Constants;
import com.example.mrasus.rubino.Constants.InternetChecker;
import com.example.mrasus.rubino.Constants.ReturnProduct;
import com.example.mrasus.rubino.CustomView.MyEditText;
import com.example.mrasus.rubino.Database.MyApp;
import com.example.mrasus.rubino.MyToast.RubinoToast;
import com.example.mrasus.rubino.R;
import com.example.mrasus.rubino.Retrofit.APIService;
import com.example.mrasus.rubino.Retrofit.ApiUtils;
import com.example.mrasus.rubino.Retrofit.Login_Request.LoginRequest;
import com.example.mrasus.rubino.Retrofit.OrderPost_Request.OrderPostRequest;
import com.example.mrasus.rubino.Retrofit.ReturnOrder_Reqestu.ReturnOrderSender;
import com.example.mrasus.rubino.activities.BaseActivity;
import com.example.mrasus.rubino.activities.LoginActivity;
import com.example.mrasus.rubino.fileManagerHelper.afilechooser.utils.FileUtils;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import me.zhanghai.android.materialprogressbar.MaterialProgressBar;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment_ReturnNew extends Fragment {

    private FloatingActionButton floatingActionButton;
    private FloatingActionButton floatingActionButtonSend;
    private AlertDialog showAddReturn;
    private RecyclerView recyclerView_return;
    private RecyclerViewAdapterReturnNew recyclerViewAdapterReturnNew;
    private LinearLayoutManager layoutManager;
    private List<ReturnProduct> returnProductList;
    private AlertDialog showSendReturn;
    private TextView textView_no_return;
    private BottomNavigationView bottomNavigationView;
    private ReturnOrderSender returnOrderSender;
    private MaterialProgressBar materialProgressBar;
    private boolean isLife = true;
    private int ATTACH_FILE_REQUEST =10;
    private TextView textView_filePath;

    public Fragment_ReturnNew() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_fragment__return_new, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        internetChecker();
        toolbar();
        reference(view);
        listener();
        if (BaseActivity.isFreezed){
            BaseActivity.unFreeze();
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        isLife = false;
        materialProgressBar.setVisibility(View.GONE);

    }

    @Override
    public void onResume() {
        super.onResume();
        isLife = true;
    }

    private void reference(View view) {
        materialProgressBar = getActivity().findViewById(R.id.materialProgressBar);
        floatingActionButton = view.findViewById(R.id.fab);
        floatingActionButtonSend = view.findViewById(R.id.fab_send);
        layoutManager = new LinearLayoutManager(getActivity());
        returnProductList = new ArrayList<>();
        inputList(MyApp.getCenterDAO_Return().getAllProductSelectedForReturn());
        recyclerView_return = view.findViewById(R.id.recyclerView_return);
        recyclerViewAdapterReturnNew = new RecyclerViewAdapterReturnNew(getActivity(),returnProductList);
        recyclerView_return.setLayoutManager(layoutManager);
        recyclerView_return.setAdapter(recyclerViewAdapterReturnNew);
        bottomNavigationView = getActivity().findViewById(R.id.bottomNavigationView);
        returnOrderSender = new ReturnOrderSender();
        returnOrderSender.setShippmentTitle("");
        returnOrderSender.setShippmentType("");



        textView_no_return = view.findViewById(R.id.textView_no_return);
        textView_no_return.setTypeface(BaseActivity.appFont);
        if (returnProductList.size()!=0){
            textView_no_return.setText("");
        }else{
            textView_no_return.setText(getString(R.string.noProductForReturn));
        }
    }

    private void listener() {
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createDialogAddReturn();
            }
        });
        floatingActionButtonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (returnProductList.size() != 0){
                    createDialogSend();
                }else{
                    //Toast.makeText(activity,R.string.noProductForReturn,Toast.LENGTH_SHORT).show();
                    RubinoToast.showToast(getActivity(),getActivity().getString(R.string.noProductForReturn),Toast.LENGTH_SHORT,RubinoToast.warning);
                }
            }
        });
    }
    private void toolbar() {
        TextView textView = getActivity().findViewById(R.id.textView_toolbar);
        textView.setText(getString(R.string.return_order));
    }

    private void createDialogAddReturn() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rootView = inflater.inflate(R.layout.dialog_add_return, null);
        builder.setView(rootView);
        TextView textView_header = rootView.findViewById(R.id.textView_header);
        TextView textView_addReturn = rootView.findViewById(R.id.textView_addReturn);
        TextView textView_message = rootView.findViewById(R.id.textView_message);
        TextView textView_cancel = rootView.findViewById(R.id.textView_cancel);
        final MyEditText editText_serial = rootView.findViewById(R.id.editText_serial);
        final MyEditText editText_Cause = rootView.findViewById(R.id.editText_Cause);


        textView_header.setTypeface(BaseActivity.appFont_Bold);
        textView_addReturn.setTypeface(BaseActivity.appFont);
        textView_message.setTypeface(BaseActivity.appFont);
        textView_cancel.setTypeface(BaseActivity.appFont);
        builder.create();
        showAddReturn = builder.show();
        editText_serial.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    Constants.hideKeyboard(getActivity(),v);
                }
            }
        });
        editText_Cause.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    Constants.hideKeyboard(getActivity(),v);
                }
            }
        });



        textView_addReturn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String serial = editText_serial.text();
                String reason = editText_Cause.text();

                if (serial.length() != 0 && reason.length() != 0){
                    if (serial.length()<12){
                        //Toast.makeText(activity,R.string.invalidSerial,Toast.LENGTH_SHORT).show();
                        RubinoToast.showToast(getActivity(),getActivity().getString(R.string.invalidSerial),Toast.LENGTH_SHORT,RubinoToast.warning);
                    }else{
                        ReturnProduct returnProduct = new ReturnProduct();
                        returnProduct.setSerial(serial);
                        returnProduct.setReason(reason);
                        //returnProductList.add(returnProduct);
                        MyApp.getCenterDAO_Return().addProduct(returnProduct);
                        List<ReturnProduct> dbList = MyApp.getCenterDAO_Return().getAllProductSelectedForReturn();
                        inputList(dbList);
                        if (returnProductList.size()!=0){
                            textView_no_return.setText("");
                        }else{
                            textView_no_return.setText(getString(R.string.noProductForReturn));
                        }
                        showAddReturn.dismiss();
                        recyclerViewAdapterReturnNew.notifyDataSetChanged();
                        scrollToBottom();
                    }
                }else{
                    //Toast.makeText(activity,R.string.completeForm,Toast.LENGTH_SHORT).show();
                    RubinoToast.showToast(getActivity(),getActivity().getString(R.string.completeForm),Toast.LENGTH_SHORT,RubinoToast.warning);
                }
            }
        });
        textView_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAddReturn.dismiss();
            }
        });


    }

    public void inputList(List<ReturnProduct> dbList) {
        returnProductList.clear();
        for (int i = 0; i < dbList.size(); i++) {
            returnProductList.add(dbList.get(i));
        }
    }

    private void createDialogSend() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rootView = inflater.inflate(R.layout.dialog_send_return, null);
        builder.setView(rootView);
        TextView textView_header = rootView.findViewById(R.id.textView_header);
        TextView textView_sendReturn = rootView.findViewById(R.id.textView_sendReturn);
        TextView textView_cancel = rootView.findViewById(R.id.textView_cancel);
        TextView textView_message = rootView.findViewById(R.id.textView_message);
        final MyEditText editText_Bearing = rootView.findViewById(R.id.editText_Bearing);
        RadioButton radio_Bearing = rootView.findViewById(R.id.radio_Bearing);
        RadioButton radio_Truck = rootView.findViewById(R.id.radio_Truck);
        RadioGroup radioGroup_transport = rootView.findViewById(R.id.radioGroup_transport);
        textView_header.setTypeface(BaseActivity.appFont_Bold);
        textView_sendReturn.setTypeface(BaseActivity.appFont);
        textView_cancel.setTypeface(BaseActivity.appFont);
        textView_message.setTypeface(BaseActivity.appFont);
        radio_Bearing.setTypeface(BaseActivity.appFont);
        radio_Truck.setTypeface(BaseActivity.appFont);

        radioGroup_transport.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {
                switch (i) {
                    case R.id.radio_Truck:
                        editText_Bearing.setBackgroundColor(getActivity().getResources().getColor(R.color.white));
                        editText_Bearing.setEnabled(false);
                        returnOrderSender.setShippmentType("Truck");

                        break;
                    case R.id.radio_Bearing:
                        editText_Bearing.setBackgroundColor(getActivity().getResources().getColor(R.color.table_background));
                        editText_Bearing.setEnabled(true);
                        returnOrderSender.setShippmentType("barbari");
                        break;
                }
            }
        });


        textView_sendReturn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (InternetChecker.isOnline(getActivity())){
                    sendReturnRequest(editText_Bearing.text());
                }else{
                    RubinoToast.showToast(getActivity(),getString(R.string.checkNetwork), Toast.LENGTH_SHORT,RubinoToast.warning);
                }
            }
        });

        textView_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showSendReturn.dismiss();
            }
        });


        editText_Bearing.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    Constants.hideKeyboard(getActivity(),v);
                }
            }
        });

        builder.create();
        showSendReturn = builder.show();
    }

    //------retrofit-------
    private void sendReturnRequest(final String title) {
        returnOrderSender.setReturnOrderDetailItems(MyApp.getCenterDAO_Return().getOrdersReturnFromDataBase());
        returnOrderSender.setShippmentTitle(title);

        if (checkRadio()){
            materialProgressBar.setVisibility(View.VISIBLE);
            APIService apiService = ApiUtils.getAPIService();
            Call<OrderPostRequest> orderPostRequestCall = apiService.returnOrder(MyApp.getMemory().getString(Constants.tokenId, ""), returnOrderSender);
            orderPostRequestCall.enqueue(new Callback<OrderPostRequest>() {
                @Override
                public void onResponse(Call<OrderPostRequest> call, Response<OrderPostRequest> response) {
                    materialProgressBar.setVisibility(View.GONE);
                    showSendReturn.dismiss();
                    if (isLife) {
                        if (response.body() != null) {
                            if (response.body().getStatus() != null) {
                                if (response.body().getStatus().getIsSuccess()) {
                                    if (response.body().getResult().equals("ok")) {
                                        BaseActivity.isFormReturn = true;
                                        MyApp.getCenterDAO_Return().clearTable();
                                        bottomNavigationView.setSelectedItemId(R.id.history);
                                    }
                                } else {
                                    RubinoToast.showToast(getActivity(), response.body().getStatus().getMessage(), Toast.LENGTH_SHORT, RubinoToast.warning);
                                }

                            } else {
                                RubinoToast.showToast(getActivity(), getString(R.string.error), Toast.LENGTH_SHORT, RubinoToast.warning);
                            }

                        } else {
                            //RubinoToast.showToast(getActivity(),getString(R.string.error), Toast.LENGTH_SHORT,RubinoToast.warning);
                            login(title);
                        }
                    }

                }

                @Override
                public void onFailure(Call<OrderPostRequest> call, Throwable t) {
                    materialProgressBar.setVisibility(View.GONE);
                    RubinoToast.showToast(getActivity(),getActivity().getString(R.string.error),Toast.LENGTH_SHORT,RubinoToast.success);
                }
            });

        }else{
            RubinoToast.showToast(getActivity(),getActivity().getString(R.string.completeForm),Toast.LENGTH_SHORT,RubinoToast.warning);
        }
    }
    private void login(final String title){
        materialProgressBar.setVisibility(View.VISIBLE);
        APIService apiService = ApiUtils.getAPIService();
        Call<LoginRequest> login = apiService.login(MyApp.getMemory().getString(Constants.username, ""), MyApp.getMemory().getString(Constants.password, ""));
        login.enqueue(new Callback<LoginRequest>() {
            @Override
            public void onResponse(Call<LoginRequest> call, Response<LoginRequest> response) {
                if (response.body()!=null){
                    if (response.body().getStatus().getIsSuccess()){
                        if (isLife) {
                            MyApp.getMemory().edit().putString(Constants.tokenId,response.body().getResult().getTokenId()).commit();
                            sendReturnRequest(title);
                        }
                    }else{
                        RubinoToast.showToast(getActivity(),response.body().getStatus().getMessage(), Toast.LENGTH_SHORT,RubinoToast.warning);
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginRequest> call, Throwable t) {
                RubinoToast.showToast(getActivity(),getString(R.string.error), Toast.LENGTH_SHORT,RubinoToast.warning);
            }
        });
    }
    //------retrofit-------

    private boolean checkRadio() {
        boolean b = false;
        String shippmentType = returnOrderSender.getShippmentType();
        if (shippmentType.length()>0){
            if (shippmentType.equals("barbari")){
                String shippmentTitle = returnOrderSender.getShippmentTitle();
                if (shippmentTitle.length()>0){
                    b = true;
                }
            }else{
                b = true;
            }
        }
        return b;
    }

    private void scrollToBottom() {
        recyclerViewAdapterReturnNew.notifyDataSetChanged();
        if (recyclerViewAdapterReturnNew.getItemCount() > 1)
            recyclerView_return.getLayoutManager().smoothScrollToPosition(recyclerView_return, null, recyclerViewAdapterReturnNew.getItemCount() - 1);
    }

    private void internetChecker() {
        boolean online = InternetChecker.isOnline(getActivity());
        if (online){

        }else{
            RubinoToast.showToast(getActivity(),getString(R.string.checkNetwork), Toast.LENGTH_SHORT,RubinoToast.warning);
        }
    }
    //private void showChooser() {
    //    // Use the GET_CONTENT intent from the utility class
    //    //Intent target = FileUtils.createGetContentIntent();
    //    Intent intent = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
    //    // Create the chooser Intent
    //    //Intent intent = Intent.createChooser(target, "chooser_title");
    //    try {
    //        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
    //            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},ATTACH_FILE_REQUEST);
    //            //ActivityCompat.requestPermissions(getActivity(),
    //            //        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
    //            //        2);
    //        }else{
    //            startActivityForResult(intent, ATTACH_FILE_REQUEST);
    //        }
//
    //    } catch (ActivityNotFoundException e) {
    //        // The reason for the existence of aFileChooser
    //    }
    //}
//
    //@Override
    //public void onActivityResult(int requestCode, int resultCode, Intent data) {
    //    super.onActivityResult(requestCode, resultCode, data);
    //    // If the file selection was successful
    //    if (requestCode == ATTACH_FILE_REQUEST && resultCode == RESULT_OK) {
    //        if (data != null) {
    //            // Get the URI of the selected file
    //            final Uri uri = data.getData();
    //            try {
    //                // Get the file path from the URI
    //                final String path = FileUtils.getPath(getActivity(), uri);
    //                Toast.makeText(getActivity(), "File Selected: " + path, Toast.LENGTH_LONG).show();
    //                textView_filePath.setText(path);
    //                //File file = new File(uri.getPath());
//
    //                //Picasso.with(MainActivity.this)
    //                //        .load(file)
    //                //        .placeholder(R.drawable.ic_file)
    //                //        .error(R.drawable.ic_provider)
    //                //        .into(imageView);
//
//
    //            } catch (Exception e) {
    //            }
    //        }else{
    //        }
    //    }else {
    //    }
    //}
    //@Override
    //public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
    //    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    //    //kharabe
    //    Log.i("kk","1");
    //    if (requestCode == ATTACH_FILE_REQUEST && grantResults.length > 0&& grantResults[0] == PackageManager.PERMISSION_GRANTED){
    //        Toast.makeText(getActivity(),"oomad",Toast.LENGTH_SHORT).show();
    //        showChooser();
    //    }
//
    //}







}
