package com.example.mrasus.rubino.Adapters;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.mrasus.rubino.Constants.RegisteredOrders;
import com.example.mrasus.rubino.Fragments.Fragment_ShowRequestReturnProfile;
import com.example.mrasus.rubino.R;
import com.example.mrasus.rubino.activities.BaseActivity;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

/**
 * Created by Mr.Asus on 5/22/2018.
 */

public class RecyclerViewAdapterHistoryReturn extends RecyclerView.Adapter {
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_FOOTER = 1;
    private static final int TYPE_ITEM = 2;
    private Activity activity;
    private List<RegisteredOrders> registeredOrdersesList;


    public RecyclerViewAdapterHistoryReturn(Activity activity, List<RegisteredOrders> list) {
        this.activity = activity;
        registeredOrdersesList = list;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM){
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_registered_return, parent, false);
            return new ItemViewHolder(itemView);
        }else {
            return null;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ItemViewHolder){
            ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
            final RegisteredOrders registeredOrder = registeredOrdersesList.get(position);
            itemViewHolder.textView_date.setText(registeredOrder.getDate());
            itemViewHolder.textView_transferTypeShow.setText(registeredOrder.getTransferType());
            itemViewHolder.textView_conditionShow.setText(registeredOrder.getCondition());
            itemViewHolder.textView_sumAll_number.setText(registeredOrder.getNumber()+"");
            itemViewHolder.textView_returnCode.setText(registeredOrder.getOrderCode());


            itemViewHolder.linearLayout_registeredReturn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Bundle bundle = new Bundle();
                    bundle.putString("OrderCode",registeredOrder.getOrderCode());
                    Fragment fragment_requestProfile = new Fragment_ShowRequestReturnProfile();
                    fragment_requestProfile.setArguments(bundle);
                    android.support.v4.app.FragmentTransaction fragmentTransaction = BaseActivity.fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.frame, fragment_requestProfile ).addToBackStack("tag").commit();
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return registeredOrdersesList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return TYPE_ITEM;
    }

    private class ItemViewHolder extends RecyclerView.ViewHolder {
        TextView textView_returnCode;
        TextView textView_date;
        TextView textView_transferTypeShow;
        TextView textView_transferTypeText;
        TextView textView_conditionShow;
        TextView textView_conditionText;
        TextView textView_sumAll;
        TextView textView_sumAll_number;
        LinearLayout linearLayout_registeredReturn;
        public ItemViewHolder(View itemView) {
            super(itemView);
            textView_returnCode = itemView.findViewById(R.id.textView_returnCode);
            textView_date = itemView.findViewById(R.id.textView_date);
            textView_transferTypeShow = itemView.findViewById(R.id.textView_transferTypeShow);
            textView_transferTypeText = itemView.findViewById(R.id.textView_transferTypeText);
            textView_conditionShow = itemView.findViewById(R.id.textView_conditionShow);
            textView_conditionText = itemView.findViewById(R.id.textView_conditionText);
            textView_sumAll = itemView.findViewById(R.id.textView_sumAll);
            textView_sumAll_number = itemView.findViewById(R.id.textView_sumAll_number);
            linearLayout_registeredReturn = itemView.findViewById(R.id.linearLayout_registeredReturn);


            //textView_orderCode.setTypeface(BaseActivity.appFont);
            textView_date.setTypeface(BaseActivity.appFont);
            textView_transferTypeShow.setTypeface(BaseActivity.appFont);
            textView_transferTypeText.setTypeface(BaseActivity.appFont);
            textView_conditionShow.setTypeface(BaseActivity.appFont);
            textView_conditionText.setTypeface(BaseActivity.appFont);
            textView_sumAll.setTypeface(BaseActivity.appFont);
            textView_sumAll_number.setTypeface(BaseActivity.appFont);
        }
    }
    public String convert(String price){
        NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
        String numberAsString = numberFormat.format(Integer.parseInt(price));
        return numberAsString;
    }
}
