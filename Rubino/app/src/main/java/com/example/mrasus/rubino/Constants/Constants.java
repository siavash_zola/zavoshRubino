package com.example.mrasus.rubino.Constants;

import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.example.mrasus.rubino.Database.MyApp;

/**
 * Created by Mr.Asus on 5/20/2018.
 */

public class Constants {
    public static String tokenId = "tokenId";
    public static String username = "username";
    public static String password = "password";

    public static String brandName = "brandName";

    public static void hideKeyboard(Activity activity,View view) {
        InputMethodManager inputMethodManager =(InputMethodManager)activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static boolean checkLanguage(String txt){
        int checker = 0;
        for (int i = 0; i < MyApp.filterLanguage.size(); i++) {
            checker = txt.indexOf(MyApp.filterLanguage.get(i));
            if (checker != -1){
                return false;
            }
        }

        return true;
    }
}
