package com.example.mrasus.rubino.Fragments;


import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.mrasus.rubino.Adapters.RecyclerViewAdapterRequestTable;
import com.example.mrasus.rubino.Constants.Product;
import com.example.mrasus.rubino.Database.MyApp;
import com.example.mrasus.rubino.R;
import com.example.mrasus.rubino.activities.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import me.zhanghai.android.materialprogressbar.MaterialProgressBar;

/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment_requestProfile extends Fragment {
    private RecyclerView recyclerView_table;
    private List<Product> productList;
    private RecyclerViewAdapterRequestTable recyclerViewAdapterRequestTable;
    private LinearLayoutManager layoutManager;
    private MaterialProgressBar materialProgressBar;

    public Fragment_requestProfile() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_fragment_request_profile, container, false);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        toolbar();
        reference(view);

    }

    @Override
    public void onResume() {
        super.onResume();
        materialProgressBar.setVisibility(View.GONE);
    }

    private void toolbar() {
        TextView textView = getActivity().findViewById(R.id.textView_toolbar);
        textView.setText(getString(R.string.invoice));
    }

    private void reference(View view) {
        recyclerView_table = (RecyclerView) view.findViewById(R.id.recyclerView_table);
        recyclerViewAdapterRequestTable = new RecyclerViewAdapterRequestTable(getActivity(), MyApp.getCenterDAO_Order().getAllProductSelected());
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView_table.setLayoutManager(layoutManager);
        recyclerView_table.setAdapter(recyclerViewAdapterRequestTable);
        materialProgressBar = getActivity().findViewById(R.id.materialProgressBar);
        //-------test

    }



}

