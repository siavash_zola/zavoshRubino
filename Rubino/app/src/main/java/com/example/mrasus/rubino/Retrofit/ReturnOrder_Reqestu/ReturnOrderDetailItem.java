package com.example.mrasus.rubino.Retrofit.ReturnOrder_Reqestu;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Mr.Asus on 5/22/2018.
 */

public class ReturnOrderDetailItem {
    @SerializedName("ProductSerialNumber")
    @Expose
    private String productSerialNumber;
    @SerializedName("ReturnReason")
    @Expose
    private String returnReason;

    public String getProductSerialNumber() {
        return productSerialNumber;
    }

    public void setProductSerialNumber(String productSerialNumber) {
        this.productSerialNumber = productSerialNumber;
    }

    public String getReturnReason() {
        return returnReason;
    }

    public void setReturnReason(String returnReason) {
        this.returnReason = returnReason;
    }
}
