package com.example.mrasus.rubino.Retrofit.GetAllMessage_Request;

import com.example.mrasus.rubino.Retrofit.Login_Request.Status;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Mr.Asus on 5/28/2018.
 */

public class GetAllMessageRequest {
    @SerializedName("result")
    @Expose
    private List<Result> result = null;
    @SerializedName("status")
    @Expose
    private Status status;

    public List<Result> getResult() {
        return result;
    }

    public void setResult(List<Result> result) {
        this.result = result;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
