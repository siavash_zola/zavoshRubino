package com.example.mrasus.rubino.Adapters;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.mrasus.rubino.Constants.RegisteredOrders;
import com.example.mrasus.rubino.Fragments.Fragment_ShowRequestProfile;
import com.example.mrasus.rubino.R;
import com.example.mrasus.rubino.activities.BaseActivity;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

/**
 * Created by Mr.Asus on 5/5/2018.
 */

public class RecyclerViewAdapterHistory extends RecyclerView.Adapter {
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_FOOTER = 1;
    private static final int TYPE_ITEM = 2;
    private Activity activity;
    private List<RegisteredOrders> registeredOrdersesList;


    public RecyclerViewAdapterHistory(Activity activity, List<RegisteredOrders> list) {
        this.activity = activity;
        registeredOrdersesList = list;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM){
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_registered_oreder, parent, false);
            return new ItemViewHolder(itemView);
        }else {
            return null;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ItemViewHolder){
            ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
            itemViewHolder.linearLayout_header_item.setBackgroundResource(R.drawable.table_header_background);
            final RegisteredOrders registeredOrder = registeredOrdersesList.get(position);
            itemViewHolder.textView_date.setText(registeredOrder.getDate());
            itemViewHolder.textView_orderCode.setText(registeredOrder.getOrderCode());
            itemViewHolder.textView_payTypeShow.setText(registeredOrder.getPaymentType());
            itemViewHolder.textView_transferTypeShow.setText(registeredOrder.getTransferType());
            itemViewHolder.textView_conditionShow.setText(registeredOrder.getCondition());
            itemViewHolder.textView_totalShow.setText(convert(registeredOrder.getTotal()));
            itemViewHolder.textView_sumAll_number.setText(registeredOrder.getNumber()+"");


            itemViewHolder.linearLayout_registeredOrder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Bundle bundle = new Bundle();
                    bundle.putString("orderCode",registeredOrder.getOrderCode());
                    Fragment fragment_requestProfile = new Fragment_ShowRequestProfile();
                    fragment_requestProfile.setArguments(bundle);
                    android.support.v4.app.FragmentTransaction fragmentTransaction = BaseActivity.fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.frame, fragment_requestProfile ).addToBackStack( "tag" ).commit();
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return registeredOrdersesList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return TYPE_ITEM;
    }

    private class ItemViewHolder extends RecyclerView.ViewHolder {
        TextView textView_orderCode;
        TextView textView_date;
        TextView textView_payTypeShow;
        TextView textView_payTypeText;
        TextView textView_transferTypeShow;
        TextView textView_transferTypeText;
        TextView textView_conditionShow;
        TextView textView_conditionText;
        TextView textView_totalShow;
        TextView textView_totalText;
        TextView textView_sumAll;
        TextView textView_sumAll_number;
        LinearLayout linearLayout_registeredOrder;
        LinearLayout linearLayout_header_item;
        public ItemViewHolder(View itemView) {
            super(itemView);
            textView_orderCode = itemView.findViewById(R.id.textView_orderCode);
            textView_date = itemView.findViewById(R.id.textView_date);
            textView_payTypeShow = itemView.findViewById(R.id.textView_payTypeShow);
            textView_payTypeText = itemView.findViewById(R.id.textView_payTypeText);
            textView_transferTypeShow = itemView.findViewById(R.id.textView_transferTypeShow);
            textView_transferTypeText = itemView.findViewById(R.id.textView_transferTypeText);
            textView_conditionShow = itemView.findViewById(R.id.textView_conditionShow);
            textView_conditionText = itemView.findViewById(R.id.textView_conditionText);
            textView_totalShow = itemView.findViewById(R.id.textView_totalShow);
            textView_totalText = itemView.findViewById(R.id.textView_totalText);
            textView_sumAll = itemView.findViewById(R.id.textView_sumAll);
            textView_sumAll_number = itemView.findViewById(R.id.textView_sumAll_number);
            linearLayout_registeredOrder = itemView.findViewById(R.id.linearLayout_registeredOrder);
            linearLayout_header_item = itemView.findViewById(R.id.linearLayout_header_item);


            //textView_orderCode.setTypeface(BaseActivity.appFont);
            textView_date.setTypeface(BaseActivity.appFont);
            textView_payTypeShow.setTypeface(BaseActivity.appFont);
            textView_payTypeText.setTypeface(BaseActivity.appFont);
            textView_transferTypeShow.setTypeface(BaseActivity.appFont);
            textView_transferTypeText.setTypeface(BaseActivity.appFont);
            textView_conditionShow.setTypeface(BaseActivity.appFont);
            textView_conditionText.setTypeface(BaseActivity.appFont);
            textView_totalShow.setTypeface(BaseActivity.appFont);
            textView_totalText.setTypeface(BaseActivity.appFont);
            textView_sumAll.setTypeface(BaseActivity.appFont);
            textView_sumAll_number.setTypeface(BaseActivity.appFont);
        }
    }
    public String convert(String price){
        NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
        String numberAsString = numberFormat.format(Integer.parseInt(price));
        return numberAsString;
    }
}
