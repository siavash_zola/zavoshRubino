package com.example.mrasus.rubino.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Mr.Asus on 5/6/2018.
 */

public class MyDataBaseOpenHelper extends SQLiteOpenHelper {
    public MyDataBaseOpenHelper(Context context) {
        super(context, SqlConstant.DATABASE_NAME,null,SqlConstant.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        Log.i("db", "ghable sakhtan");
        //sqLiteDatabase.execSQL(SqlConstant.CREATE_RETURN_TABLE);
        sqLiteDatabase.execSQL(SqlConstantReturn.CREATE_RETURN_TABLE);
        Log.i("db","sakhte shod");
        sqLiteDatabase.execSQL(SqlConstant.CREATE_ORDER_TABLE);
        Log.i("db","dovomiam sakht");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
