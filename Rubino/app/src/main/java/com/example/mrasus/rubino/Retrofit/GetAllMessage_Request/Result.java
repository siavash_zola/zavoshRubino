package com.example.mrasus.rubino.Retrofit.GetAllMessage_Request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Mr.Asus on 5/28/2018.
 */

public class Result {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("subject")
    @Expose
    private String subject;
    @SerializedName("body")
    @Expose
    private String body;
    @SerializedName("submitDate")
    @Expose
    private String submitDate;
    @SerializedName("typeTitle")
    @Expose
    private String typeTitle;
    @SerializedName("typeCode")
    @Expose
    private Integer typeCode;
    @SerializedName("isReaded")
    @Expose
    private Boolean isReaded;
    @SerializedName("attachmentUrl")
    @Expose
    private String attachmentUrl;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getSubmitDate() {
        return submitDate;
    }

    public void setSubmitDate(String submitDate) {
        this.submitDate = submitDate;
    }

    public String getTypeTitle() {
        return typeTitle;
    }

    public void setTypeTitle(String typeTitle) {
        this.typeTitle = typeTitle;
    }

    public Integer getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(Integer typeCode) {
        this.typeCode = typeCode;
    }

    public Boolean getIsReaded() {
        return isReaded;
    }

    public void setIsReaded(Boolean isReaded) {
        this.isReaded = isReaded;
    }

    public String getAttachmentUrl() {
        return attachmentUrl;
    }

    public void setAttachmentUrl(String attachmentUrl) {
        this.attachmentUrl = attachmentUrl;
    }


    @Override
    public String toString() {
        return "Result{" +
                "id='" + id + '\'' +
                ", subject='" + subject + '\'' +
                ", body='" + body + '\'' +
                ", submitDate='" + submitDate + '\'' +
                ", typeTitle='" + typeTitle + '\'' +
                ", typeCode=" + typeCode +
                ", isReaded=" + isReaded +
                ", attachmentUrl='" + attachmentUrl + '\'' +
                '}';
    }
}
