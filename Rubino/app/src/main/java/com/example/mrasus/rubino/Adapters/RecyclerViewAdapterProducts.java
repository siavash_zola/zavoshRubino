package com.example.mrasus.rubino.Adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.example.mrasus.rubino.Constants.Product;
import com.example.mrasus.rubino.Database.MyApp;
import com.example.mrasus.rubino.R;
import com.example.mrasus.rubino.Retrofit.ApiUtils;
import com.example.mrasus.rubino.activities.BaseActivity;
import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

/**
 * Created by Mr.Asus on 5/1/2018.
 */

public class RecyclerViewAdapterProducts extends RecyclerView.Adapter{
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_FOOTER = 1;
    private static final int TYPE_ITEM = 2;
    private List<Product> productList;
    private Activity activity;
    private ViewSwitcher viewSwitcher;
    private boolean isShowBtn = false;

    public RecyclerViewAdapterProducts(Activity activity, List<Product> data) {
        this.activity = activity;
        this.productList = data;


        //namayeshe btn
        int sumNumber = MyApp.getCenterDAO_Order().getSumNumber();

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM){
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.show_details_product_new, parent, false);
            return new ItemViewHolder(itemView);
        }else if (viewType == TYPE_FOOTER){
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.space_for_recycler, parent, false);
            return new FooterViewHolder(itemView);
        }else {
            return null;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof ItemViewHolder){
            final ItemViewHolder itemViewHolder = (ItemViewHolder)holder;
            final Product product = productList.get(position);
            itemViewHolder.textView_showTitle.setText(product.getTitle());
            //baraye load kardane listi ke sefaresh dade
            itemViewHolder.textView_number.setText(MyApp.getCenterDAO_Order().getNumberProduct(product)+"");
            itemViewHolder.textView_size.setText(product.getSize());
            itemViewHolder.textView_price.setText(convert(product.getPrice()));

            Picasso.with(activity)
                    .load(product.getImageUrl())
                    .placeholder(R.drawable.pre_view_picaso)
                    .error(R.drawable.pre_view_picaso)
                    .into(itemViewHolder.imageView_large);

            itemViewHolder.linearLayout_minus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    MyApp.getCenterDAO_Order().deleteProduct(product);
                    itemViewHolder.textView_number.setText(MyApp.getCenterDAO_Order().getNumberProduct(product)+"");

                }
            });
            itemViewHolder.linearLayout_plus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    MyApp.getCenterDAO_Order().addProduct(product);
                    itemViewHolder.textView_number.setText(MyApp.getCenterDAO_Order().getNumberProduct(product)+"");

                }
            });
        }
    }//sefareshe az grooh haye digar

    @Override
    public int getItemCount() {
        return productList.size()+1;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == productList.size()){
            return TYPE_FOOTER;
        }else {
            return TYPE_ITEM;
        }
    }

    private class ItemViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView_large;
        TextView textView_showTitle;
        TextView textView_size;
        TextView textView_price;
        TextView textView_number;
        TextView textView_price_title;
        LinearLayout linearLayout_plus;
        LinearLayout linearLayout_minus;
        public ItemViewHolder(View itemView) {
            super(itemView);
            imageView_large = itemView.findViewById(R.id.imageView_large);
            textView_showTitle = itemView.findViewById(R.id.textView_show_title);
            textView_size = itemView.findViewById(R.id.textView_size);
            textView_price = itemView.findViewById(R.id.textView_price);
            textView_number = itemView.findViewById(R.id.textView_number);
            textView_price_title = itemView.findViewById(R.id.textView_price_title);
            linearLayout_plus = itemView.findViewById(R.id.linearLayout_plus);
            linearLayout_minus = itemView.findViewById(R.id.linearLayout_minus);

            textView_showTitle.setTypeface(BaseActivity.appFont);
            textView_size.setTypeface(BaseActivity.appFont);
            textView_price.setTypeface(BaseActivity.appFont);
            textView_number.setTypeface(BaseActivity.appFont);
            textView_price_title.setTypeface(BaseActivity.appFont);


        }
    }

    private class FooterViewHolder extends RecyclerView.ViewHolder {
        public FooterViewHolder(View itemView) {
            super(itemView);
        }
    }

    public String convert(String price){
        NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
        String numberAsString = numberFormat.format(Integer.parseInt(price));
        return numberAsString;
    }
}
