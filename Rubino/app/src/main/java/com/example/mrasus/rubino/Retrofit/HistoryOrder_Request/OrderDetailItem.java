package com.example.mrasus.rubino.Retrofit.HistoryOrder_Request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Mr.Asus on 5/26/2018.
 */

public class OrderDetailItem {
    @SerializedName("productTitle")
    @Expose
    private String productTitle;
    @SerializedName("price")
    @Expose
    private Integer price;
    @SerializedName("amount")
    @Expose
    private Integer amount;
    @SerializedName("quantity")
    @Expose
    private Integer quantity;

    public String getProductTitle() {
        return productTitle;
    }

    public void setProductTitle(String productTitle) {
        this.productTitle = productTitle;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

}
