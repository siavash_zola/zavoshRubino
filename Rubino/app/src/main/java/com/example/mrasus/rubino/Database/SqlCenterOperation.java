package com.example.mrasus.rubino.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.mrasus.rubino.Constants.Product;
import com.example.mrasus.rubino.Retrofit.OrderPost_Request.OrderDetailItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mr.Asus on 5/6/2018.
 */

public class SqlCenterOperation implements CenterDAO{
    private SQLiteDatabase db;
    private Context context;
    public SqlCenterOperation(Context context) {
        MyDataBaseOpenHelper myDataBaseOpenHelper = new MyDataBaseOpenHelper(context);
        db = myDataBaseOpenHelper.getWritableDatabase();
        this.context = context;
    }

    @Override
    public void addProduct(Product product) {
        if (isExistProduct(product)) {
            updateProduct(product,1);
        }else{
            ContentValues values = new ContentValues();
            values.put(SqlConstant.COLUMN_1, product.getId());
            values.put(SqlConstant.COLUMN_2, product.getTitle());
            values.put(SqlConstant.COLUMN_3, product.getPrice());
            values.put(SqlConstant.COLUMN_4, 1);
            db.insert(SqlConstant.TABLE_NAME, null, values);
        }
    }

    @Override
    public void deleteProduct(Product product) {
        if (isExistProduct(product)){
            int numberProduct = getNumberProduct(product);
            if (numberProduct>1){
                updateProduct(product,0);
            }else {
                db.delete(SqlConstant.TABLE_NAME, "productID = ?", new String[]{"" + product.getId()});
            }
        }
    }

    @Override
    public boolean isExistProduct(Product product) {
        Cursor cursor = db.query(SqlConstant.TABLE_NAME,null,null,null,null,null,null);
        String id = product.getId();
        boolean b = false;
        while (cursor.moveToNext()){
            String productId = cursor.getString(cursor.getColumnIndex(SqlConstant.COLUMN_1));
            //Log.i("db",productId);
            if (productId.equals(id)){
                Log.i("db",productId);
                b = true;
            }
        }
        return b;
    }

    @Override
    public int getNumberProduct(Product product) {
        Cursor cursor = db.query(SqlConstant.TABLE_NAME, null, null, null, null, null, null);
        String id = product.getId() + "";
        int number = 0;
        while (cursor.moveToNext()){
            String proId = cursor.getString(cursor.getColumnIndex(SqlConstant.COLUMN_1));
            if (proId.equals(id)){
                number = cursor.getInt(cursor.getColumnIndex(SqlConstant.COLUMN_4));
            }
        }
        return number;
    }

    @Override
    public void updateProduct(Product product, int moreOrLess) {
        int numberProduct = getNumberProduct(product);
        if (numberProduct >0 && moreOrLess == 0) {
            numberProduct--;
        }else if (moreOrLess == 1){
            numberProduct++;
        }
        ContentValues values = new ContentValues();
        values.put(SqlConstant.COLUMN_1,product.getId());
        values.put(SqlConstant.COLUMN_2,product.getTitle());
        values.put(SqlConstant.COLUMN_3,product.getPrice());
        values.put(SqlConstant.COLUMN_4,numberProduct);
        db.update(SqlConstant.TABLE_NAME, values, "productID = ?",new String[]{product.getId()+""});
        Log.i("jj","updated");

    }

    @Override
    public List<Product> getAllProductSelected() {
        List<Product> productList = new ArrayList<>();
        Cursor query = db.query(SqlConstant.TABLE_NAME, null, null, null, null, null, null);
        while (query.moveToNext()){
            Product product = new Product();
            product.setId(query.getString(query.getColumnIndex(SqlConstant.COLUMN_1)));
            product.setTitle(query.getString(query.getColumnIndex(SqlConstant.COLUMN_2)));
            product.setPrice(query.getString(query.getColumnIndex(SqlConstant.COLUMN_3)));
            product.setNumber(query.getInt(query.getColumnIndex(SqlConstant.COLUMN_4)));
            productList.add(product);

        }
        return productList;
    }

    @Override
    public int getSumNumber() {
        Cursor query = db.query(SqlConstant.TABLE_NAME, null, null, null, null, null, null);
        int counter = 0;
        while (query.moveToNext()){
            int numberProduct = query.getInt(query.getColumnIndex(SqlConstant.COLUMN_4));
            counter = counter + numberProduct;
        }

        return counter;
    }

    @Override
    public void clearTable() {
        db.delete(SqlConstant.TABLE_NAME,null,null);
    }

    @Override
    public List<OrderDetailItem> getOrdersFromDataBase() {
        return null;
    }
}
