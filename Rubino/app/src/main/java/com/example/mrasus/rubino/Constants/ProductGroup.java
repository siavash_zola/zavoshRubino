package com.example.mrasus.rubino.Constants;

/**
 * Created by Mr.Asus on 4/29/2018.
 */

public class ProductGroup {
    private String imageUrl;
    private String productName;
    private String productId;
    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }


    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }
}
