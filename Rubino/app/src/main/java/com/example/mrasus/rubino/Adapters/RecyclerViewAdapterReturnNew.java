package com.example.mrasus.rubino.Adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.mrasus.rubino.Constants.ReturnProduct;
import com.example.mrasus.rubino.Database.MyApp;
import com.example.mrasus.rubino.R;
import com.example.mrasus.rubino.activities.BaseActivity;

import java.util.List;

/**
 * Created by Mr.Asus on 5/13/2018.
 */

public class RecyclerViewAdapterReturnNew extends RecyclerView.Adapter {
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_FOOTER = 1;
    private static final int TYPE_ITEM = 2;
    private Activity activity;
    private TextView textView_no_return;
    private List<ReturnProduct> returnProductList;

    public RecyclerViewAdapterReturnNew(Activity activity, List<ReturnProduct> returnProductList) {
        this.activity = activity;
        this.returnProductList = returnProductList;
        textView_no_return = activity.findViewById(R.id.textView_no_return);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM){
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.return_item_new, parent, false);
            return new ItemViewHolder(itemView);
        }else if (viewType == TYPE_FOOTER){
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.space_for_recycler, parent, false);
            return new FooterViewHolder(itemView);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ItemViewHolder){
            ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
            final ReturnProduct returnProduct = returnProductList.get(position);
            itemViewHolder.textView_serial.setText(returnProduct.getSerial());
            itemViewHolder.textView_reasonText.setText(returnProduct.getReason());


            itemViewHolder.linearLayout_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    MyApp.getCenterDAO_Return().deleteProduct(returnProduct);
                    inputList(MyApp.getCenterDAO_Return().getAllProductSelectedForReturn());
                    if (returnProductList.size() == 0){
                        textView_no_return.setText(R.string.noProductForReturn);
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return returnProductList.size()+1;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == returnProductList.size()){
            return TYPE_FOOTER;
        }
        return TYPE_ITEM;
    }

    private class ItemViewHolder extends RecyclerView.ViewHolder {
        TextView textView_serial_title,textView_reasonTitle,textView_reasonText,textView_serial;
        LinearLayout linearLayout_cancel;
        public ItemViewHolder(View itemView) {
            super(itemView);
            textView_serial_title = itemView.findViewById(R.id.textView_serial_title);
            textView_reasonTitle = itemView.findViewById(R.id.textView_reasonTitle);
            textView_reasonText = itemView.findViewById(R.id.textView_reasonText);
            textView_serial = itemView.findViewById(R.id.textView_serial);
            linearLayout_cancel = itemView.findViewById(R.id.linearLayout_cancel);

            textView_serial_title.setTypeface(BaseActivity.appFont_Bold);
            textView_reasonTitle.setTypeface(BaseActivity.appFont);
            textView_reasonText.setTypeface(BaseActivity.appFont);
            textView_serial.setTypeface(BaseActivity.appFont_Bold);


        }
    }
    private class FooterViewHolder extends RecyclerView.ViewHolder {
        public FooterViewHolder(View itemView) {
            super(itemView);
        }
    }

    public void inputList(List<ReturnProduct> dbList) {
        returnProductList.clear();
        for (int i = 0; i < dbList.size(); i++) {
            returnProductList.add(dbList.get(i));
        }
        notifyDataSetChanged();
    }

}
