package com.example.mrasus.rubino.Constants;

/**
 * Created by Mr.Asus on 5/15/2018.
 */

public class Attach {
    private String attachName;
    private String attachUrl;

    public String getAttachName() {
        return attachName;
    }

    public void setAttachName(String attachName) {
        this.attachName = attachName;
    }

    public String getAttachUrl() {
        return attachUrl;
    }

    public void setAttachUrl(String attachUrl) {
        this.attachUrl = attachUrl;
    }
}
