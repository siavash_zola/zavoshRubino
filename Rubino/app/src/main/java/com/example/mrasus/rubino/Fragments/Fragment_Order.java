package com.example.mrasus.rubino.Fragments;


import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mrasus.rubino.Adapters.RecyclerViewAdapterHome;
import com.example.mrasus.rubino.Constants.Constants;
import com.example.mrasus.rubino.Constants.InternetChecker;
import com.example.mrasus.rubino.Constants.Product;
import com.example.mrasus.rubino.Constants.ProductGroup;
import com.example.mrasus.rubino.Database.MyApp;
import com.example.mrasus.rubino.MyToast.RubinoToast;
import com.example.mrasus.rubino.R;
import com.example.mrasus.rubino.Retrofit.APIService;
import com.example.mrasus.rubino.Retrofit.ApiUtils;
import com.example.mrasus.rubino.Retrofit.Login_Request.LoginRequest;
import com.example.mrasus.rubino.Retrofit.Product_Category.ProductCategory;
import com.example.mrasus.rubino.Retrofit.Product_Category.Result;
import com.example.mrasus.rubino.Retrofit.Product_Category.Sender;
import com.example.mrasus.rubino.activities.BaseActivity;
import com.example.mrasus.rubino.activities.Splash;

import java.util.ArrayList;
import java.util.List;

import me.zhanghai.android.materialprogressbar.MaterialProgressBar;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment_Order extends Fragment {

    private RecyclerView recyclerView_products;
    private RecyclerViewAdapterHome recyclerViewAdapterHome;
    private List<ProductGroup> productList;
    private GridLayoutManager mGridLayoutManager;
    private MaterialProgressBar materialProgressBar;
    private SwipeRefreshLayout refreshLayout;
    private boolean isLife = true;
    private String brand = "";
    //private int oneRequest= 0;


    public Fragment_Order() {
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fragment__order, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.i("life","onViewCreated");
        toolbar();
        reference(view);
        listener();
        internetChecker();
    }

    @Override
    public void onResume() {
        super.onResume();
        isLife = true;
        Log.i("life","onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        isLife = false;
        materialProgressBar.setVisibility(View.GONE);
        Log.i("life","onPause");
    }

    private void listener() {
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //oneRequest = 0;
                internetChecker();
            }
        });
    }

    //---retrofit------------
    private void getProductGroup() {
        materialProgressBar.setVisibility(View.VISIBLE);
        APIService apiService = ApiUtils.getAPIService();
        Sender sender = new Sender();
        sender.setBrandName(brand);
        Call<ProductCategory> productCategoryCall = apiService.categoryGrp(MyApp.getMemory().getString(Constants.tokenId,""),sender);
        Log.i("retrofit",MyApp.getMemory().getString(Constants.tokenId,""));
        productCategoryCall.enqueue(new Callback<ProductCategory>() {
            @Override
            public void onResponse(Call<ProductCategory> call, Response<ProductCategory> response) {
                materialProgressBar.setVisibility(View.GONE);
                refreshLayout.setRefreshing(false);
                if (isLife) {
                    Log.i("kk","1");
                    if (response.body() != null) {
                        Log.i("kk","2");
                        if (response.body().getStatus() != null) {
                            Log.i("kk","3");
                            if (response.body().getStatus().getIsSuccess()) {
                                Log.i("kk","4");
                                ProductCategory productCategory = response.body();
                                productList.clear();
                                inputList(productCategory.getResult());
                                BaseActivity.unFreeze();
                            } else {
                                Log.i("kk","5");
                                RubinoToast.showToast(getActivity(), response.body().getStatus().getMessage(), Toast.LENGTH_SHORT, RubinoToast.warning);
                                BaseActivity.unFreeze();
                            }
                        } else {
                            Log.i("kk","6");
                            RubinoToast.showToast(getActivity(), getString(R.string.error), Toast.LENGTH_SHORT, RubinoToast.warning);
                            BaseActivity.unFreeze();
                        }
                    } else {
                        Log.i("kk","7");
                        login();
                    }
                }
            }

            @Override
            public void onFailure(Call<ProductCategory> call, Throwable t) {
                materialProgressBar.setVisibility(View.GONE);
                refreshLayout.setRefreshing(false);
                RubinoToast.showToast(getActivity(),getString(R.string.error), Toast.LENGTH_SHORT,RubinoToast.warning);
                BaseActivity.unFreeze();
            }
        });
    }
    private void login(){
        materialProgressBar.setVisibility(View.VISIBLE);
        APIService apiService = ApiUtils.getAPIService();
        Call<LoginRequest> login = apiService.login(MyApp.getMemory().getString(Constants.username, ""), MyApp.getMemory().getString(Constants.password, ""));
        login.enqueue(new Callback<LoginRequest>() {
            @Override
            public void onResponse(Call<LoginRequest> call, Response<LoginRequest> response) {
                if (response.body()!=null){
                    if (response.body().getStatus().getIsSuccess()){
                        if (isLife) {
                            MyApp.getMemory().edit().putString(Constants.tokenId, response.body().getResult().getTokenId()).commit();
                            getProductGroup();
                        }
                    }else{
                        RubinoToast.showToast(getActivity(),response.body().getStatus().getMessage(), Toast.LENGTH_SHORT,RubinoToast.warning);
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginRequest> call, Throwable t) {
                RubinoToast.showToast(getActivity(),getString(R.string.error), Toast.LENGTH_SHORT,RubinoToast.warning);
            }
        });

    }
    //---retrofit------------

    private void inputList(List<Result> resultList) {
        for (int i = 0; i < resultList.size(); i++) {
            Result result = resultList.get(i);
            ProductGroup productGroup = new ProductGroup();
            productGroup.setImageUrl(result.getImageUrl());
            productGroup.setProductId(result.getId());
            productGroup.setProductName(result.getTitle());
            productList.add(productGroup);
            recyclerViewAdapterHome.notifyDataSetChanged();
        }
    }

    private void toolbar() {
        TextView textView = getActivity().findViewById(R.id.textView_toolbar);
        textView.setText(getString(R.string.order));
        this.brand = getArguments().getString(Constants.brandName);
    }
    private void reference(View view) {
        refreshLayout = view.findViewById(R.id.refresh);
        refreshLayout.setColorSchemeResources(R.color.colorPrimary);
        materialProgressBar = getActivity().findViewById(R.id.materialProgressBar);
        recyclerView_products = (RecyclerView) view.findViewById(R.id.recyclerView_products);
        productList = new ArrayList<>();
        recyclerViewAdapterHome = new RecyclerViewAdapterHome(getActivity(),productList);
        mGridLayoutManager = new GridLayoutManager(getActivity(), 2);
        recyclerView_products.setLayoutManager(mGridLayoutManager);
        recyclerView_products.setAdapter(recyclerViewAdapterHome);

    }
    private void internetChecker() {
        boolean online = InternetChecker.isOnline(getActivity());
        if (online){
            //if (oneRequest == 0) {
            //    BaseActivity.freeze(getActivity());
            //    getProductGroup();
            //    oneRequest = 1;
            //}
            if (!BaseActivity.isFreezed) {
                BaseActivity.freeze(getActivity());
            }
            getProductGroup();
        }else{
            RubinoToast.showToast(getActivity(),getString(R.string.checkNetwork), Toast.LENGTH_SHORT,RubinoToast.warning);
            refreshLayout.setRefreshing(false);
        }
    }



}
