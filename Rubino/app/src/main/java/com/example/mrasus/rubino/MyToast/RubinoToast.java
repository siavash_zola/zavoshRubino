package com.example.mrasus.rubino.MyToast;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mrasus.rubino.R;
import com.example.mrasus.rubino.activities.BaseActivity;

/**
 * Created by Mr.Asus on 5/15/2018.
 */

public class RubinoToast {
    public static int success = 0;
    public static int information = 1;
    public static int warning = 2;
    public static void showToast(Context context,String message,int time,int imgCode){
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.toast_layout,null);

        ImageView image = (ImageView) layout.findViewById(R.id.image);
        switch (imgCode){
            case 0:
                image.setImageResource(R.drawable.success);
                break;
            case 1:
                image.setImageResource(R.drawable.information);
                break;
            case 2:
                image.setImageResource(R.drawable.warning);
                break;
        }
        //image.setImageResource(R.drawable.logo);
        TextView text = (TextView) layout.findViewById(R.id.text);
        text.setText(message);
        text.setTypeface(BaseActivity.appFont);

        Toast toast = new Toast(context);
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(time);
        toast.setView(layout);
        toast.show();
    }
}
