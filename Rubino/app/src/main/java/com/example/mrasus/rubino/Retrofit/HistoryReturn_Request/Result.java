package com.example.mrasus.rubino.Retrofit.HistoryReturn_Request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Mr.Asus on 5/27/2018.
 */

public class Result {
    @SerializedName("returnOrderDetailItems")
    @Expose
    private List<ReturnOrderDetailItem> returnOrderDetailItems = null;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("submitDate")
    @Expose
    private String submitDate;
    @SerializedName("shippmentTitle")
    @Expose
    private String shippmentTitle;
    @SerializedName("shippmetType")
    @Expose
    private String shippmetType;

    public List<ReturnOrderDetailItem> getReturnOrderDetailItems() {
        return returnOrderDetailItems;
    }

    public void setReturnOrderDetailItems(List<ReturnOrderDetailItem> returnOrderDetailItems) {
        this.returnOrderDetailItems = returnOrderDetailItems;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getSubmitDate() {
        return submitDate;
    }

    public void setSubmitDate(String submitDate) {
        this.submitDate = submitDate;
    }

    public String getShippmentTitle() {
        return shippmentTitle;
    }

    public void setShippmentTitle(String shippmentTitle) {
        this.shippmentTitle = shippmentTitle;
    }

    public String getShippmetType() {
        return shippmetType;
    }

    public void setShippmetType(String shippmetType) {
        this.shippmetType = shippmetType;
    }
}
