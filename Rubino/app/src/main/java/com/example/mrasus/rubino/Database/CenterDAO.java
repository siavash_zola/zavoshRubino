package com.example.mrasus.rubino.Database;

import com.example.mrasus.rubino.Constants.Product;
import com.example.mrasus.rubino.Retrofit.OrderPost_Request.OrderDetailItem;

import java.util.List;

/**
 * Created by Mr.Asus on 5/6/2018.
 */

public interface CenterDAO {
    void addProduct(Product product);
    void deleteProduct (Product product);
    boolean isExistProduct (Product product);
    int getNumberProduct (Product product);
    void updateProduct(Product product,int moreOrLess);
    List<Product> getAllProductSelected ();
    int getSumNumber();
    void clearTable();
    List<OrderDetailItem> getOrdersFromDataBase();
}
