package com.example.mrasus.rubino.activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mrasus.rubino.Constants.Constants;
import com.example.mrasus.rubino.Constants.InternetChecker;
import com.example.mrasus.rubino.Database.MyApp;
import com.example.mrasus.rubino.MyToast.RubinoToast;
import com.example.mrasus.rubino.R;
import com.example.mrasus.rubino.Retrofit.APIService;
import com.example.mrasus.rubino.Retrofit.ApiUtils;
import com.example.mrasus.rubino.Retrofit.Brand_Request.BrandRequest;
import com.example.mrasus.rubino.Retrofit.Brand_Request.Result;
import com.example.mrasus.rubino.Retrofit.Login_Request.LoginRequest;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Splash extends AppCompatActivity {
    private LinearLayout linearLayout_splash,linearLayout_padisan;
    private ImageView imageView_rubino,imageView_padisan;
    private TextView textView_rubino;
    private TextView textView_padisan;
    private Typeface appFont;
    private AlertDialog show;
    private List<Result> brandList;
    private Animation goLeft;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //status bar remove just in this activity
        setContentView(R.layout.activity_splash);
        reference();
        internetChecker();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        goLeft = AnimationUtils.loadAnimation(Splash.this, R.anim.go_left_drawer);
        linearLayout_splash = (LinearLayout) findViewById(R.id.linearLayout_splash);
        linearLayout_padisan = (LinearLayout) findViewById(R.id.linearLayout_splash_padisan);


        linearLayout_splash.setVisibility(View.GONE);
        linearLayout_padisan.setVisibility(View.GONE);

        linearLayout_splash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Splash.this, BaseActivity.class);
                intent.putExtra(Constants.brandName, brandList.get(1).getName());
                startActivity(intent);
            }
        });

        linearLayout_padisan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Splash.this, BaseActivity.class);
                intent.putExtra(Constants.brandName, brandList.get(0).getName());
                startActivity(intent);
            }
        });

        //startThread();

    }

    @Override
    protected void onResume() {
        super.onResume();
        String token = MyApp.getMemory().getString(Constants.tokenId, "");
        String phone = MyApp.getMemory().getString(Constants.username, "");
        String pass = MyApp.getMemory().getString(Constants.password, "");
        if (token.length() == 0 || phone.length() == 0 || pass.length() == 0){
            Intent intent = new Intent(Splash.this,LoginActivity.class);
            startActivity(intent);
            finish();
        }
    }

    private void reference() {
        imageView_rubino = (ImageView) findViewById(R.id.imageView_rubino);
        imageView_padisan = (ImageView) findViewById(R.id.imageView_padisan);
        textView_rubino = (TextView) findViewById(R.id.textView_rubino);
        textView_padisan = (TextView) findViewById(R.id.textView_padisan);
        appFont = Typeface.createFromAsset(getAssets(), "fonts/IRANSansMobile(FaNum)_Light.ttf");
    }

    //---retrofit-----
    private void getBrand() {
        APIService apiService = ApiUtils.getAPIService();
        Call<BrandRequest> brand = apiService.getBrand();
        brand.enqueue(new Callback<BrandRequest>() {
            @Override
            public void onResponse(Call<BrandRequest> call, Response<BrandRequest> response) {
                if (response.body() != null) {
                    linearLayout_splash.setVisibility(View.VISIBLE);
                    linearLayout_padisan.setVisibility(View.VISIBLE);
                    linearLayout_splash.startAnimation(goLeft);
                    linearLayout_padisan.startAnimation(goLeft);
                    if (true){
                        brandList = response.body().getResult();
                        int size = response.body().getResult().size();
                        for (int i = 0; i < size; i++) {
                            switch (i) {
                                case 0:
                                    Result result = response.body().getResult().get(i);
                                    String name = result.getName();
                                    Log.i("kk",name);
                                    Picasso.with(Splash.this)
                                            .load(result.getImageUrl())
                                            .placeholder(R.drawable.padisan)
                                            .error(R.drawable.padisan)
                                            .into(imageView_padisan);

                                    textView_padisan.setText(result.getName());
                                    break;
                                case 1:
                                    Result result1 = response.body().getResult().get(i);
                                    Picasso.with(Splash.this)
                                            .load(result1.getImageUrl())
                                            .placeholder(R.drawable.logo)
                                            .error(R.drawable.logo)
                                            .into(imageView_rubino);

                                    textView_rubino.setText(result1.getName());
                                    break;
                                case 2:
                                    //badaan
                                    break;
                            }
                        }
                    }else{
                        RubinoToast.showToast(Splash.this,response.body().getStatus().getMessage(), Toast.LENGTH_SHORT,RubinoToast.warning);
                    }
                }else {
                    //RubinoToast.showToast(Splash.this,getString(R.string.error), Toast.LENGTH_SHORT,RubinoToast.warning);
                    RubinoToast.showToast(Splash.this,"logout", Toast.LENGTH_SHORT,RubinoToast.warning);
                    login();

                }
            }

            @Override
            public void onFailure(Call<BrandRequest> call, Throwable t) {
                RubinoToast.showToast(Splash.this,getString(R.string.error),Toast.LENGTH_SHORT,RubinoToast.warning);
            }
        });
    }
    private void login(){
        APIService apiService = ApiUtils.getAPIService();
        Call<LoginRequest> login = apiService.login(MyApp.getMemory().getString(Constants.username, ""), MyApp.getMemory().getString(Constants.password, ""));
        login.enqueue(new Callback<LoginRequest>() {
            @Override
            public void onResponse(Call<LoginRequest> call, Response<LoginRequest> response) {
                if (response.body()!=null){
                    if (response.body().getStatus().getIsSuccess()){
                        getBrand();
                        RubinoToast.showToast(Splash.this,"login", Toast.LENGTH_SHORT,RubinoToast.warning);
                    }else {
                        RubinoToast.showToast(Splash.this,response.body().getStatus().getMessage(), Toast.LENGTH_SHORT,RubinoToast.warning);
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginRequest> call, Throwable t) {
                RubinoToast.showToast(Splash.this,getString(R.string.error), Toast.LENGTH_SHORT,RubinoToast.warning);
            }
        });
    }

    //private void startThread() {
    //    new Thread(new Runnable() {
    //        @Override
    //        public void run() {
    //            try {
    //                Thread.sleep(3300);
    //                startActivity(new Intent(Splash.this,BaseActivity.class));
    //                finish();
    //            } catch (InterruptedException e) {
    //                e.printStackTrace();
    //            }
    //        }
    //    }).start();
    //}

    @Override
    public void onBackPressed() {
        createExitAlertDialog();
    }

    private void internetChecker() {
        boolean online = InternetChecker.isOnline(Splash.this);
        if (online){
            getBrand();
        }else{
            //Snackbar.make(findViewById(android.R.id.content), R.string.checkNetwork, Snackbar.LENGTH_LONG).setAction("Action", null).show();
            //RubinoToast.showToast(Splash.this,getString(R.string.checkNetwork),Toast.LENGTH_SHORT,RubinoToast.warning);
        }
    }

    private void createExitAlertDialog() {
        TextView textView_title_dialog;
        TextView textView_message_dialog;
        TextView textView_no_dialog;
        TextView textView_yes_dialog;

        AlertDialog.Builder builder = new AlertDialog.Builder(Splash.this);
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.exite_dialog, null);

        textView_title_dialog = dialogView.findViewById(R.id.textView_title_dialog);
        textView_message_dialog = dialogView.findViewById(R.id.textView_message_dialog);
        textView_no_dialog = dialogView.findViewById(R.id.textView_no_dialog);
        textView_yes_dialog = dialogView.findViewById(R.id.textView_yes_dialog);

        textView_title_dialog.setTypeface(appFont);
        textView_message_dialog.setTypeface(appFont);
        textView_no_dialog.setTypeface(appFont);
        textView_yes_dialog.setTypeface(appFont);

        builder.setView(dialogView);

        textView_no_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                show.dismiss();
            }
        });
        textView_yes_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Splash.super.onBackPressed();
            }
        });

        builder.create();
        show = builder.show();
    }
}
