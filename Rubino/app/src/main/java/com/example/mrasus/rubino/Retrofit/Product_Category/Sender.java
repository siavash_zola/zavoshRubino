package com.example.mrasus.rubino.Retrofit.Product_Category;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Mr.Asus on 5/20/2018.
 */

public class Sender {
    @SerializedName("brandName")
    @Expose
    private String brandName;

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }
}
