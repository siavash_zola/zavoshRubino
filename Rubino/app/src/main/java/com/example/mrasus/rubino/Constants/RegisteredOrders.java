package com.example.mrasus.rubino.Constants;

/**
 * Created by Mr.Asus on 5/5/2018.
 */

public class RegisteredOrders {
    private String date;
    private String total;
    private String transferType;
    private String Condition;
    private String paymentType;
    private String orderCode;
    private String priceORCargoCode;
    private int number;

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getPriceORCargoCode() {
        return priceORCargoCode;
    }

    public void setPriceORCargoCode(String priceORCargoCode) {
        this.priceORCargoCode = priceORCargoCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    private String address;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getTransferType() {
        return transferType;
    }

    public void setTransferType(String transferType) {
        this.transferType = transferType;
    }

    public String getCondition() {
        return Condition;
    }

    public void setCondition(String condition) {
        Condition = condition;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }
}
