package com.example.mrasus.rubino.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mrasus.rubino.Constants.BottomNavigationViewHelper;
import com.example.mrasus.rubino.Constants.Constants;
import com.example.mrasus.rubino.Constants.Product;
import com.example.mrasus.rubino.Constants.TransparentProgressDialog;
import com.example.mrasus.rubino.Database.MyApp;
import com.example.mrasus.rubino.Fragments.Fragment_MessagesNew;
import com.example.mrasus.rubino.Fragments.Fragment_Order;
import com.example.mrasus.rubino.Fragments.Fragment_Profile;
import com.example.mrasus.rubino.Fragments.Fragment_ReturnNew;
import com.example.mrasus.rubino.Fragments.history.Fragment_HistoryNew;
import com.example.mrasus.rubino.Fragments.messages.Fragment_Messages;
import com.example.mrasus.rubino.Fragments.messages.Fragment_SendMessage;
import com.example.mrasus.rubino.Helper;
import com.example.mrasus.rubino.R;
import com.example.mrasus.rubino.fileManagerHelper.afilechooser.utils.FileUtils;

import java.util.ArrayList;
import java.util.List;

public class BaseActivity extends AppCompatActivity {
    public static TextView textView_toolbar;
    public static BottomNavigationView bottomNavigationView;
    public static Typeface appFont;
    public static Typeface appFont_Bold;
    public static List<Product> orderListForShow;
    public static FragmentManager fragmentManager;
    public static boolean isFormReturn = false;
    private FragmentTransaction fragmentTransaction;
    private String brand;
    List<Fragment> fragmentList;
    public static LinearLayout freeze;
    public static boolean isFreezed = false;
    public static Helper.Browser browser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);

        referenceAndToolbar();

        bottomNavigationListener();

    }
    private void referenceAndToolbar() {
        freeze = (LinearLayout) findViewById(R.id.linearLayout_freeze);
        fragmentList = new ArrayList<>();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        textView_toolbar = (TextView) findViewById(R.id.textView_toolbar);
        brand = getIntent().getExtras().getString(Constants.brandName);
        Fragment_Order fragment_order = new Fragment_Order();
        Bundle bundle = new Bundle();
        bundle.putString(Constants.brandName,brand);
        fragment_order.setArguments(bundle);
        fragmentManager = getSupportFragmentManager();
        appFont = Typeface.createFromAsset(getAssets(), "fonts/IRANSansMobile(FaNum)_Light.ttf");
        appFont_Bold = Typeface.createFromAsset(getAssets(), "fonts/IRANSansMobile(FaNum)_Bold.ttf");
        orderListForShow = new ArrayList<>();
        textView_toolbar.setTypeface(appFont);
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomNavigationView);
        setTypeFaceMenu();
        BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame, fragment_order,"order").commit();

        bottomNavigationView.setClickable(false);
    }
    private void bottomNavigationListener() {
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {


                if (item.getItemId() == R.id.order) {
                    clearBackStack();
                    onBackPressed();
                    //fragmentTransaction = fragmentManager.beginTransaction();
                    //Fragment fragmentSelected = new Fragment_Order();
                    //fragmentTransaction
                    //        .replace(R.id.frame,fragmentSelected,"order")
                    //        .commit();

                    return true;
                } else if (item.getItemId() == R.id.return_order) {

                    Fragment fragmentById = fragmentManager.findFragmentById(R.id.frame);
                    clearBackStack();
                    fragmentTransaction = fragmentManager.beginTransaction();
                    Fragment fragmentSelected1 = new Fragment_ReturnNew();
                    fragmentTransaction
                            .replace(R.id.frame,fragmentSelected1,"return_order")
                            .commit();
                    return true;
                } else if (item.getItemId() == R.id.profile) {

                    clearBackStack();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    Fragment fragmentSelected = new Fragment_Profile();
                    fragmentTransaction
                            .replace(R.id.frame,fragmentSelected,"profile")
                            .commit();
                    return true;
                } else if(item.getItemId() == R.id.history){

                    clearBackStack();
                    Fragment fragmentById = fragmentManager.findFragmentById(R.id.frame);
                    fragmentTransaction = fragmentManager.beginTransaction();
                    Fragment fragmentSelected = new Fragment_HistoryNew();
                    fragmentTransaction
                            .replace(R.id.frame,fragmentSelected,"history")
                            .commit();
                    return true;
                }else if (item.getItemId() == R.id.messages){
                    clearBackStack();
                    Fragment fragmentById = fragmentManager.findFragmentById(R.id.frame);
                    fragmentTransaction = fragmentManager.beginTransaction();
                    Fragment fragmentSelected = new Fragment_MessagesNew();
                    fragmentTransaction
                            .replace(R.id.frame,fragmentSelected,"messages")
                            .commit();
                    return true;
                }
                return true;
            }
        });
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == android.R.id.home){
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }


    public void setTypeFaceMenu(){
        TextView textView = (TextView)bottomNavigationView.findViewById(R.id.order).findViewById(R.id.largeLabel);
        TextView textView1 = (TextView)bottomNavigationView.findViewById(R.id.return_order).findViewById(R.id.largeLabel);
        TextView textView2 = (TextView)bottomNavigationView.findViewById(R.id.profile).findViewById(R.id.largeLabel);
        TextView textView3 = (TextView)bottomNavigationView.findViewById(R.id.history).findViewById(R.id.largeLabel);
        TextView textView4 = (TextView)bottomNavigationView.findViewById(R.id.order).findViewById(R.id.smallLabel);
        TextView textView5 = (TextView)bottomNavigationView.findViewById(R.id.return_order).findViewById(R.id.smallLabel);
        TextView textView6 = (TextView)bottomNavigationView.findViewById(R.id.profile).findViewById(R.id.smallLabel);
        TextView textView7 = (TextView)bottomNavigationView.findViewById(R.id.history).findViewById(R.id.smallLabel);
        TextView textView8 = (TextView)bottomNavigationView.findViewById(R.id.messages).findViewById(R.id.smallLabel);
        TextView textView9 = (TextView)bottomNavigationView.findViewById(R.id.messages).findViewById(R.id.largeLabel);

        textView.setTypeface(appFont);
        textView1.setTypeface(appFont);
        textView2.setTypeface(appFont);
        textView3.setTypeface(appFont);
        textView4.setTypeface(appFont);
        textView5.setTypeface(appFont);
        textView6.setTypeface(appFont);
        textView7.setTypeface(appFont);
        textView8.setTypeface(appFont);
        textView9.setTypeface(appFont);
    }


    @Override
    public void onBackPressed() {
        BaseActivity.unFreeze();
        super.onBackPressed();
        //Fragment_Order.oneRequest = 0;
        //int backStackEntryCount = fragmentManager.getBackStackEntryCount();
        //if (backStackEntryCount > 0){
        //    super.onBackPressed();
        //}else{
        //    createExitAlertDialog();
        //}
    }

    public static void clearBackStack(){
        int backStackEntryCount = fragmentManager.getBackStackEntryCount();

        for (int i = backStackEntryCount; i >0 ; i--) {
            fragmentManager.popBackStack();
        }
    }

    public static TransparentProgressDialog transparentProgressDialog ;
    public static void freeze(Context context){
        //freeze.setBackgroundResource(android.R.drawable.screen_background_dark_transparent);
        //freeze.setClickable(true);
        //bottomNavigationView.setClickable(false);
        transparentProgressDialog = new TransparentProgressDialog(context,R.drawable.logo);
        transparentProgressDialog.show();
        isFreezed = true;
    }

    public static void unFreeze(){
        //freeze.setBackgroundResource(android.R.color.transparent);
        //freeze.setClickable(false);
        //bottomNavigationView.setClickable(true);
        try {
            transparentProgressDialog.dismiss();
        }catch (Exception e){

        }

        isFreezed = false;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.i("kk","onActivityResult");
        if (requestCode == 1 && resultCode == RESULT_OK) {
            if (data != null) {
                browser.setUri(data.getData());
            }
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 2 && grantResults.length > 0&& grantResults[0] == PackageManager.PERMISSION_GRANTED){
            browser.showChooserNow();
        }

    }
}