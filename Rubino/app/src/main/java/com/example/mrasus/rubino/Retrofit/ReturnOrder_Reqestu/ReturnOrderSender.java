package com.example.mrasus.rubino.Retrofit.ReturnOrder_Reqestu;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Mr.Asus on 5/22/2018.
 */

public class ReturnOrderSender {
    @SerializedName("returnOrderDetailItems")
    @Expose
    private List<ReturnOrderDetailItem> returnOrderDetailItems = null;
    @SerializedName("shippmentType")
    @Expose
    private String shippmentType;
    @SerializedName("shippmentTitle")
    @Expose
    private String shippmentTitle;

    public List<ReturnOrderDetailItem> getReturnOrderDetailItems() {
        return returnOrderDetailItems;
    }

    public void setReturnOrderDetailItems(List<ReturnOrderDetailItem> returnOrderDetailItems) {
        this.returnOrderDetailItems = returnOrderDetailItems;
    }

    public String getShippmentType() {
        return shippmentType;
    }

    public void setShippmentType(String shippmentType) {
        this.shippmentType = shippmentType;
    }

    public String getShippmentTitle() {
        return shippmentTitle;
    }

    public void setShippmentTitle(String shippmentTitle) {
        this.shippmentTitle = shippmentTitle;
    }
}
